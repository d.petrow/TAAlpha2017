﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Shuffle
{
    class Shuffle
    {
        static int[] numsAndShuffleCount;
        static int[] shuffles;
        static LList linkedList;
        static IDictionary<int, LLNode> fastAccess;

        static void Main(string[] args)
        {
            ReadLL();

            ShuffleLL();

            PrintLL(linkedList);
        }

        private static void ShuffleLL()
        {
            foreach (int shuffle in shuffles)
            {
                int moveAfter = new int();

                if (shuffle % 2 == 0)
                {
                    moveAfter = shuffle / 2;
                }
                else if (shuffle % 2 == 1)
                {
                    moveAfter = shuffle * 2;

                    if (moveAfter > numsAndShuffleCount[0])
                    {
                        moveAfter = numsAndShuffleCount[0];
                    }
                }

                linkedList.MoveAfter(shuffle, moveAfter);
            }
        }

        private static void ReadLL()
        {
            numsAndShuffleCount = Console.ReadLine().Split().Select(int.Parse).ToArray();
            shuffles = Console.ReadLine().Split().Select(int.Parse).ToArray();
            fastAccess = new Dictionary<int, LLNode>();
            linkedList = new LList(fastAccess);

            for (int i = 0; i < numsAndShuffleCount[0]; i++)
            {
                LLNode node = new LLNode(i + 1);

                linkedList.AddFront(node);
                fastAccess.Add(i + 1, node);
            }
        }

        private static void PrintLL(LList linkedList)
        {
            LLNode start = linkedList.Head;
            while (start != null)
            {
                Console.Write(start.Value + " ");

                start = start.Next;
            }

            Console.WriteLine();
        }
    }

    class LList
    {
        private readonly IDictionary<int, LLNode> fastAccess;

        public LList(IDictionary<int, LLNode> fastAccess)
        {
            this.fastAccess = fastAccess;
        }

        public LLNode Head { get; set; }

        public LLNode Tail { get; set; }

        public void AddFront(LLNode node)
        {
            if (this.Head == null)
            {
                this.Head = node;
                this.Tail = node;
            }
            else
            {
                this.Tail.Next = node;
                node.Prev = this.Tail;
                this.Tail = node;
            }
        }

        public void MoveAfter(int toMoveValue, int moveAfter)
        {
            if (toMoveValue == moveAfter)
            {
                return;
            }

            LLNode toMove = this.Find(toMoveValue);

            if (toMove == this.Head)
            {
                this.Head = toMove.Next;
            }

            if (toMove == this.Tail)
            {
                this.Tail = toMove.Prev;
            }

            if (toMove != null)
            {
                toMove.Detach();
            }

            this.AddAfter(toMove, moveAfter);
        }

        private void AddAfter(LLNode moved, int moveAfter)
        {
            LLNode after = this.Find(moveAfter);

            if (after == this.Tail)
            {
                this.AddFront(moved);
                return;
            }

            after.Next.Prev = moved;
            moved.Next = after.Next;

            after.Next = moved;
            moved.Prev = after;
        }

        private LLNode Find(int searched)
        {
            if (this.fastAccess.ContainsKey(searched))
            {
                return this.fastAccess[searched];
            }

            return null;
        }

        public override string ToString()
        {
            if (this.Head == null && this.Tail == null)
            {
                return $"H: null; T: null";
            }

            return $"H: {this.Head.Value}; T: {this.Tail.Value}";
        }
    }

    class LLNode
    {
        public LLNode(int value)
        {
            this.Value = value;
        }

        public int Value { get; }

        public LLNode Next { get; set; }

        public LLNode Prev { get; set; }

        public override string ToString()
        {
            return $"V: {this.Value}";
        }

        public void Detach()
        {
            if (this.Prev != null)
            {
                this.Prev.Next = this.Next;
            }

            if (this.Next != null)
            {
                this.Next.Prev = this.Prev;
            }

            this.Next = null;
            this.Prev = null;
        }
    }
}