﻿using System;
using System.Linq;

namespace BagsOfBalls
{
    class BagsOfBalls
    {
        static int[] ballsAndBags;
        static int[] bagsSizes;
        static int maxSteps;

        static void Main(string[] args)
        {
            ballsAndBags = Console.ReadLine().Split().Select(int.Parse).ToArray();
            bagsSizes = Console.ReadLine().Split().Select(int.Parse).ToArray();
            maxSteps = 0;

            for (int i = 0; i < bagsSizes.Length; i++)
            {
                if (ballsAndBags[0] % bagsSizes[i] != 0 || ballsAndBags[0] == bagsSizes[i])
                {
                    continue;
                }

                SplitBag(ballsAndBags[0], 1, bagsSizes[i]);
            }

            Console.WriteLine(maxSteps);
        }

        private static int SplitBag(int currentBallsCount, int currentBagsCount, int bagSize)
        {
            int currentSteps = 0;

            int nextBallsCount = bagSize;
            int nextBagsCount = currentBallsCount / nextBallsCount;

            currentSteps += currentBagsCount;

            for (int j = 0; j < bagsSizes.Length; j++)
            {
                int nextBagSize = bagsSizes[j];
                if (nextBallsCount % nextBagSize != 0 || nextBallsCount <= nextBagSize)
                {
                    continue;
                }

                currentSteps += SplitBag(nextBallsCount, currentBagsCount + nextBagsCount, nextBagSize);
            }

            if (currentSteps > maxSteps)
            {
                maxSteps = currentSteps;
            }

            return currentSteps;
        }
    }
}
