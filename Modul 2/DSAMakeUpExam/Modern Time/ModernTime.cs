﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wintellect.PowerCollections;

namespace Modern_Time
{
    class ModernTime
    {
        static IDictionary<string, Interest> interests;
        static IDictionary<string, Couple> couples;

        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            ReadInput(n);

            FindCouples();

            Console.WriteLine(couples.Values.Max().ToString());
        }

        private static void FindCouples()
        {
            foreach (Interest interest in interests.Values)
            {
                foreach (Person male in interest.Males)
                {
                    foreach (Person female in interest.Females)
                    {
                        Couple couple;

                        if (!couples.ContainsKey(male.Name + female.Name))
                        {
                            couple = new Couple(male, female);
                            couples.Add(male.Name + female.Name, couple);
                        }
                        else
                        {
                            couple = couples[male.Name + female.Name];
                        }

                        couple.InterestsCount++;
                    }
                }
            }
        }

        private static void ReadInput(int n)
        {
            interests = new Dictionary<string, Interest>();
            couples = new Dictionary<string, Couple>();

            for (int i = 0; i < n; i++)
            {
                string name = Console.ReadLine();
                char gender = char.Parse(Console.ReadLine());
                int interestsCount = int.Parse(Console.ReadLine());
                string[] personInterestsStr = Console.ReadLine().Split(',');

                IList<Interest> personInterests = new List<Interest>();

                for (int j = 0; j < interestsCount; j++)
                {
                    if (!interests.ContainsKey(personInterestsStr[j]))
                    {
                        Interest interest = new Interest(personInterestsStr[j]);
                        interests.Add(personInterestsStr[j], interest);

                        personInterests.Add(interest);
                    }
                    else
                    {
                        Interest interest = interests[personInterestsStr[j]];

                        personInterests.Add(interest);
                    }
                }

                Person person = new Person(name, gender);

                foreach (Interest interest in personInterests)
                {
                    interest.AddPerson(person);
                }
            }
        }
    }

    class Couple : IComparable<Couple>
    {
        public Couple(Person male, Person female)
        {
            this.Male = male;
            this.Female = female;
        }

        public Person Male { get; }
        public Person Female { get; }

        public int InterestsCount { get; set; }

        public int CompareTo(Couple other)
        {
            int result = this.InterestsCount.CompareTo(other.InterestsCount);

            if (result == 0)
            {
                result = this.Male.Name.CompareTo(other.Male.Name) * -1;
            }

            return result;
        }

        public override string ToString()
        {
            return $"{this.Male.Name} and {this.Female.Name} have {this.InterestsCount} common interests!";
        }
    }

    class Person
    {
        public Person(string name, char gender)
        {
            this.Name = name;
            this.Gender = gender;
        }

        public string Name { get; }
        public char Gender { get; }
    }

    class Interest
    {
        public Interest(string name)
        {
            this.Name = name;
            this.Males = new List<Person>();
            this.Females = new List<Person>();
        }

        public string Name { get; }
        public IList<Person> Males { get; }
        public IList<Person> Females { get; }

        public void AddPerson(Person person)
        {
            if (person.Gender == 'm')
            {
                this.Males.Add(person);
            }
            else if (person.Gender == 'f')
            {
                this.Females.Add(person);
            }
        }
    }
}
