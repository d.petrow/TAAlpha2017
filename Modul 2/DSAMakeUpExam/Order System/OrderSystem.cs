﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wintellect.PowerCollections;

namespace Order_System
{
    class OrderSystem
    {
        static OrderedBag<Order> ordersOrderedByPrice;
        static IDictionary<string, OrderedBag<Order>> ordersByConsumer;
        static StringBuilder result;

        static void Main(string[] args)
        {
            Comparison<Order> comparison = new Comparison<Order>((x, y) => x.Price.CompareTo(y.Price));

            ordersOrderedByPrice = new OrderedBag<Order>(comparison);
            ordersByConsumer = new Dictionary<string, OrderedBag<Order>>();
            result = new StringBuilder();

            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                string commandLine = Console.ReadLine();
                int commandSeparatorInd = commandLine.IndexOf(' ');

                string command = commandLine.Substring(0, commandSeparatorInd);
                string[] parameters = commandLine.Substring(commandSeparatorInd + 1).Split(';');

                Execute(command, parameters);
            }

            Console.Write(result.ToString());
        }

        private static void Execute(string command, string[] parameters)
        {
            switch (command)
            {
                case "AddOrder":
                    AddOrder(parameters);
                    break;
                case "DeleteOrders":
                    DeleteOrders(parameters);
                    break;
                case "FindOrdersByPriceRange":
                    FindOrdersByPriceRange(parameters);
                    break;
                case "FindOrdersByConsumer":
                    FindOrdersByConsumer(parameters);
                    break;
            }
        }

        private static void AddOrder(string[] parameters)
        {
            string name = parameters[0];
            decimal price = decimal.Parse(parameters[1]);
            string consumer = parameters[2];

            Order order = new Order(name, price, consumer);

            ordersOrderedByPrice.Add(order);

            if (!ordersByConsumer.ContainsKey(consumer))
            {
                ordersByConsumer.Add(consumer, new OrderedBag<Order>());
            }

            ordersByConsumer[consumer].Add(order);

            result.AppendLine("Order added");
        }

        private static void DeleteOrders(string[] parameters)
        {
            string consumer = parameters[0];

            if (!ordersByConsumer.ContainsKey(consumer))
            {
                result.AppendLine("No orders found");
            }
            else
            {
                OrderedBag<Order> orders = ordersByConsumer[consumer];
                int count = orders.Count;

                foreach (Order orderFromConsumer in orders)
                {
                    ordersOrderedByPrice.Remove(orderFromConsumer);
                }

                orders.Clear();

                result.AppendLine(string.Format("{0} orders deleted", count));
            }
        }

        private static void FindOrdersByPriceRange(string[] parameters)
        {
            decimal fromPrice = decimal.Parse(parameters[0]);
            decimal toPrice = decimal.Parse(parameters[1]);

            var initRange = ordersOrderedByPrice.RangeFrom(new Order("", fromPrice, ""), true);

            var range = initRange.TakeWhile(x => x.Price <= toPrice);

            range = range.OrderBy(x => x.Name);

            if (range.Count() == 0)
            {
                result.AppendLine("No orders found");
            }
            else
            {
                result.Append("{");
                result.Append(string.Join("}\n{", range));
                result.AppendLine("}");
            }
        }

        private static void FindOrdersByConsumer(string[] parameters)
        {
            string consumer = parameters[0];

            if (!ordersByConsumer.ContainsKey(consumer) || ordersByConsumer[consumer].Count == 0)
            {
                result.AppendLine("No orders found");
            }
            else
            {
                result.Append("{");
                result.Append(string.Join("}\n{", ordersByConsumer[consumer]));
                result.AppendLine("}");
            }
        }
    }

    class Order : IComparable<Order>
    {
        public Order(string name, decimal price, string consumer)
        {
            this.Name = name;
            this.Price = price;
            this.Consumer = consumer;
        }

        public string Name { get; }
        public decimal Price { get; }
        public string Consumer { get; }

        public int CompareTo(Order other)
        {
            int result = this.Name.CompareTo(other.Name);

            if (result == 0)
            {
                result = this.Price.CompareTo(other.Price);
            }
            if (result == 0)
            {
                result = this.Consumer.CompareTo(other.Consumer);
            }

            return result;
        }

        public override string ToString()
        {
            return string.Format("{0};{1};{2:f2}", this.Name, this.Consumer, this.Price);
        }
    }
}