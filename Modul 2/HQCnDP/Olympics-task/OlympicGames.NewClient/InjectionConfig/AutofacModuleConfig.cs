﻿using Autofac;
using OlympicGames.Core;
using OlympicGames.Core.Commands;
using OlympicGames.Core.Commands.Decorated;
using OlympicGames.Core.Contracts;
using OlympicGames.Core.Factories;
using OlympicGames.Core.Providers;
using System.Configuration;

namespace OlympicGames.NewClient.InjectionConfig
{
    public class AutofacModuleConfig : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Engine>().As<IEngine>();
            builder.RegisterType<CommandFactory>().As<ICommandFactory>();
            builder.RegisterType<CommandProcessor>().As<ICommandProcessor>();
            builder.RegisterType<OlympicCommittee>().As<IOlympicCommittee>().SingleInstance();
            builder.RegisterType<OlympicsFactory>().As<IOlympicsFactory>().SingleInstance();
            builder.RegisterType<ConsoleWriter>().As<IWriter>();
            builder.RegisterType<ConsoleReader>().As<IReader>();

            builder.RegisterType<ListOlympiansCommand>().Named<ICommand>("listolympiansOrigin");

            bool isTestEnv = bool.Parse(ConfigurationManager.AppSettings.Get("isTestEnviroment"));

            if (isTestEnv)
            {
                builder.RegisterType<LoggerListOlympiansCommand>().Named<ICommand>("listolympians").WithParameter(
                    (pi, ctx) => pi.Name == "listOlympiansCommand",
                    (pi, ctx) => ctx.ResolveNamed<ICommand>("listolympiansOrigin")
                    );
            }
            else
            {
                builder.RegisterType<ListOlympiansCommand>().Named<ICommand>("listolympians");
            }


            builder.RegisterType<CreateBoxerCommand>().Named<ICommand>("createboxer");
            builder.RegisterType<CreateSprinterCommand>().Named<ICommand>("createsprinter");
        }
    }
}
