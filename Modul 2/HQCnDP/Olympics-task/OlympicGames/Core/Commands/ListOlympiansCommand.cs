﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

using OlympicGames.Core.Commands.Abstracts;
using OlympicGames.Core.Contracts;
using OlympicGames.Utils;
using OlympicGames.Olympics.Contracts;

namespace OlympicGames.Core.Commands
{
    public class ListOlympiansCommand : Command, ICommand
    {
        private string key;
        private string order;

        public ListOlympiansCommand(IOlympicCommittee committee, IOlympicsFactory factory)
            : base(committee, factory) { }

        public override string Execute(IList<string> commandParameters)
        {
            var stringBuilder = new StringBuilder();
            var sorted = this.Committee.Olympians.ToList();

            if (sorted.Count == 0)
            {
                stringBuilder.AppendLine(GlobalConstants.NoOlympiansAdded);
                return stringBuilder.ToString();
            }

            this.GetKeyAndOrder(commandParameters);

            stringBuilder.AppendLine(string.Format(GlobalConstants.SortingTitle, this.key, this.order));

            sorted = this.OrderOlympians();

            foreach (var item in sorted)
            {
                stringBuilder.AppendLine(item.ToString());
            }

            return stringBuilder.ToString();
        }

        private void GetKeyAndOrder(IList<string> commandParameters)
        {
            if (commandParameters == null || commandParameters.Count == 0)
            {
                this.key = "firstname";
                this.order = "asc";
            }
            else if (commandParameters.Count == 1)
            {
                this.key = commandParameters[0];
                this.order = "asc";
            }
            else
            {
                if (commandParameters[1].ToLower() != "asc" && commandParameters[1].ToLower() != "desc")
                {
                    this.order = "asc";
                }
                else
                {
                    this.order = commandParameters[1];
                }
                this.key = commandParameters[0];
            }
        }

        private List<IOlympian> OrderOlympians()
        {
            List<IOlympian> sorted;

            if (this.order.ToLower().Trim() == "desc")
            {
                sorted = this.Committee.Olympians.OrderByDescending(x =>
                {
                    return x.GetType().GetProperties().FirstOrDefault(y => y.Name.ToLower() == this.key.ToLower()).GetValue(x, null);
                }).ToList();
            }
            else
            {
                sorted = this.Committee.Olympians.OrderBy(x =>
                {
                    return x.GetType().GetProperties().FirstOrDefault(y => y.Name.ToLower() == this.key.ToLower()).GetValue(x, null);
                }).ToList();
            }

            return sorted;
        }
    }
}
