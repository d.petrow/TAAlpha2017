﻿using System;
using System.Collections.Generic;
using OlympicGames.Core.Commands.Abstracts;
using OlympicGames.Olympics.Contracts;
using OlympicGames.Utils;
using OlympicGames.Core.Contracts;

namespace OlympicGames.Core.Commands
{
    public abstract class CreateOlympianCommand : Command
    {
        public CreateOlympianCommand(IOlympicCommittee committee, IOlympicsFactory factory)
            : base(committee, factory) { }

        protected string FirstName { get; set; }

        protected string LastName { get; set; }

        protected string Country { get; set; }

        public override string Execute(IList<string> commandParameters)
        {
            if (commandParameters.Count != 3)
            {
                throw new ArgumentException(GlobalConstants.ParametersCountInvalid);
            }

            this.FirstName = commandParameters[0];
            this.LastName = commandParameters[1];
            this.Country = commandParameters[2];

            var olympian = this.CreatePerson();

            this.Committee.Olympians.Add(olympian);

            return string.Format("Created {0}\n{1}", olympian.GetType().Name, olympian);
        }

        protected abstract IOlympian CreatePerson();
    }
}
