﻿using OlympicGames.Core.Contracts;
using OlympicGames.Olympics.Contracts;
using OlympicGames.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OlympicGames.Core.Commands
{
    public class CreateSprinterCommand : CreateOlympianCommand, ICommand
    {
        private readonly IDictionary<string, double> records;

        public CreateSprinterCommand(IOlympicCommittee committee, IOlympicsFactory factory)
            : base(committee, factory)
        {
            this.records = new Dictionary<string, double>();
        }

        public override string Execute(IList<string> commandParameters)
        {
            commandParameters.ValidateIfNull();

            if (commandParameters.Count < 3)
            {
                throw new ArgumentException(GlobalConstants.ParametersCountInvalid);
            }

            foreach (var recordItem in commandParameters)
            {
                var recordValue = recordItem.Split('/');
                this.records.Add(recordValue[3], double.Parse(recordValue[1]));
            }

            commandParameters = commandParameters.Take(3).ToList();

            return base.Execute(commandParameters);
        }

        protected override IOlympian CreatePerson()
        {
            return this.Factory.CreateSprinter(this.FirstName, this.LastName, this.Country, this.records);
        }
    }
}
