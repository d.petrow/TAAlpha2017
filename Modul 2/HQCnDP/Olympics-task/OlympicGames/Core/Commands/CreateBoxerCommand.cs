﻿using System;
using System.Collections.Generic;
using OlympicGames.Core.Contracts;
using OlympicGames.Olympics.Contracts;
using OlympicGames.Utils;
using System.Linq;

namespace OlympicGames.Core.Commands
{
    public class CreateBoxerCommand : CreateOlympianCommand, ICommand
    {
        private string category;
        private int wins;
        private int losses;

        public CreateBoxerCommand(IOlympicCommittee committee, IOlympicsFactory factory)
            : base(committee, factory) { }

        public override string Execute(IList<string> commandParameters)
        {
            commandParameters.ValidateIfNull();

            if (commandParameters.Count < 3)
            {
                throw new ArgumentException(GlobalConstants.ParametersCountInvalid);
            }

            this.category = commandParameters[3];

            bool checkWins = int.TryParse(commandParameters[4], out this.wins);
            bool checkLosses = int.TryParse(commandParameters[5], out this.losses);

            if (!checkWins || !checkLosses)
            {
                throw new ArgumentException(GlobalConstants.WinsLossesMustBeNumbers);
            }

            commandParameters = commandParameters.Take(3).ToList();

            return base.Execute(commandParameters);
        }

        protected override IOlympian CreatePerson()
        {
            return this.Factory.CreateBoxer(this.FirstName, this.LastName, this.Country, this.category, this.wins, this.losses);
        }
    }
}
