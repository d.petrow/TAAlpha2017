﻿using System.Collections.Generic;
using OlympicGames.Core.Contracts;
using System.Text;
using System;

namespace OlympicGames.Core.Commands.Decorated
{
    public class LoggerListOlympiansCommand : ICommand
    {
        private readonly ICommand listOlympiansCommand;

        public LoggerListOlympiansCommand(ICommand listOlympiansCommand)
        {
            this.listOlympiansCommand = listOlympiansCommand;
        }

        public string Execute(IList<string> commandParameters)
        {
            StringBuilder result = new StringBuilder();

            result.AppendLine(this.listOlympiansCommand.Execute(commandParameters));
            result.AppendLine(string.Format("Command Proccesed: listolympians; {0}", DateTime.Now));

            return result.ToString();
        }
    }
}
