﻿using System;

using OlympicGames.Core.Contracts;
using OlympicGames.Core.Factories;
using OlympicGames.Core.Providers;
using System.Linq;

namespace OlympicGames.Core
{
    public class Engine : IEngine
    {
        private readonly ICommandFactory commandFactory;
        private readonly ICommandProcessor commandProcessor;
        private readonly IOlympicsFactory olympicsFactory;
        private readonly IReader reader;
        private readonly IWriter writer;
        private readonly IOlympicCommittee committee;

        private const string Delimiter = "####################";

        public Engine(
            ICommandFactory commandFactory,
            ICommandProcessor commandProcessor,
            IOlympicCommittee committee,
            IOlympicsFactory olympicsFactory,
            IReader reader,
            IWriter writer
            )
        {
            this.commandFactory = commandFactory;
            this.commandProcessor = commandProcessor;
            this.olympicsFactory = olympicsFactory;
            this.reader = reader;
            this.writer = writer;
            this.committee = committee;
        }

        public void Run()
        {
            string commandLine = null;

            while ((commandLine = this.reader.ReadLine()) != "end")
            {
                try
                {
                    var commandParameters = commandLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    var commandName = commandParameters[0];
                    var command = this.commandFactory.Create(commandName);

                    if (command != null)
                    {
                        commandParameters = commandParameters.Skip(1).ToList();
                        this.writer.WriteLine(this.commandProcessor.ProcessSingleCommand(command, commandParameters));
                        this.writer.WriteLine(Delimiter);
                    }
                }
                catch (Exception ex)
                {
                    while (ex.InnerException != null)
                    {
                        ex = ex.InnerException;
                    }

                    this.writer.WriteLine(string.Format("ERROR: {0}", ex.Message));
                }
            }
        }
    }
}
