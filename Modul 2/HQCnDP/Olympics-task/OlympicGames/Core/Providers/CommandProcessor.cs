﻿using System;
using System.Collections.Generic;
using System.Linq;

using OlympicGames.Core.Contracts;

namespace OlympicGames.Core.Providers
{
    public class CommandProcessor : ICommandProcessor
    {
        public string ProcessSingleCommand(ICommand command, IList<string> commandParameters)
        {
            var result = command.Execute(commandParameters);
            return this.NormalizeOutput(result);
        }

        private string NormalizeOutput(string commandOutput)
        {
            var list = commandOutput.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList().Where(x => !string.IsNullOrWhiteSpace(x));

            return string.Join("\r\n", list);
        }
    }
}