﻿using Autofac;
using OlympicGames.Core.Contracts;
using System;

namespace OlympicGames.Core.Factories
{
    public class CommandFactory : ICommandFactory
    {
        private readonly IComponentContext container;

        public CommandFactory(IComponentContext container)
        {
            this.container = container;
        }

        public ICommand Create(string commandName)
        {
            ICommand command;

            try
            {
                command = this.container.ResolveNamed<ICommand>(commandName);
            }
            catch (Exception e)
            {
                throw new ArgumentException("No such command implemented! Consider implementing it before using!");
            }

            return command;
        }
    }
}
