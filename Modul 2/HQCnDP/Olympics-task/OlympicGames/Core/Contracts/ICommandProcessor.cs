﻿using System.Collections.Generic;

namespace OlympicGames.Core.Contracts
{
    public interface ICommandProcessor
    {
        string ProcessSingleCommand(ICommand command, IList<string> commandParameters);
    }
}
