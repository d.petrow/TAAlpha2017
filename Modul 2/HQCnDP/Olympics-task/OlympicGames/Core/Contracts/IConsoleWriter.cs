﻿namespace OlympicGames.Core.Contracts
{
    public interface IWriter
    {
        void WriteLine(string msg);
    }
}