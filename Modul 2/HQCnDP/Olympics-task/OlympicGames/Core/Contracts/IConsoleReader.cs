﻿namespace OlympicGames.Core.Contracts
{
    public interface IReader
    {
        string ReadLine();
    }
}