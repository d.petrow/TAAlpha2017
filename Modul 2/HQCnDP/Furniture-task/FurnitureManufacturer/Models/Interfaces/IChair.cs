﻿namespace FurnitureManufacturer.Models.Interfaces
{
    public interface IChair : IFurniture
    {
        int NumberOfLegs { get; }
    }
}
