﻿using FurnitureManufacturer.Models.Interfaces;

namespace FurnitureManufacturer.Models.Furnitures.ChairDecorator
{
    public abstract class Decorator : Chair
    {
        private readonly IChair chair;

        public Decorator(IChair chair) : base(chair.Model, chair.Material, chair.Price, chair.Height, chair.NumberOfLegs)
        {
            this.chair = chair;
        }
    }
}
