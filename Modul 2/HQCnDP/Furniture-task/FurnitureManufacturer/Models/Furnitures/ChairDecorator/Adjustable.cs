﻿using FurnitureManufacturer.Models.Interfaces;

namespace FurnitureManufacturer.Models.Furnitures.ChairDecorator
{
    public class Adjustable : Decorator
    {
        public Adjustable(IChair chair) : base(chair) { }

        public virtual void AdjustHeight(decimal height)
        {
            this.Height = height;
        }
    }
}
