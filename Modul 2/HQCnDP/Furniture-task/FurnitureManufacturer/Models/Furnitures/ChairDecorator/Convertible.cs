﻿using FurnitureManufacturer.Models.Interfaces;
using System;

namespace FurnitureManufacturer.Models.Furnitures.ChairDecorator
{
    public class Convertible : Decorator
    {
        public Convertible(IChair chair) : base(chair) { }

        public ChairState State { get; set; } = ChairState.Immovable;
    }
}
