﻿namespace FurnitureManufacturer.Models.Furnitures.ChairDecorator
{
    public enum ChairType
    {
        Normal,
        Adjustable,
        Convertible
    }
}
