﻿using Autofac;
using FurnitureManufacturer.Core.Interfaces.Engine;
using System.Reflection;

namespace FurnitureManufacturer
{
    public class FurnitureProgram
    {
        public static void Main()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(Assembly.GetAssembly(typeof(IEngine)));
            var container = builder.Build();

            var engine = container.Resolve<IEngine>();
            engine.Start();
        }
    }
}
