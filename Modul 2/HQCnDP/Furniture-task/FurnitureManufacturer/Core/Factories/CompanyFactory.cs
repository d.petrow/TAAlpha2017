﻿using FurnitureManufacturer.Core.Interfaces.Engine;
using FurnitureManufacturer.Models;
using FurnitureManufacturer.Models.Interfaces;

namespace FurnitureManufacturer.Core.Factories
{
    public class CompanyFactory : ICompanyFactory
    {
        public ICompany CreateCompany(string name, string registrationNumber)
        {
            return new Company(name, registrationNumber);
        }
    }
}
