﻿using Autofac;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;
using System;

namespace FurnitureManufacturer.Core.Factories
{
    public class CommandFactory : ICommandFactory
    {
        private readonly string InvalidCommandErrorMessage = "Invalid command name: {0}";
        private readonly IComponentContext constainer;

        public CommandFactory(IComponentContext constainer)
        {
            this.constainer = constainer;
        }

        public ICommand Create(string commandName)
        {
            try
            {
                return this.constainer.ResolveNamed<ICommand>(commandName);
            }
            catch (Exception)
            {
                throw new ArgumentException(this.InvalidCommandErrorMessage);
            }
        }
    }
}
