﻿using FurnitureManufacturer.Core.Interfaces.Engine;
using FurnitureManufacturer.Models;
using FurnitureManufacturer.Models.Furnitures;
using FurnitureManufacturer.Models.Interfaces;
using System;

namespace FurnitureManufacturer.Core.Factories
{
    public class FurnitureFactory : IFurnitureFactory
    {
        private const string InvalidMaterialName = "Invalid material name: {0}";

        public IFurniture CreateTable(string model, string materialType, decimal price, decimal height, decimal length, decimal width)
        {
            return new Table(model, this.GetMaterialType(materialType), price, height, length, width);
        }

        public IFurniture CreateChair(string model, string materialType, decimal price, decimal height, int numberOfLegs)
        {
            return new Chair(model, this.GetMaterialType(materialType), price, height, numberOfLegs);
        }

        private MaterialType GetMaterialType(string material)
        {
            try
            {
                return (MaterialType)Enum.Parse(typeof(MaterialType), material, true);
            }
            catch (Exception)
            {
                throw new ArgumentException(string.Format(InvalidMaterialName, material));
            }
        }
    }
}
