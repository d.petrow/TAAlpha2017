﻿using FurnitureManufacturer.Core.Factories;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;
using FurnitureManufacturer.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FurnitureManufacturer.Core
{
    public sealed class Engine : IEngine
    {
        private readonly ICommandFactory commandFactory;
        private readonly ICommandProcessor processor;
        private readonly ICommandReader commandReader;
        private readonly ICommandOutputer commandOutputer;

        public Engine(ICommandFactory commandFactory, ICommandProcessor processor, ICommandReader commandReader, ICommandOutputer commandOutputer)
        {
            this.commandFactory = commandFactory;
            this.processor = processor;
            this.commandReader = commandReader;
            this.commandOutputer = commandOutputer;
        }

        public void Start()
        {
            var commandResults = new List<string>();

            try
            {
                var commandLines = this.commandReader.ReadCommands();

                string commandName;
                ICommand command;
                IList<string> parameters;
                string commandResult;

                foreach (string commandLine in commandLines)
                {
                    parameters = commandLine.Split(' ').ToList();

                    commandName = parameters[0];
                    command = this.commandFactory.Create(commandName);

                    parameters = parameters.Skip(1).ToList();
                    commandResult = this.processor.ProcessSingeCommand(command, parameters);

                    commandResults.Add(commandResult);
                }
            }
            catch (Exception ex)
            {
                commandResults.Add(ex.Message);
            }

            this.commandOutputer.RenderCommandResults(commandResults);
        }
    }
}
