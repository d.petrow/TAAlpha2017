﻿using System.Collections.Generic;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;
using System;

namespace FurnitureManufacturer.Core
{
    public class CommandReader : ICommandReader
    {
        private readonly string InvalidRendererMessage = "Invalid renderer passed!";
        private readonly IRenderer renderer;

        public CommandReader(IRenderer renderer)
        {
            this.renderer = renderer ?? throw new ArgumentNullException(this.InvalidRendererMessage);
        }

        public IRenderer Renderer { get; }

        public IEnumerable<string> ReadCommands()
        {
            var commands = new List<string>();
            string currentLine = this.renderer.Input();

            while (currentLine != string.Empty)
            {
                commands.Add(currentLine);
                currentLine = this.renderer.Input();
            }

            return commands;
        }
    }
}
