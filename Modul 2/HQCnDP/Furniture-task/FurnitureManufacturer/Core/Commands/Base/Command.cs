﻿using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;
using System;
using System.Collections.Generic;

namespace FurnitureManufacturer.Core.Commands.Base
{
    public abstract class Command : ICommand
    {
        protected readonly string CompanyNotFoundErrorMessage = "Company {0} not found";
        protected readonly string FurnitureNotFoundErrorMessage = "Furniture {0} not found";
        protected readonly string FurnitureExistsErrorMessage = "Furniture {0} already exists";


        private const string NullOrEmptyNameErrorMessage = "Name cannot be null or empty";
        private const string NullCollectionOfParameters = "Collection of parameteres cannot be null";

        private readonly IDatabase database;

        public Command(IDatabase database)
        {
            //this.parameters = parameters ?? throw new ArgumentNullException(NullCollectionOfParameters);

            this.database = database;
        }

        protected IDatabase Database => database;

        public abstract string Execute(IList<string> parameters);
    }
}
