﻿using System.Collections.Generic;
using FurnitureManufacturer.Core.Commands.Base;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;

namespace FurnitureManufacturer.Core.Commands
{
    public class AddFurnitureToCompanyCommand : Command, ICommand
    {
        private readonly string FurnitureAddedSuccessMessage = "Furniture {0} added to company {1}";

        public AddFurnitureToCompanyCommand(IDatabase database) : base(database)
        {
        }

        public override string Execute(IList<string> parameters)
        {
            var companyName = parameters[0];
            var furnitureName = parameters[1];

            if (!this.Database.Companies.ContainsKey(companyName))
            {
                return string.Format(this.CompanyNotFoundErrorMessage, companyName);
            }

            if (!this.Database.Furnitures.ContainsKey(furnitureName))
            {
                return string.Format(this.FurnitureNotFoundErrorMessage, furnitureName);
            }

            var company = this.Database.Companies[companyName];
            var furniture = this.Database.Furnitures[furnitureName];
            company.Add(furniture);

            return string.Format(this.FurnitureAddedSuccessMessage, furnitureName, companyName);
        }
    }
}
