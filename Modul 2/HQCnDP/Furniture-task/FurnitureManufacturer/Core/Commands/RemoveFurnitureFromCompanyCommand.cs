﻿using System.Collections.Generic;
using FurnitureManufacturer.Core.Commands.Base;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;

namespace FurnitureManufacturer.Core.Commands
{
    public class RemoveFurnitureFromCompanyCommand : Command, ICommand
    {
        private readonly string FurnitureRemovedSuccessMessage = "Furniture {0} removed from company {1}";

        public RemoveFurnitureFromCompanyCommand(IDatabase database) : base(database)
        {
        }

        public override string Execute(IList<string> parameters)
        {
            var companyName = parameters[0];
            var furnitureName = parameters[1];

            if (!this.Database.Companies.ContainsKey(companyName))
            {
                return string.Format(this.CompanyNotFoundErrorMessage, companyName);
            }

            if (!this.Database.Furnitures.ContainsKey(furnitureName))
            {
                return string.Format(this.FurnitureNotFoundErrorMessage, furnitureName);
            }

            var company = this.Database.Companies[companyName];
            var furniture = this.Database.Furnitures[furnitureName];
            company.Remove(furniture);

            return string.Format(this.FurnitureRemovedSuccessMessage, furnitureName, companyName);
        }
    }
}
