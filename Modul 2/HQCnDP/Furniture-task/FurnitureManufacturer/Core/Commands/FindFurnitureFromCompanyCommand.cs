﻿using System.Collections.Generic;
using FurnitureManufacturer.Core.Commands.Base;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;

namespace FurnitureManufacturer.Core.Commands
{
    public class FindFurnitureFromCompanyCommand : Command, ICommand
    {
        public FindFurnitureFromCompanyCommand(IDatabase database) : base(database)
        {
        }

        public override string Execute(IList<string> parameters)
        {
            var companyName = parameters[0];
            var furnitureName = parameters[1];

            if (!this.Database.Companies.ContainsKey(companyName))
            {
                return string.Format(this.CompanyNotFoundErrorMessage, companyName);
            }

            var company = this.Database.Companies[companyName];
            var furniture = company.Find(furnitureName);
            if (furniture == null)
            {
                return string.Format(this.FurnitureNotFoundErrorMessage, furnitureName);
            }

            return furniture.ToString();
        }
    }
}
