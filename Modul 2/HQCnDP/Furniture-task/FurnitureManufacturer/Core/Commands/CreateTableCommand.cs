﻿using System.Collections.Generic;
using FurnitureManufacturer.Core.Commands.Base;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;
using FurnitureManufacturer.Models.Interfaces;

namespace FurnitureManufacturer.Core.Commands
{
    public class CreateTableCommand : Command, ICommand
    {
        private readonly string TableCreatedSuccessMessage = "Table {0} created";

        private readonly IFurnitureFactory furnitureFactory;

        public CreateTableCommand(IFurnitureFactory furnitureFactory, IDatabase database) : base(database)
        {
            this.furnitureFactory = furnitureFactory;
        }

        public override string Execute(IList<string> parameters)
        {
            string model = parameters[0];
            string material = parameters[1];
            decimal price = decimal.Parse(parameters[2]);
            decimal height = decimal.Parse(parameters[3]);
            decimal length = decimal.Parse(parameters[4]);
            decimal width = decimal.Parse(parameters[5]);

            if (this.Database.Furnitures.ContainsKey(model))
            {
                return string.Format(this.FurnitureExistsErrorMessage, model);
            }

            IFurniture table = this.furnitureFactory.CreateTable(model, material, price, height, length, width);
            this.Database.Furnitures.Add(model, table);

            return string.Format(this.TableCreatedSuccessMessage, model);
        }
    }
}
