﻿using System.Collections.Generic;
using FurnitureManufacturer.Core.Commands.Base;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;

namespace FurnitureManufacturer.Core.Commands
{
    public class ConvertChairCommand : Command, ICommand
    {
        public ConvertChairCommand(IDatabase database) : base(database)
        {
        }

        public override string Execute(IList<string> parameters)
        {
            throw new System.NotImplementedException();
        }

        //internal const string InvalidChairTypeErrorMessage = "Invalid chair type: {0}";
        //internal const string FurnitureIsNotAdjustableChairErrorMessage = "{0} is not adjustable chair";
        //internal const string FurnitureIsNotConvertibleChairErrorMessage = "{0} is not convertible chair";

        //// Success messages
        //internal const string FurnitureAddedSuccessMessage = "Furniture {0} added to company {1}";
        //internal const string ChairHeightAdjustedSuccessMessage = "Chair {0} adjusted to height {1}";
        //internal const string ChairStateConvertedSuccessMessage = "Chair {0} converted";
    }
}
