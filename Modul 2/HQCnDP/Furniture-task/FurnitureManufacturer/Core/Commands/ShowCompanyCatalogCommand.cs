﻿using System.Collections.Generic;
using FurnitureManufacturer.Core.Commands.Base;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;

namespace FurnitureManufacturer.Core.Commands
{
    public class ShowCompanyCatalogCommand : Command, ICommand
    {
        public ShowCompanyCatalogCommand(IDatabase database) : base(database)
        {
        }

        public override string Execute(IList<string> parameters)
        {
            var companyName = parameters[0];

            if (!this.Database.Companies.ContainsKey(companyName))
            {
                return string.Format(this.CompanyNotFoundErrorMessage, companyName);
            }

            return this.Database.Companies[companyName].Catalog();
        }
    }
}
