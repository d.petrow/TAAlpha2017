﻿using System.Collections.Generic;
using FurnitureManufacturer.Core.Commands.Base;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;
using FurnitureManufacturer.Models.Interfaces;
using System;
using FurnitureManufacturer.Models.Furnitures.ChairDecorator;

namespace FurnitureManufacturer.Core.Commands
{
    public class CreateChairCommand : Command, ICommand
    {
        private readonly string ChairCreatedSuccessMessage = "Chair {0} created";

        private readonly IFurnitureFactory furnitureFactory;

        public CreateChairCommand(IFurnitureFactory furnitureFactory, IDatabase database) : base(database)
        {
            this.furnitureFactory = furnitureFactory;
        }

        public override string Execute(IList<string> parameters)
        {
            string model = parameters[0];
            string material = parameters[1];
            decimal price = decimal.Parse(parameters[2]);
            decimal height = decimal.Parse(parameters[3]);
            int legs = int.Parse(parameters[4]);
            string type = parameters[5];

            if (this.Database.Furnitures.ContainsKey(model))
            {
                return string.Format(this.FurnitureExistsErrorMessage, model);
            }

            IFurniture chair = this.furnitureFactory.CreateChair(model, material, price, height, legs);

            if (((ChairType)Enum.Parse(typeof(ChairType), type, true)) == ChairType.Adjustable)
            {
                IFurniture adjustable = new Adjustable((IChair)chair);
            }
            else if (((ChairType)Enum.Parse(typeof(ChairType), type, true)) == ChairType.Convertible)
            {
                IFurniture convertible = new Convertible((IChair)chair);
            }

            this.Database.Furnitures.Add(model, chair);

            return string.Format(this.ChairCreatedSuccessMessage, model);
        }
    }
}
