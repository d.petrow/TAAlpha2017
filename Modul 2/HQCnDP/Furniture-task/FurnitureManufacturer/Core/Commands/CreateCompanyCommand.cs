﻿using System.Collections.Generic;
using FurnitureManufacturer.Core.Commands.Base;
using FurnitureManufacturer.Core.Interfaces.Engine;
using FurnitureManufacturer.Core.Interfaces;
using System;

namespace FurnitureManufacturer.Core.Commands
{
    public class CreateCompanyCommand : Command, ICommand
    {
        private readonly string CompanyCreatedSuccessMessage = "Company {0} created";
        private readonly string CompanyExistsErrorMessage = "Company {0} already exists";

        private readonly ICompanyFactory companyFactory;

        public CreateCompanyCommand(ICompanyFactory companyFactory, IDatabase database) : base(database)
        {
            this.companyFactory = companyFactory;
        }

        public override string Execute(IList<string> parameters)
        {
            var companyName = parameters[0];
            var registrationNumber = parameters[1];

            if (this.Database.Companies.ContainsKey(companyName))
            {
                return string.Format(this.CompanyExistsErrorMessage, companyName);
            }

            var company = this.companyFactory.CreateCompany(companyName, registrationNumber);
            this.Database.Companies.Add(companyName, company);

            return string.Format(this.CompanyCreatedSuccessMessage, companyName);
        }
    }
}
