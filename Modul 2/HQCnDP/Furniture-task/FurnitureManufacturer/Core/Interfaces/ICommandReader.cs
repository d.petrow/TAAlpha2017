﻿using System.Collections.Generic;

namespace FurnitureManufacturer.Core.Interfaces
{
    public interface ICommandReader
    {
        IEnumerable<string> ReadCommands();
    }
}
