﻿using System.Collections.Generic;

namespace FurnitureManufacturer.Core.Interfaces
{
    public interface ICommandOutputer
    {
        void RenderCommandResults(IEnumerable<string> output);
    }
}
