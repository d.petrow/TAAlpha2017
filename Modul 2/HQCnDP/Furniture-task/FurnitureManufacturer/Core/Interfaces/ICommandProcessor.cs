﻿using FurnitureManufacturer.Core.Interfaces.Engine;
using System.Collections.Generic;

namespace FurnitureManufacturer.Core.Interfaces
{
    public interface ICommandProcessor
    {
        string ProcessSingeCommand(ICommand command, IList<string> parameters);
    }
}
