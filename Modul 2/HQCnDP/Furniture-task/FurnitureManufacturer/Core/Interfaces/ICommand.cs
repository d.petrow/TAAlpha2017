﻿using System.Collections.Generic;

namespace FurnitureManufacturer.Core.Interfaces.Engine
{
    public interface ICommand
    {
        string Execute(IList<string> parameters);
    }
}
