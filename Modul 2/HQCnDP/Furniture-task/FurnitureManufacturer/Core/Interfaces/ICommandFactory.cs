﻿using FurnitureManufacturer.Core.Interfaces.Engine;
using System.Collections.Generic;

namespace FurnitureManufacturer.Core.Interfaces
{
    public interface ICommandFactory
    {
        ICommand Create(string commandName);
    }
}
