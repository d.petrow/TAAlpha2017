﻿namespace FurnitureManufacturer.Core.Interfaces.Engine
{
    public interface IEngine
    {
        void Start();
    }
}
