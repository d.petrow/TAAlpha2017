﻿namespace FurnitureManufacturer.Core.Interfaces.Engine
{
    public interface IRenderer
    {
        string Input();

        void Output(string output);
    }
}
