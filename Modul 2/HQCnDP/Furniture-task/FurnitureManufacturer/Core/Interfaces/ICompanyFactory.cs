﻿using FurnitureManufacturer.Models.Interfaces;

namespace FurnitureManufacturer.Core.Interfaces.Engine
{
    public interface ICompanyFactory
    {
        ICompany CreateCompany(string name, string registrationNumber);
    }
}
