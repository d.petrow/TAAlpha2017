﻿using FurnitureManufacturer.Models.Interfaces;
using System.Collections.Generic;

namespace FurnitureManufacturer.Core.Interfaces
{
    public interface IDatabase
    {
        IDictionary<string, ICompany> Companies { get; }

        IDictionary<string, IFurniture> Furnitures { get; }
    }
}
