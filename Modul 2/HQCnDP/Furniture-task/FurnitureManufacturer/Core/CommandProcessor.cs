﻿using System.Collections.Generic;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;
using Autofac;

namespace FurnitureManufacturer.Core
{
    public class CommandProcessor : ICommandProcessor
    {
        public string ProcessSingeCommand(ICommand command, IList<string> parameters)
        {
            return command.Execute(parameters);
        }
        
        //InvalidCommandErrorMessage, command.Name));
    }
}
