﻿using FurnitureManufacturer.Core.Interfaces.Engine;
using System;


namespace FurnitureManufacturer.Core
{
    public class ConsoleRenderer : IRenderer
    {
        public string Input()
        {
            return Console.ReadLine();
        }

        public void Output(string output)
        {
            Console.WriteLine(output);
        }
    }
}
