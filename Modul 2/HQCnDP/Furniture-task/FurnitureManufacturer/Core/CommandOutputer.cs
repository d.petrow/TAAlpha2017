﻿using System.Collections.Generic;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;

namespace FurnitureManufacturer.Core
{
    class CommandOutputer : ICommandOutputer
    {
        private readonly IRenderer renderer;

        public CommandOutputer(IRenderer renderer)
        {
            this.renderer = renderer;
        }

        public void RenderCommandResults(IEnumerable<string> output)
        {
            foreach (string outputLine in output)
            {
                this.renderer.Output(outputLine);
            }
        }
    }
}
