﻿using System.Collections.Generic;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Models.Interfaces;

namespace FurnitureManufacturer.Core.DataStorage
{
    public class Database : IDatabase
    {
        public Database()
        {
            this.Companies = new Dictionary<string, ICompany>();
            this.Furnitures = new Dictionary<string, IFurniture>();
        }

        public IDictionary<string, ICompany> Companies { get; private set; }

        public IDictionary<string, IFurniture> Furnitures { get; private set; }
    }
}
