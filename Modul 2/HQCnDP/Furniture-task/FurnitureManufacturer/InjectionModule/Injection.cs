﻿using Autofac;
using FurnitureManufacturer.Core;
using FurnitureManufacturer.Core.Commands;
using FurnitureManufacturer.Core.DataStorage;
using FurnitureManufacturer.Core.Factories;
using FurnitureManufacturer.Core.Interfaces;
using FurnitureManufacturer.Core.Interfaces.Engine;

namespace FurnitureManufacturer.InjectionModule
{
    public class Injection : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Engine>().As<IEngine>();
            builder.RegisterType<CommandReader>().As<ICommandReader>();
            builder.RegisterType<CommandOutputer>().As<ICommandOutputer>();
            builder.RegisterType<ConsoleRenderer>().As<IRenderer>();
            builder.RegisterType<CommandProcessor>().As<ICommandProcessor>();
            builder.RegisterType<Database>().As<IDatabase>().SingleInstance();
            builder.RegisterType<CommandFactory>().As<ICommandFactory>();
            builder.RegisterType<FurnitureFactory>().As<IFurnitureFactory>();
            builder.RegisterType<CompanyFactory>().As<ICompanyFactory>();

            //Commands
            builder.RegisterType<CreateCompanyCommand>().Named<ICommand>("CreateCompany");
            builder.RegisterType<AddFurnitureToCompanyCommand>().Named<ICommand>("AddFurnitureToCompany");
            builder.RegisterType<RemoveFurnitureFromCompanyCommand>().Named<ICommand>("RemoveFurnitureFromCompany");
            builder.RegisterType<FindFurnitureFromCompanyCommand>().Named<ICommand>("FindFurnitureFromCompany");
            builder.RegisterType<ShowCompanyCatalogCommand>().Named<ICommand>("ShowCompanyCatalog");
            builder.RegisterType<CreateTableCommand>().Named<ICommand>("CreateTable");
            builder.RegisterType<CreateChairCommand>().Named<ICommand>("CreateChair");
        }
    }
}
