﻿using FurnitureManufacturer.Core;
using FurnitureManufacturer.Core.Interfaces.Engine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace FurnitureManufacturer.UnitTests.Core.CommandReaderTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void Assign_Passed_Values()
        {
            //arrange
            Mock<IRenderer> expectedRenderer = new Mock<IRenderer>();

            //act
            var commandReader = new CommandReader(expectedRenderer.Object);
            var actualRenderer = commandReader.Renderer;

            //assert
            Assert.AreSame(expectedRenderer.Object, actualRenderer);
        }

        [TestMethod]
        public void Throw_ArgumenNullException_When_RendererIsNull()
        {
            //arrange & act
            Action creatingCommandReader = (() => new CommandReader(null));

            //assert
            Assert.ThrowsException<ArgumentNullException>(creatingCommandReader);
        }
    }
}
