﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _01.SecretMessage
{
    class SecretMessage
    {
        static string encodedMessage;
        static StringBuilder finalMessage;

        static void Main(string[] args)
        {
            encodedMessage = Console.ReadLine();
            finalMessage = new StringBuilder();

            Evaluate();

            Console.WriteLine(finalMessage);
        }

        private static int Evaluate(int ind = 0)
        {
            int n = new int();
            string num = string.Empty;

            for (int i = ind; i < encodedMessage.Length; i++)
            {
                if (char.IsLetter(encodedMessage[i]))
                {
                    finalMessage.Append(encodedMessage[i]);
                }
                else if (char.IsDigit(encodedMessage[i]))
                {
                    num += encodedMessage[i];
                }
                else if (encodedMessage[i] == '{')
                {
                    n = int.Parse(num);
                    num = string.Empty;

                    int iToContinue = new int();

                    for (int j = 0; j < n; j++)
                    {
                        iToContinue = Evaluate(i + 1);
                    }

                    i = iToContinue;
                }
                else if (encodedMessage[i] == '}')
                {
                    return i;
                }
            }

            return 0;
        }
    }
}