﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _03.Expressions
{
    class Expressions
    {
        static int[] digits;
        static string[] exprOps;
        static string[] operations;
        static int searched;
        static int k;

        public static void Main()
        {
            string input = Console.ReadLine();
            digits = input.Select(x => int.Parse(x.ToString())).ToArray();
            exprOps = new string[digits.Length - 1];
            operations = new string[] { "+", "-", "*", "m" };

            searched = int.Parse(Console.ReadLine());
            k = 0;

            FindAllExpresssions();

            Console.WriteLine(k);
        }

        private static void FindAllExpresssions(int digit = 1)
        {
            if (digit == digits.Length)
            {
                Console.Write("({0}); [{1}]; Result: ", string.Join(", ", digits), string.Join(", ", exprOps));
                Evaluate();
                return;
            }

            for (int i = 0; i < 4; i++)
            {
                exprOps[digit - 1] = operations[i];
                FindAllExpresssions(digit + 1);
            }
        }

        private static void Evaluate()
        {
            int result = 0;
            List<int> numbers = digits.Select(x => x).ToList();
            bool[] calculated = new bool[digits.Length];

            string number = string.Empty;

            //Merge
            if (exprOps.Contains("m"))
            {
                for (int i = 0; i < exprOps.Length; i++)
                {
                    if (exprOps[i] == "m")
                    {
                        number += digits[i].ToString();

                        int j = i + 1;
                        for (; j < exprOps.Length && exprOps[j - 1] == "m"; j++)
                        {
                            number += digits[j].ToString();
                        }

                        if (number[0] == '0')
                        {
                            Console.WriteLine("Leading Zero!");
                            return;
                        }

                        for (int ind = i; ind < numbers.Count && ind < i + number.Length; ind++)
                        {
                            numbers[ind] = int.Parse(number);
                        }

                        i = j - 1;
                        number = string.Empty;
                    }
                }


            }

            //Multyply
            if (exprOps.Contains("*"))
            {
                int product = 0;

                for (int i = 0; i < exprOps.Length; i++)
                {
                    if (exprOps[i] == "*")
                    {
                        if (!calculated[i] && !calculated[i + 1])
                        {
                            product = numbers[i] * numbers[i + 1];
                            numbers[i] = product;
                            numbers[i + 1] = product;

                            calculated[i] = true;
                            calculated[i + 1] = true;
                        }
                        else if (!calculated[i])
                        {
                            product = numbers[i];
                            numbers[i] = product;

                            calculated[i] = true;
                        }
                        else if (!calculated[i + 1])
                        {
                            product = numbers[i + 1];
                            numbers[i + 1] = product;

                            calculated[i + 1] = true;
                        }
                    }
                }

                for (int i = 0; i < calculated.Length; i++)
                {
                    calculated[i] = false;
                }
            }

            //Sum and Substract
            if (exprOps.Contains("+") || exprOps.Contains("-"))
            {
                for (int i = 0; i < exprOps.Length; i++)
                {
                    if (exprOps[i] == "+")
                    {
                        if (!calculated[i] && !calculated[i + 1])
                        {
                            result += numbers[i] + numbers[i + 1];
                            calculated[i] = true;
                            calculated[i + 1] = true;
                        }
                        else if (!calculated[i])
                        {
                            result += numbers[i];
                            calculated[i] = true;
                        }
                        else if (!calculated[i + 1])
                        {
                            result += numbers[i + 1];
                            calculated[i + 1] = true;
                        }

                        if (i + 1 < exprOps.Length && (exprOps[i + 1] == "*" || exprOps[i + 1] == "m"))
                        {
                            for (int j = i + 1; j < exprOps.Length && (exprOps[j] == "*" || exprOps[j] == "m"); j++)
                            {
                                calculated[j + 1] = true;
                            }
                        }
                    }
                    else if (exprOps[i] == "-")
                    {
                        if (!calculated[i] && !calculated[i + 1])
                        {
                            result += numbers[i] - numbers[i + 1];
                            calculated[i] = true;
                            calculated[i + 1] = true;
                        }
                        else if (!calculated[i])
                        {
                            result -= numbers[i];
                            calculated[i] = true;
                        }
                        else if (!calculated[i + 1])
                        {
                            result -= numbers[i + 1];
                            calculated[i + 1] = true;
                        }

                        if (i + 1 < exprOps.Length && (exprOps[i + 1] == "*" || exprOps[i + 1] == "m"))
                        {
                            for (int j = i + 1; j < exprOps.Length && (exprOps[j] == "*" || exprOps[j] == "m"); j++)
                            {
                                calculated[j + 1] = true;
                            }
                        }
                    }
                }
            }

            if (result == 0 && exprOps.All(x => x == "m"))
            {
                result = int.Parse(number);
            }


            Console.WriteLine(result);

            if (result == searched)
            {
                k++;
            }
        }

        private static int IndexOf(string str)
        {
            int ind = 0;
            foreach (var item in exprOps)
            {
                if (item == str)
                {
                    break;
                }

                ind++;
            }

            return ind;
        }
    }
}