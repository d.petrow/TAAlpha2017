﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04.DogLovesPaper
{
    class DogLovesPaper
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            Dictionary<int, Node> accessNodes = new Dictionary<int, Node>();
            SortedSet<Node> graph = new SortedSet<Node>();
            StringBuilder result = new StringBuilder();

            for (int i = 0; i < n; i++)
            {
                string[] rule = Console.ReadLine().Split();
                int a = int.Parse(rule[0]);
                string position = rule[2];
                int b = int.Parse(rule[3]);

                Node first = null;
                if (!accessNodes.ContainsKey(a))
                {
                    first = new Node(a);
                    accessNodes.Add(a, first);
                }
                else
                {
                    first = accessNodes[a];
                }

                Node second = null;
                if (!accessNodes.ContainsKey(b))
                {
                    second = new Node(b);
                    accessNodes.Add(b, second);
                }
                else
                {
                    second = accessNodes[b];
                }

                if (position == "before")
                {
                    first.AddChild(second);
                    second.Parrents++;
                }
                else if (position == "after")
                {
                    second.AddChild(first);
                    first.Parrents++;
                }

                graph.Add(first);
                graph.Add(second);
            }

            while (true)
            {
                if (AllVisited(graph))
                {
                    break;
                }

                foreach (Node node in graph)
                {
                    if (!node.Visited && node.Parrents == 0 && (result.Length != 0 || node.Name != 0))
                    {
                        node.Visited = true;

                        result.Append(node.Name);

                        foreach (Node child in node.Childs)
                        {
                            child.Parrents--;
                        }

                        break;
                    }
                }
            }

            Console.WriteLine(result);
        }

        private static bool AllVisited(SortedSet<Node> graph)
        {
            foreach (Node node in graph)
            {
                if (!node.Visited)
                {
                    return false;
                }
            }

            return true;
        }
    }

    class Node : IComparable<Node>
    {
        public Node(int name)
        {
            this.Name = name;
            this.Childs = new List<Node>();
        }

        public int Name { get; }
        public List<Node> Childs { get; }
        public int Parrents { get; set; }
        public bool Visited { get; set; }

        public void AddChild(Node child)
        {
            this.Childs.Add(child);
        }

        public int CompareTo(Node other)
        {
            return this.Name.CompareTo(other.Name);
        }
    }
}
