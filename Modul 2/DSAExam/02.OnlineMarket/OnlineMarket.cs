﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wintellect.PowerCollections;

namespace _02.OnlineMarket
{
    class OnlineMarket
    {
        static OrderedSet<Product> market;
        static Dictionary<string, Product> productsByName;
        static Dictionary<string, OrderedSet<Product>> productsByType;
        static StringBuilder result;

        static string command;
        static string[] commandParams;
        static string name;
        static double price;
        static string type;

        static double from;
        static double to;

        static void Main(string[] args)
        {
            market = new OrderedSet<Product>();
            productsByName = new Dictionary<string, Product>();
            productsByType = new Dictionary<string, OrderedSet<Product>>();
            result = new StringBuilder();

            command = Console.ReadLine();

            name = string.Empty;
            price = new double();
            type = string.Empty;

            from = new double();
            to = new double();

            while (!command.StartsWith("end"))
            {
                if (command.StartsWith("add"))
                {
                    command = command.Remove(0, 4);
                    commandParams = command.Split();

                    name = commandParams[0];
                    price = double.Parse(commandParams[1]);
                    type = commandParams[2];

                    Product productToAdd = new Product(name, price, type);

                    AddProduct(productToAdd);
                }
                else if (command.StartsWith("filter by type"))
                {
                    command = command.Remove(0, 15);
                    commandParams = command.Split();

                    type = commandParams[0];

                    FilterByType();
                }
                else if (command.StartsWith("filter by price from"))
                {
                    command = command.Remove(0, 21);
                    commandParams = command.Split();

                    from = double.Parse(commandParams[0]);

                    if (commandParams.Length > 1)
                    {
                        to = double.Parse(commandParams[2]);

                        FilterFromTo();
                    }
                    else
                    {
                        FilterFrom();
                    }
                }
                else if (command.StartsWith("filter by price to"))
                {
                    command = command.Remove(0, 19);
                    commandParams = command.Split();

                    to = double.Parse(commandParams[0]);

                    FilterTo();
                }

                command = Console.ReadLine();
            }

            Console.Write(result);
        }
        private static void AddProduct(Product productToAdd)
        {
            if (!productsByName.ContainsKey(productToAdd.Name))
            {
                productsByName.Add(productToAdd.Name, productToAdd);
                market.Add(productToAdd);

                if (!productsByType.ContainsKey(productToAdd.Type))
                {
                    productsByType.Add(productToAdd.Type, new OrderedSet<Product>());
                }

                if (productsByType[productToAdd.Type].Count >= 10)
                {
                    Product lastProduct = productsByType[productToAdd.Type][productsByType[productToAdd.Type].Count - 1];

                    if (productToAdd.CompareTo(lastProduct) < 0)
                    {
                        productsByType[productToAdd.Type].RemoveLast();
                        productsByType[productToAdd.Type].Add(productToAdd);
                    }
                }
                else
                {
                    productsByType[productToAdd.Type].Add(productToAdd);
                }

                result.AppendLine($"Ok: Product {productToAdd.Name} added successfully");
            }
            else
            {
                result.AppendLine($"Error: Product {productToAdd.Name} already exists");
            }
        }

        private static void FilterByType()
        {
            if (productsByType.ContainsKey(type))
            {
                result.AppendLine($"Ok: {string.Join(", ", productsByType[type])}");
            }
            else
            {
                result.AppendLine($"Error: Type {type} does not exists");
            }
        }

        private static void FilterFromTo()
        {
            result.Append("Ok: ");

            int counter = 0;

            var list = new List<Product>();

            foreach (Product product in market)
            {
                if (counter == 10 || product.Price > to)
                {
                    break;
                }

                if (product.Price >= from)
                {
                    list.Add(product);
                    counter++;
                }
            }

            result.AppendLine(string.Join(", ", list));
        }

        private static void FilterFrom()
        {
            result.Append("Ok: ");

            int counter = 0;

            var list = new List<Product>();

            foreach (Product product in market)
            {
                if (counter == 10)
                {
                    break;
                }

                if (product.Price >= from)
                {
                    list.Add(product);
                    counter++;
                }
            }

            result.AppendLine(string.Join(", ", list));
        }

        private static void FilterTo()
        {
            result.Append("Ok: ");

            int counter = 0;

            var list = new List<Product>();

            foreach (Product product in market)
            {
                if (counter == 10 || product.Price > to)
                {
                    break;
                }

                list.Add(product);
                counter++;
            }

            result.AppendLine(string.Join(", ", list));
        }
    }

    class Product : IComparable<Product>
    {
        public Product(string name, double price, string type)
        {
            this.Name = name;
            this.Price = price;
            this.Type = type;
        }

        public string Name { get; }
        public double Price { get; }
        public string Type { get; }

        public int CompareTo(Product other)
        {
            int compareResult = this.Price.CompareTo(other.Price);

            if (compareResult == 0)
            {
                compareResult = this.Name.CompareTo(other.Name);
            }

            if (compareResult == 0)
            {
                compareResult = this.Type.CompareTo(other.Type);
            }

            return compareResult;
        }

        public override string ToString()
        {
            return $"{this.Name}({this.Price})";
        }
    }
}