﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictDemo
{
    class City
    {
        public City(string name, string country, int popul, int temp)
        {
            this.Name = name;
            this.Country = country;
            this.Population = popul;
            this.Temperature = temp;
        }

        public string Name { get; }
        public string Country { get; }
        public int Population { get; set; }
        public int Temperature { get; }

        public override string ToString()
        {
            return $"Name: {this.Name}, Country: {this.Country}, Population: {this.Population}, Temperature Today: {this.Temperature}, HashCode: {this.GetHashCode()}";
        }

        public override bool Equals(object obj)
        {
            return this.Population.Equals(((City)obj).Population);
        }

        public override int GetHashCode()
        {
            return this.Name.Length;
        }
    }


    class DictDemo
    {
        static void Main(string[] args)
        {
            //Dictionary<string, City> citiesInfo = new Dictionary<string, City>();

            //citiesInfo.Add("Sofia", new City("Sofia", "Bulgaria", 1000, 0));
            //citiesInfo.Add("Novi pazar", new City("Novi pazar", "Bulgaria", 1300, 2));
            //citiesInfo.Add("Varna", new City("Varna", "Bulgaria", 1400, -2));
            //citiesInfo.Add("Plovdiv", new City("Plovdiv", "Bulgaria", 1100, -10));
            //citiesInfo.Add("Vraca", new City("Vraca", "Bulgaria", 1200, 10));

            //foreach (var city in citiesInfo)
            //{
            //    Console.WriteLine(city.ToString());
            //}



            //citiesInfo["Sofia"].Population = 20000000;
            //Console.WriteLine();

            //foreach (var city in citiesInfo)
            //{
            //    Console.WriteLine(city.ToString());
            //}

            PrintMilionCities();
        }

        private static void PrintMilionCities()
        {
            var cities = new Dictionary<City, int>();

            for (int i = 1000000; i < 2000000; i++)
            {
                var city = new City(i.ToString(), "Bulgaria", 2000000, 6);
                cities.Add(city, i);
            }

            Console.WriteLine("All cities are created.");
        }

    }
}
