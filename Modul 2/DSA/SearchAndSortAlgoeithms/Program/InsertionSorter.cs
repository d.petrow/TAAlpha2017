﻿using System;
using System.Collections.Generic;

namespace Program
{
    static public class InsertionSorter<T> where T : IComparable<T>
    {
        static public void Sort(IList<T> unsorted)
        {
            int i = 1;

            while (i < unsorted.Count)
            {
                int j = i;

                while (j > 0 && unsorted[j - 1].CompareTo(unsorted[j]) > 0)
                {
                    Swap(unsorted, j, j - 1);
                    j = j - 1;
                }

                i = i + 1;
            }
        }

        private static void Swap(IList<T> unsorted, int i, int j)
        {
            T temp = unsorted[i];
            unsorted[i] = unsorted[j];
            unsorted[j] = temp;
        }
    }
}
