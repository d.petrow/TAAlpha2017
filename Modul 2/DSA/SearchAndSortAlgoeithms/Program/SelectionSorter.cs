﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    static public class SelectionSorter<T> where T : IComparable<T>
    {
        static public void Sort(IList<T> unsortedCollection)
        {
            int n = unsortedCollection.Count;

            for (int i = 0; i < n; i++)
            {
                int min = i;

                for (int j = i + 1; j < n; j++)
                {
                    if (unsortedCollection[min].CompareTo(unsortedCollection[j]) > 0)
                    {
                        min = j;
                    }
                }//Search For element After i with value less than the value from i

                if (min != i)
                {
                    Swap(unsortedCollection, i, min);
                }//When element with such value is found Swap their positions in the Array
            }
        }

        private static void Swap(IList<T> arr, int i, int min)
        {
            T temp = arr[i];
            arr[i] = arr[min];
            arr[min] = temp;
        }
    }
}
