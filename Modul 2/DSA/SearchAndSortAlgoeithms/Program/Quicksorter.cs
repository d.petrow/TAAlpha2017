﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    static public class Quicksorter<T> where T : IComparable<T>
    {
        static public void Sort(List<T> arr, int left, int right)
        {
            int n = right - left;

            if (n > 1)
            {
                T pivot = arr[right];
                int pivotIdex = right;
                int wallIndex = left;

                for (int i = left; i < right; i++)
                {
                    if (arr[i].CompareTo(pivot) <= 0)
                    {
                        Swap(arr, i, wallIndex);
                        wallIndex++;
                    }
                }

                Swap(arr, pivotIdex, wallIndex);
                
                Sort(arr, left, wallIndex - 1);
                Sort(arr, wallIndex + 1, right);
            }
        }

        private static void Swap(List<T> arr, int i, int wallIndex)
        {
            T temp = arr[i];
            arr[i] = arr[wallIndex];
            arr[wallIndex] = temp;
        }
    }
}
