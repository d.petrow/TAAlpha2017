﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Program
{
    static public class MergeSorter<T> where T : IComparable<T>
    {
        public static List<T> Sort(List<T> list)
        {
            var left = list.GetRange(0, list.Count / 2);
            var right = list.GetRange(list.Count / 2, list.Count / 2);

            var sortedList = Sort(left, right);

            return sortedList;
        }

        private static List<T> Sort(List<T> first, List<T> second)
        {
            var firstSorted = first;
            var secondSorted = second;

            if (first.Count > 1)
            {
                var left = first.Take(first.Count / 2).ToList();
                var right = first.Skip(first.Count / 2).ToList();
                firstSorted = Sort(left, right);
            }

            if (second.Count > 1)
            {
                var left = second.Take(second.Count / 2).ToList();
                var right = second.Skip(second.Count / 2).ToList();
                secondSorted = Sort(left, right);
            }

            var merged = Merge(firstSorted, secondSorted);

            return merged;
        }

        private static List<T> Merge(List<T> first, List<T> second)
        {
            var mergedList = new List<T>();

            int i = 0;
            int j = 0;

            while (true)
            {
                if (i == first.Count)
                {
                    mergedList.AddRange(second.Skip(j));
                    break;
                }
                else if (j == second.Count)
                {
                    mergedList.AddRange(first.Skip(i));
                    break;
                }

                if (first[i].CompareTo(second[j]) <= 0)
                {
                    mergedList.Add(first[i]);
                    i++;
                    continue;
                }
                else if (first[i].CompareTo(second[j]) > 0)
                {
                    mergedList.Add(second[j]);
                    j++;
                    continue;
                }
            }

            return mergedList;
        }
    }

    public class Person
    {
        private string age;

        private static string name;

        static Person()
        {
            name = "asdadas";
        }
    }
}
