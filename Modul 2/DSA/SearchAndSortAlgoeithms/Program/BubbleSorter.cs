﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    static public class BubbleSorter<T> where T : IComparable<T>
    {
        public static void Sort(IList<T> unsortedArr)
        {
            int n = unsortedArr.Count;

            bool swapped = true;

            while (swapped)
            {
                swapped = false;

                for (int i = 1; i < n; i++)
                {
                    if (unsortedArr[i - 1].CompareTo(unsortedArr[i]) > 0)
                    {
                        Swap(unsortedArr, i - 1, i);
                        swapped = true;
                    }
                }
            }
        }

        private static void Swap(IList<T> unsortedArr, int i, int j)
        {
            T temp = unsortedArr[i];
            unsortedArr[i] = unsortedArr[j];
            unsortedArr[j] = temp;
        }
    }
}
