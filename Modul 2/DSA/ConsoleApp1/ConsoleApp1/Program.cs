﻿using System;
using System.IO;
using System.Threading;

class Program
{
    static void Main(string[] args)
    {
        StreamReader sr = new StreamReader("../../TextFile1.txt");

        char[][] maze = new char[10][];
        bool[,] beenThere = new bool[10, 10];

        for (int i = 0; i < 10; i++)
        {
            maze[i] = sr.ReadLine().ToCharArray();
        }

        bool isSolvable = ExploreMaze(maze, beenThere, 2, 3);
    }

    static bool ExploreMaze(char[][] maze, bool[,] beenThere, int x, int y)
    {
        // If the current position is off the grid, then
        // we can't keep going on this path
        if ((y > 8 || y < 1) || (x < 1 || x > 8)) { return false; }

        // If the current position is a '*', then we
        // can't continue down this path
        if (maze[x][y] == '*') { return false; }

        //Check if Already been Here
        if (beenThere[x, y]) { return false; }
        else { beenThere[x, y] = true; }

        PrintMaze(maze, x, y);
        Thread.Sleep(500);

        // If the current position is an 'E', then 
        // we're at the end, so the maze is solveable.
        if (maze[x][y] == 'E')
        {
            return true;
        }


        // Otherwise, keep exploring by trying each possible
        // next decision from this point.  If any of the options
        // allow us to solve the maze, then return true.  We don't
        // have to worry about going off the grid or through a wall - 
        // we can trust our recursive call to handle those possibilities
        // correctly.
        if (ExploreMaze(maze, beenThere, x, y - 1)) { return true; } // search left
        if (ExploreMaze(maze, beenThere, x, y + 1)) { return true; } // search right
        if (ExploreMaze(maze, beenThere, x - 1, y)) { return true; }  // search up
        if (ExploreMaze(maze, beenThere, x + 1, y)) { return true; }  // search down

        // None of the options worked, so we can't solve the maze
        // using this path.
        return false;
    }

    private static void PrintMaze(char[][] maze, int x, int y)
    {
        for (int r = 0; r < maze.Length; r++)
        {
            for (int c = 0; c < maze[r].Length; c++)
            {
                if (r == x && c == y)
                {
                    Console.Write('H');
                }
                else
                {
                    Console.Write(maze[r][c]);
                }
            }
            Console.WriteLine();
        }
        Console.WriteLine("----------");
    }
}
