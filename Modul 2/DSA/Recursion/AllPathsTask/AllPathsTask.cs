﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AllPathsTask
{
    class AllPathsTask
    {
        static char[,] matrix;
        static bool[,] beenThere;
        static StringBuilder result;
        static StringBuilder path;
        static Random wallGenerator;

        static void Main(string[] args)
        {
            matrix = new char[20, 20];

            Initialize(matrix);
            wallGenerator = new Random();
            beenThere = new bool[matrix.GetLength(0), matrix.GetLength(1)];

            GenerateRandomLabyrinth(5, 5);

            Print(1, 1);

            //matrix[0, 0] = ' ';
            //matrix[19, 19] = 'E';
            ////{
            ////   { '+','-','-','+','-','-','-','-','+' },
            ////   { '|','E',' ','|',' ',' ',' ',' ','|' },
            ////   { '+','+',' ','+',' ','+','+',' ','|' },
            ////   { '|',' ',' ',' ',' ',' ',' ',' ','|' },
            ////   { '|',' ','+','-','-','-','+',' ','|' },
            ////   { '|',' ',' ','E',' ',' ',' ',' ','|' },
            ////   { '+','-','-','-','-','-','-','-','+' },
            ////};
            //result = new StringBuilder();
            //path = new StringBuilder();

            //FindPaths(0, 0);

            //Console.WriteLine(result);
        }

        private static void GenerateRandomLabyrinth(int row, int col, int prevR = -1, int prevC = -1)
        {
            if (IsValidSpace(row, col, prevR, prevC))
            {
                beenThere[row, col] = true;

                matrix[row, col] = '*';

                Print(1, 1);
                Thread.Sleep(500);

                prevR = row;
                prevC = col;

                switch (wallGenerator.Next(0, 4))
                {
                    case 0:
                        GenerateRandomLabyrinth(row + 1, col, prevR, prevC);
                        break;
                    case 1:
                        GenerateRandomLabyrinth(row, col - 1, prevR, prevC);
                        break;
                    case 2:
                        GenerateRandomLabyrinth(row - 1, col, prevR, prevC);
                        break;
                    case 3:
                        GenerateRandomLabyrinth(row, col + 1, prevR, prevC);
                        break;
                    default:
                        break;
                }
            }

        }

        private static bool IsValidSpace(int row, int col, int prevR, int prevC)
        {
            if (row > -1 && row < matrix.GetLength(0) && col > -1 && col < matrix.GetLength(1))
            {
                if (IsValid(row + 1, col, prevR, prevC) && IsValid(row, col - 1, prevR, prevC) && IsValid(row - 1, col, prevR, prevC) && IsValid(row, col + 1, prevR, prevC))
                {
                    return true;
                }
            }

            return false;
        }

        private static bool IsValid(int row, int col, int prevR, int prevC)
        {
            if (row > -1 && row < matrix.GetLength(0) && col > -1 && col < matrix.GetLength(1) && (!beenThere[row, col] || (row == prevR && col == prevC)))
            {
                return true;
            }

            return false;
        }

        private static void Initialize(char[,] matrix)
        {
            for (int r = 0; r < matrix.GetLength(0); r++)
            {
                for (int c = 0; c < matrix.GetLength(1); c++)
                {
                    matrix[r, c] = ' ';
                }
            }
        }

        private static void FindPaths(int row, int col, char dir = 'S')
        {

            if (IsValidPosition(row, col))
            {
                //Thread.Sleep(50);
                Print(row, col);

                if (matrix[row, col] == 'E')
                {
                    result.AppendLine(path.ToString());
                    Console.WriteLine(result);
                    return;
                }

                path.Append(dir);
                beenThere[row, col] = true;

                FindPaths(row - 1, col, 'U');
                FindPaths(row, col + 1, 'R');
                FindPaths(row + 1, col, 'D');
                FindPaths(row, col - 1, 'L');

                path.Length--;
                beenThere[row, col] = false;

                //Thread.Sleep(500);
                Print(row, col);

            }
        }

        private static bool IsValidPosition(int row, int col)
        {
            if (row > -1 && row < matrix.GetLength(0) && col > -1 && col < matrix.GetLength(1) && !beenThere[row, col] && (matrix[row, col] == ' ' || matrix[row, col] == 'E'))
            {
                return true;
            }

            return false;
        }

        private static void Print(int row, int col)
        {
            Console.SetCursorPosition(0, 0);
            for (int r = 0; r < matrix.GetLength(0); r++)
            {
                for (int c = 0; c < matrix.GetLength(1); c++)
                {
                    if (r == row && c == col)
                    {
                        Console.Write('H');
                    }
                    //else if (beenThere[r, c])
                    //{
                    //    Console.Write('0');
                    //}
                    else
                    {
                        Console.Write(matrix[r, c]);
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
