﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Portals
{
    class Portals
    {
        static int[] startCoordinates;
        static int[] labyrinthSizes;
        static int[,] labyrinth;
        static bool[,] isActive;
        static int maxPowerSum;

        static int counter = 0;

        static void Main(string[] args)
        {
            InputLabyrinth();

            MaxSumOfTeleportations(startCoordinates[0], startCoordinates[1]);

            Console.WriteLine(maxPowerSum);
        }

        private static void InputLabyrinth()
        {
            startCoordinates = Console.ReadLine().Split().Select(int.Parse).ToArray();
            labyrinthSizes = Console.ReadLine().Split().Select(int.Parse).ToArray();
            labyrinth = new int[labyrinthSizes[0], labyrinthSizes[1]];
            isActive = new bool[labyrinthSizes[0], labyrinthSizes[1]];
            maxPowerSum = 0;
            for (int i = 0; i < labyrinthSizes[0]; i++)
            {
                string[] singleRow = Console.ReadLine().Split(' ');
                bool isSwamp = new bool();
                for (int j = 0; j < labyrinthSizes[1]; j++)
                {
                    isSwamp = singleRow[j] == "#";

                    labyrinth[i, j] = !isSwamp ? int.Parse(singleRow[j]) : -1;
                    isActive[i, j] = !isSwamp;
                }
            }
        }

        private static void MaxSumOfTeleportations(int row, int col, int currentPowerSum = 0)
        {
            Console.WriteLine($"{++counter}. [{row}, {col}] {currentPowerSum}");

            if (currentPowerSum > maxPowerSum)
            {
                maxPowerSum = currentPowerSum;
            }

            int power = labyrinth[row, col];

            if (IsInLabyrinth(row + power, col) && isActive[row + power, col])
            {
                isActive[row + power, col] = false;
                MaxSumOfTeleportations(row + power, col, currentPowerSum + power);
                isActive[row + power, col] = true;
            }
            if (IsInLabyrinth(row, col + power) && isActive[row, col + power])
            {
                isActive[row, col + power] = false;
                MaxSumOfTeleportations(row, col + power, currentPowerSum + power);
                isActive[row, col + power] = true;
            }
            if (IsInLabyrinth(row - power, col) && isActive[row - power, col])
            {
                isActive[row - power, col] = false;
                MaxSumOfTeleportations(row - power, col, currentPowerSum + power);
                isActive[row - power, col] = true;
            }
            if (IsInLabyrinth(row, col - power) && isActive[row, col - power])
            {
                isActive[row, col - power] = false;
                MaxSumOfTeleportations(row, col - power, currentPowerSum + power);
                isActive[row, col - power] = true;
            }

            counter--;
        }

        private static bool IsInLabyrinth(int row, int col)
        {
            return row >= 0 && col >= 0 && row < labyrinthSizes[0] && col < labyrinthSizes[1];
        }
    }
}
