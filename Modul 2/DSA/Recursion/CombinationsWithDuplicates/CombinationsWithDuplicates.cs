﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombinationsWithDuplicates
{
    class CombinationsWithDuplicates
    {
        static int n;
        static int k;
        static int[] combBuilder;

        static void Main(string[] args)
        {
            n = 3;//int.Parse(Console.ReadLine());
            k = 2;//int.Parse(Console.ReadLine());
            combBuilder = new int[k];
            PrintCombsWithDublicates();
        }

        private static void PrintCombsWithDublicates(int currentIndex = 0, int startNumber = 1)
        {
            if (currentIndex == k)
            {
                Console.WriteLine("({0})", string.Join(", ", combBuilder));
                return;
            }
            //Bottom when Combination is found


            for (int currentNumber = startNumber; currentNumber <= n; currentNumber++)
            {
                combBuilder[currentIndex] = currentNumber;
                PrintCombsWithDublicates(currentIndex + 1, currentNumber);
            }
        }
    }
}
