﻿using System;
using System.Linq;

namespace Permutations
{
    class NextPermutation
    {
        static int n;
        static int[] frame;

        static void Main(string[] args)
        {
            n = int.Parse(Console.ReadLine());
            frame = Console.ReadLine().Split().Select(int.Parse).ToArray();

            Next();

            Console.WriteLine(string.Join(" ", frame));
        }

        private static void Next(int index = 0)
        {
            int k = n - 2;

            for (; k >= 0; k--)
            {
                if (frame[k] < frame[k + 1])
                {
                    for (int l = n - 1; l >= k + 1; l--)
                    {
                        if (frame[k] < frame[l])
                        {
                            Swap(frame, k, l);
                            //Swap the value of a[k] with that of a[l].
                            break;
                        }
                        //Find the largest index l greater than k such that frame[k] < frame[l].
                    }

                    for (int l = k + 1, end = n - 1; l < end; l++, end--)
                    {
                        Swap(frame, l, end);
                    }
                    //Reverse the sequence from a[k + 1] up to and including the final element a[n].

                    break;
                }
                //Find the largest index k such that frame[k] < frame[k + 1].
                //If no such index exists, the permutation is the last permutation
            }
        }

        private static void Swap(int[] frame, int k, int l)
        {
            int temp = frame[k];
            frame[k] = frame[l];
            frame[l] = temp;
        }
    }
}
