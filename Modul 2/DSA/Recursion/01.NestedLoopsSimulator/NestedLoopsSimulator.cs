﻿using System;

class RecursiveNestedLoops
{
    static int n;
    static int[] loops;

    static void Main()
    {
        n = int.Parse(Console.ReadLine());
        loops = new int[n];

        LoopSimulator(0);
    }

    private static void LoopSimulator(int loop)
    {
        if (loop == n)
        {
            Print();
            return;
        }

        for (int current = 1; current <= n; current++)
        {
            loops[loop] = current;
            LoopSimulator(loop + 1);
        }
    }

    private static void Print()
    {
        for (int i = 0; i < loops.Length; i++)
        {
            Console.Write(loops[i] + " ");
        }

        Console.WriteLine();
    }
}