﻿using System;
using System.Collections.Generic;
using System.Linq;

class Slogan
{
    static List<string> wordsAllowed;
    static string sloganSuggestion;
    static Stack<string> slogansBuilder;
    static HashSet<string> impossibleSlogans;


    static void Main(string[] args)
    {
        int n = int.Parse(Console.ReadLine());
        slogansBuilder = new Stack<string>();
        impossibleSlogans = new HashSet<string>();

        while (n != 0)
        {
            wordsAllowed = Console.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            sloganSuggestion = Console.ReadLine();
            wordsAllowed.Sort();

            if (CanCreateSlogan(sloganSuggestion))
            {
                PrintSlogan();
            }
            else
            {
                InvalidSlogan();
            }

            n--;
        }
    }

    private static bool CanCreateSlogan(string suggestion)
    {
        if (suggestion == string.Empty)
        {
            return true;
        }

        if (impossibleSlogans.Contains(suggestion))
        {
            return false;
        }


        foreach (var word in wordsAllowed)
        {
            if (suggestion.StartsWith(word))
            {
                bool created = CanCreateSlogan(suggestion.Substring(word.Length));

                if (created)
                {
                    slogansBuilder.Push(word);
                    return true;
                }
            }
        }

        impossibleSlogans.Add(suggestion);

        return false;
    }

    private static void PrintSlogan()
    {
        Console.Write(slogansBuilder.Pop());
        while (slogansBuilder.Count != 0)
        {
            Console.Write(" " + slogansBuilder.Pop());
        }
        Console.WriteLine();
    }

    private static void InvalidSlogan()
    {
        Console.WriteLine("NOT VALID");
        slogansBuilder.Clear();
    }
}
