﻿using System;
using System.Linq;

namespace Permutations
{
    class Permutations
    {
        static int n;
        static int[] frame;

        static void Main(string[] args)
        {
            n = int.Parse(Console.ReadLine());
            frame = new int[n];

            PrintPermutations();
        }

        private static void PrintPermutations(int index = 0)
        {
            if (index == n)
            {
                Console.WriteLine(string.Join(" ", frame));
                return;
            }

            for (int value = index; value <= n; value++)
            {
                if (!frame.Contains(value))
                {
                    frame[index] = value;
                    PrintPermutations(index + 1);
                    frame[index] = 0;
                }
            }
        }
    }
}
