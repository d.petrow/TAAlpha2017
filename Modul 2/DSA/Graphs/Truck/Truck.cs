﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wintellect.PowerCollections;

namespace Truck
{
    class Truck
    {
        static int[] destAndRoads;
        static IDictionary<int, Destination> map;
        static OrderedSet<int> bridgeHeights;
        static int upperBorderHeight;

        static void Main(string[] args)
        {
            ReadMap();

            Console.WriteLine(FindBiggestTruckHeight());
        }

        private static int FindBiggestTruckHeight()
        {
            int truckHeight = new int();

            upperBorderHeight = FindUpperBorderHeight();

            while (bridgeHeights.Count != 0)
            {
                truckHeight = bridgeHeights.RemoveLast();

                if (truckHeight > upperBorderHeight)
                {
                    continue;
                }

                if (DFS(truckHeight) == destAndRoads[0])
                {
                    break;
                }
            }

            return truckHeight;
        }

        private static int FindUpperBorderHeight()
        {
            int maxHeight = int.MaxValue;

            foreach (Destination destinations in map.Values)
            {
                int thisCityHighest = 0;

                foreach (Road road in destinations.Roads)
                {
                    if (thisCityHighest < road.BridgeHeight)
                    {
                        thisCityHighest = road.BridgeHeight;
                    }
                }

                if (thisCityHighest < maxHeight)
                {
                    maxHeight = thisCityHighest;
                }
            }

            return maxHeight;
        }

        private static int DFS(int truckHeight)
        {
            int countDestinations = 0;

            StackSet stackSet = new StackSet();
            stackSet.Push(map[1]);
            bool[] visited = new bool[destAndRoads[0]];

            while (stackSet.Count != 0)
            {
                Destination current = stackSet.Pop();
                visited[current.Name - 1] = true;
                countDestinations++;

                foreach (Road road in current.Roads)
                {
                    Destination other = road.OtherSideOf(current);

                    if (visited[other.Name - 1] || road.BridgeHeight < truckHeight)
                    {
                        continue;
                    }

                    stackSet.Push(other);
                }
            }

            return countDestinations;
        }

        private static void ReadMap()
        {
            destAndRoads = Console.ReadLine().Split().Select(int.Parse).ToArray();
            map = new Dictionary<int, Destination>();
            bridgeHeights = new OrderedSet<int>();

            for (int i = 0; i < destAndRoads[0]; i++)
            {
                map.Add(i + 1, new Destination(i + 1));
            }

            for (int i = 0; i < destAndRoads[1]; i++)
            {
                int[] connection = Console.ReadLine().Split().Select(int.Parse).ToArray();

                Destination firstDest = map[connection[0]];
                Destination secondDest = map[connection[1]];

                Road road = new Road(firstDest, secondDest, connection[2]);

                firstDest.Roads.Add(road);
                secondDest.Roads.Add(road);

                bridgeHeights.Add(connection[2]);
            }
        }
    }

    public class Destination
    {
        public Destination(int name)
        {
            this.Name = name;
            this.Roads = new List<Road>();
        }

        public int Name { get; }

        public IList<Road> Roads { get; }

        public override string ToString()
        {
            return $"Destination: {this.Name}";
        }
    }

    public class Road : IComparable<Road>
    {
        public Road(Destination first, Destination second, int bridgeHeight)
        {
            this.First = first;
            this.Second = second;

            this.BridgeHeight = bridgeHeight;
        }

        public Destination First { get; }
        public Destination Second { get; }
        public int BridgeHeight { get; }

        public int CompareTo(Road other)
        {
            return this.BridgeHeight.CompareTo(other.BridgeHeight);
        }

        public override string ToString()
        {
            return $"Road: {this.First.Name} - {this.Second.Name}; Bridge H: {this.BridgeHeight}";
        }

        public Destination OtherSideOf(Destination current)
        {
            if (current == First)
            {
                return Second;
            }
            else if (current == Second)
            {
                return First;
            }

            return null;
        }
    }

    class StackSet
    {
        private HashSet<Destination> set;
        private Stack<Destination> stack;

        public StackSet()
        {
            this.set = new HashSet<Destination>();
            this.stack = new Stack<Destination>();
        }

        public int Count { get { return this.stack.Count; } }

        public void Push(Destination destination)
        {
            if (!this.set.Contains(destination))
            {
                this.set.Add(destination);
                this.stack.Push(destination);
            }
        }

        public Destination Pop()
        {
            if (this.stack.Count > 0)
            {
                Destination destination = this.stack.Pop();
                this.set.Remove(destination);
                return destination;
            }

            throw new ArgumentException("Stack is Empty!");
        }
    }
}
