﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wintellect.PowerCollections;

namespace Beers
{
    class Beers
    {
        static List<Node> nodes;
        static Node start;
        static Node end;
        static OrderedSet<Node> sortedByDistanceFromStart;

        static void Main(string[] args)
        {
            int[] sizesAndBeers = Console.ReadLine().Split().Select(int.Parse).ToArray();

            int n = sizesAndBeers[0];
            int m = sizesAndBeers[1];
            int b = sizesAndBeers[2];

            nodes = new List<Node>();

            start = new Node(0, 0);
            end = new Node(n - 1, m - 1);

            sortedByDistanceFromStart = new OrderedSet<Node>();

            nodes.Add(start);
            nodes.Add(end);

            int[] coords;

            for (int i = 0; i < b; i++)
            {
                coords = Console.ReadLine().Split().Select(int.Parse).ToArray();

                nodes.Add(new Node(coords[0], coords[1], true));
            }

            Console.WriteLine(Dijkstra());
        }

        private static int Dijkstra()
        {
            Node current = start;
            current.DistanceFromStart = 0;
            current.IsCurrent = true;

            while (true)
            {
                foreach (Node node in nodes)
                {
                    if (!node.IsCurrent && !node.Visited)
                    {
                        int distanceBetweenNodes = CalculateDistance(current, node);

                        if (node.IsBeer)
                        {
                            distanceBetweenNodes -= 5;
                        }

                        if (node.DistanceFromStart > current.DistanceFromStart + distanceBetweenNodes)
                        {
                            node.DistanceFromStart = current.DistanceFromStart + distanceBetweenNodes;
                            sortedByDistanceFromStart.Add(node);
                        }
                    }
                }

                if (sortedByDistanceFromStart.Count == 0)
                {
                    break;
                }

                current.IsCurrent = false;
                current.Visited = true;
                current = sortedByDistanceFromStart.RemoveFirst();
                current.IsCurrent = true;
            }

            return end.DistanceFromStart;
        }

        private static int CalculateDistance(Node current, Node node)
        {
            return (Math.Max(current.X, node.X) - Math.Min(current.X, node.X)) + (Math.Max(current.Y, node.Y) - Math.Min(current.Y, node.Y));
        }
    }

    class Node : IComparable<Node>
    {
        public Node(int x, int y, bool isBeer = false)
        {
            this.X = x;
            this.Y = y;
            this.IsBeer = isBeer;
            this.DistanceFromStart = int.MaxValue;
        }

        public int X { get; }
        public int Y { get; }
        public int DistanceFromStart { get; set; }
        public bool IsCurrent { get; set; }
        public bool IsBeer { get; }
        public bool Visited { get; set; }

        public int CompareTo(Node other)
        {
            return this.DistanceFromStart.CompareTo(other.DistanceFromStart);
        }

        public override string ToString()
        {
            return $"[{this.X} {this.Y}]: {this.DistanceFromStart}";
        }
    }
}