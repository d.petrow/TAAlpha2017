﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace GraphImplementation
{
    public class Graph<T> where T : IComparable<T>
    {
        private readonly List<Node<T>> nodes;

        public Graph()
        {
            this.nodes = new List<Node<T>>();
        }
        public Graph(List<Node<T>> nodes) : this()
        {
            this.nodes = nodes;
        }
        public Graph(T[] nodes) : this()
        {
            for (int i = 0; i < nodes.Length; i++)
            {
                this.nodes.Add(new Node<T>(nodes[i]));
            }
        }

        public List<Node<T>> Nodes { get { return this.nodes; } }

        public void AddNode(Node<T> node)
        {
            this.nodes.Add(node);
        }

        public Node<T> GetByValue(T a)
        {
            foreach (Node<T> node in this.nodes)
            {
                if (node.Value.CompareTo(a) == 0)
                {
                    return node;
                }
            }

            return null;
        }
    }
}
