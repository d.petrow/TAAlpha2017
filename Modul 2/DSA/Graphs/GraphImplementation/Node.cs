﻿using System;
using System.Collections.Generic;

namespace GraphImplementation
{
    public class Node<T> : IComparable<Node<T>> where T : IComparable<T>
    {
        private readonly T value;
        private readonly List<Road<T>> edges;

        public Node(T value)
        {
            this.value = value;
            this.edges = new List<Road<T>>();
        }
        public Node(T value, List<Road<T>> edges) : this(value)
        {
            this.edges = edges;
        }

        public T Value { get { return this.value; } }
        public List<Road<T>> Roads { get { return this.edges; } }

        public void AddRoadTo(T weight, Node<T> neighbour)
        {
            var edge = new Road<T>(weight, this, neighbour);

            this.edges.Add(edge);
        }

        public int CompareTo(Node<T> other)
        {
            if (this.Value.CompareTo(other.Value) < 0)
            {
                return -1;
            }
            else if (this.Value.CompareTo(other.Value) > 0)
            {
                return 1;
            }
            else if (this.Value.CompareTo(other.Value) == 0)
            {
                return 0;
            }

            return 0;
        }
    }
}
