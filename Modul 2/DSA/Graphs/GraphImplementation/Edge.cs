﻿using System;

namespace GraphImplementation
{
    public class Road<T> where T : IComparable<T>
    {
        private readonly T weight;
        private readonly Node<T> attachedTo;
        private readonly Node<T> pointedTo;

        public Road(T weight)
        {
            this.weight = weight;
        }
        public Road(T weight, Node<T> toAttachTo, Node<T> toPointTo) : this(weight)
        {
            this.attachedTo = toAttachTo;
            this.pointedTo = toPointTo;
        }

        public T Weight { get { return this.weight; } }
        public Node<T> AttachedTo { get { return this.attachedTo; } }
        public Node<T> PointedTo { get { return this.pointedTo; } }


    }
}
