﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphImplementation
{
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        int[] input1 = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
    //        int numberOfActions = input1[0];
    //        int numberOfOrders = input1[1];
    //        var dict = new Dictionary<int, List<int>>(); // key => Node.Value  ; value => Node.AdjacencyList
    //        for (int i = 0; i < numberOfActions; i++)
    //        {
    //            dict.Add(i, new List<int>());
    //        }
    //        for (int i = 0; i < numberOfOrders; i++)
    //        {
    //            int[] input2 = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
    //            int first = input2[0];
    //            int second = input2[1];
    //            dict[second].Add(first);
    //        }

    //        // Sorting algorithm
    //        var result = new Queue<int>();
    //        for (int i = 0; i < numberOfActions; i++)
    //        {
    //            List<int> potentialStarts = new List<int>();
    //            foreach (var item in dict)
    //            {
    //                if (!item.Value.Any())
    //                {
    //                    potentialStarts.Add(item.Key);
    //                }
    //            }
    //            int first = potentialStarts.Min();
    //            result.Enqueue(first);
    //            dict.Remove(first);
    //            foreach (var item in dict)
    //            {
    //                if (item.Value.Contains(first))
    //                {
    //                    item.Value.Remove(first);
    //                }
    //            }
    //        }

    //        // Printing:
    //        for (int i = 0; i < numberOfActions; i++)
    //        {
    //            Console.WriteLine(result.Dequeue());
    //        }
    //    }
    //}


    class Program
    {
        static void Main(string[] args)
        {
            string[] inputNM = Console.ReadLine().Split(' ');

            int actionsCount = int.Parse(inputNM[0]);
            int requiredOrders = int.Parse(inputNM[1]);

            Graph<int> actions = new Graph<int>(Enumerable.Range(0, actionsCount).ToArray());

            int[][] requirements = new int[requiredOrders][];
            for (int i = 0; i < requiredOrders; i++)
            {
                requirements[i] = Console.ReadLine().Split().Select(int.Parse).ToArray();
            }

            int a = new int();
            int b = new int();
            for (int req = 0; req < requirements.Length; req++)
            {
                a = requirements[req][0];
                b = requirements[req][1];

                actions.GetByValue(a).AddRoadTo(0, actions.GetByValue(b));
            }

            List<Node<int>> ordered = new List<Node<int>>();


            while (actions.Nodes.Count != 0)
            {
                List<Node<int>> from = new List<Node<int>>();

                foreach (var node in actions.Nodes)
                {
                    foreach (var edge in node.Roads)
                    {
                        if (edge.PointedTo != null)
                        {
                            from.Add(node);
                            break;
                        }
                    }
                }

                for (int i = from.Count - 1; i >= 0; i--)
                {
                    bool removed = false;

                    foreach (var node in actions.Nodes)
                    {
                        foreach (var edge in node.Roads)
                        {
                            if (edge.PointedTo == from[i])
                            {
                                from.Remove(from[i]);

                                removed = true;
                                break;
                            }
                        }

                        if (removed)
                        {
                            break;
                        }
                    }
                }



                for (int i = 0; i < actions.Nodes.Count; i++)
                {
                    if (from.Contains(actions.Nodes[i]))
                    {
                        ordered.Add(actions.Nodes[i]);
                    }
                }

                foreach (var item in from)
                {
                    actions.Nodes.Remove(item);
                }

            }
        }
    }
}
