﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BridgesTask
{
    class BridgesTask
    {
        static int destinations;
        static int bridges;
        static Dictionary<int, Destination> map;
        static int busWeight;
        static Queue<Bridge> nonPassable;

        static void Main(string[] args)
        {
            InputData();

            Console.WriteLine(RepairBridges());
        }

        private static int RepairBridges()
        {
            int count = 0;

            Bridge currentBridge;

            while (nonPassable.Count > 0)
            {
                currentBridge = nonPassable.Dequeue();

                currentBridge.IsCurrent = true;

                if (!IsThereAnotherPathFromTo(currentBridge.FirstEnd, currentBridge.SecondEnd))
                {
                    count++;
                    currentBridge.Passable = true;
                }

                currentBridge.IsCurrent = false;
            }

            return count;
        }

        private static bool IsThereAnotherPathFromTo(Destination from, Destination to)
        {
            Stack<Destination> stack = new Stack<Destination>();
            bool[] visited = new bool[map.Count];

            stack.Push(from);
            visited[from.Name - 1] = true;

            while (stack.Count > 0)
            {
                Destination current = stack.Pop();

                foreach (Bridge bridge in current.Connections)
                {
                    if (!bridge.Passable || bridge.IsCurrent)
                    {
                        continue;
                    }

                    Destination otherSide = bridge.OtherEndOf(current);

                    if (otherSide == to)
                    {
                        return true;
                    }
                    else if (!visited[otherSide.Name - 1])
                    {
                        stack.Push(otherSide);
                        visited[otherSide.Name - 1] = true;
                    }
                }
            }

            return false;
        }

        private static void InputData()
        {
            int[] destinationsAndBrigdesCount = Console.ReadLine().Split().Select(int.Parse).ToArray();
            destinations = destinationsAndBrigdesCount[0];
            bridges = destinationsAndBrigdesCount[1];
            //Read Destinations and Bridges Count

            map = new Dictionary<int, Destination>();

            for (int i = 1; i <= destinations; i++)
            {
                map.Add(i, new Destination(i));
            }
            //Graph


            int[][] connections = new int[bridges][];
            for (int i = 0; i < bridges; i++)
            {
                connections[i] = Console.ReadLine().Split().Select(int.Parse).ToArray();
            }
            //Get Bridges

            busWeight = int.Parse(Console.ReadLine());
            //Bus Weight Set

            int firstEnd = new int();
            int secondEnd = new int();
            int maxWeight = new int();
            Bridge bridge;

            nonPassable = new Queue<Bridge>();

            foreach (var connection in connections)
            {
                firstEnd = connection[0];
                secondEnd = connection[1];
                maxWeight = connection[2];
                if (maxWeight < busWeight)
                {
                    bridge = new Bridge(maxWeight, false);
                    nonPassable.Enqueue(bridge);
                }
                else
                {
                    bridge = new Bridge(maxWeight, true);
                }

                map[firstEnd].ConnectWith(map[secondEnd], bridge);
            }
            //Connect Destinations with Bridges
        }
    }

    class Destination
    {
        private int name;
        private List<Bridge> connections;

        public Destination(int name)
        {
            this.name = name;
            this.connections = new List<Bridge>();
        }

        public int Name { get { return this.name; } }
        public List<Bridge> Connections { get { return this.connections; } }

        public void ConnectWith(Destination destination, Bridge bridge)
        {
            bridge.FirstEnd = this;
            bridge.SecondEnd = destination;

            this.Connections.Add(bridge);
            destination.Connections.Add(bridge);
        }

        public override string ToString()
        {
            return string.Format("Destination: {0}", this.name);
        }
    }

    class Bridge
    {
        private int maxWeight;
        private Destination firstEnd;
        private Destination secondEnd;
        private bool passable;
        private bool isCurrent;

        public Bridge(int maxWeight, bool passable)
        {
            this.maxWeight = maxWeight;
            this.passable = passable;
        }

        public int MaxWeight { get { return this.maxWeight; } }
        public Destination FirstEnd { get { return this.firstEnd; } set { this.firstEnd = value; } }
        public Destination SecondEnd { get { return this.secondEnd; } set { this.secondEnd = value; } }
        public bool Passable { get { return this.passable; } set { this.passable = value; } }
        public bool IsCurrent { get { return this.isCurrent; } set { this.isCurrent = value; } }

        public Destination OtherEndOf(Destination current)
        {
            if (current == firstEnd)
            {
                return secondEnd;
            }
            else if (current == secondEnd)
            {
                return firstEnd;
            }

            return null;
        }

        public override string ToString()
        {
            return string.Format("From: {0}, To: {1}, Passable: {2}", firstEnd.Name, secondEnd.Name, this.passable);
        }
    }
}
