﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wintellect.PowerCollections;

namespace Bike
{
    class Bike
    {
        static int rows;
        static int cols;
        static Tile[,] ground;

        static Tile start;
        static Tile end;

        static void Main(string[] args)
        {
            InputGround();

            double result = start.Height + FindPathWithLowestDamage() + end.Height;

            Console.WriteLine("{0:f2}", result);
        }

        private static double FindPathWithLowestDamage()
        {
            start.DamageFromStart = 0;

            OrderedBag<Tile> priorityQueue = new OrderedBag<Tile>();

            priorityQueue.Add(start);

            while (priorityQueue.Count != 0)
            {
                Tile current = priorityQueue.RemoveFirst();
                current.Visited = true;

                foreach (Tile neighbour in current.Neighbours)
                {
                    if (!neighbour.Visited && neighbour.DamageFromStart > current.DamageFromStart + Math.Abs(neighbour.Height - current.Height))
                    {
                        neighbour.DamageFromStart = current.DamageFromStart + Math.Abs(neighbour.Height - current.Height);

                        if (neighbour.DamageFromStart < end.DamageFromStart)
                        {
                            priorityQueue.Add(neighbour);
                        }
                    }
                }
            }

            return end.DamageFromStart;
        }

        private static void InputGround()
        {
            rows = int.Parse(Console.ReadLine());
            cols = int.Parse(Console.ReadLine());
            ground = new Tile[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                IList<double> singleRow = Console.ReadLine().Split().Select(double.Parse).ToList();

                for (int j = 0; j < cols; j++)
                {
                    ground[i, j] = new Tile(singleRow[j]);
                    Connect(i % 2 == 0, i, j);
                }
            }

            start = ground[0, 0];
            end = ground[rows - 1, cols - 1];
        }

        private static void Connect(bool isEvenRow, int r, int c)
        {
            int upper = r - 1;
            int right = c + 1;
            int left = c - 1;

            Tile currentHexagon = ground[r, c];

            if (!IsOutside(upper, c))
            {
                Tile neighbourHexagon = ground[upper, c];

                currentHexagon.ConnectWith(neighbourHexagon);
            }

            if (!IsOutside(r, left))
            {
                Tile neighbourHexagon = ground[r, left];

                currentHexagon.ConnectWith(neighbourHexagon);
            }

            if (isEvenRow)
            {
                if (!IsOutside(upper, left))
                {
                    Tile neighbourHexagon = ground[upper, left];

                    currentHexagon.ConnectWith(neighbourHexagon);
                }
            }
            else
            {
                if (!IsOutside(upper, right))
                {
                    Tile neighbourHexagon = ground[upper, right];

                    currentHexagon.ConnectWith(neighbourHexagon);
                }
            }
        }

        private static bool IsOutside(int r, int c)
        {
            return r < 0 || c < 0 || r >= rows || c >= cols;
        }
    }

    class Tile : IComparable<Tile>
    {
        public Tile(double h)
        {
            this.Height = h;
            this.Neighbours = new List<Tile>();
        }

        public double Height { get; }

        public IList<Tile> Neighbours { get; }

        public double DamageFromStart { get; set; } = double.MaxValue;

        public bool Visited { get; set; }

        public void ConnectWith(Tile neighbour)
        {
            this.Neighbours.Add(neighbour);
            neighbour.Neighbours.Add(this);
        }

        public override string ToString()
        {
            return $"{this.Height}: {this.DamageFromStart}";
        }

        public int CompareTo(Tile other)
        {
            return this.DamageFromStart.CompareTo(other.DamageFromStart);
        }
    }
}
