﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wintellect.PowerCollections;

namespace Friends
{
    class Friends
    {
        static int numberOfCities;
        static int numberOfConnections;

        static Dictionary<int, Town> map;

        static Town startCity;
        static Town finalCity;

        static Town firstIntermediateCity;
        static Town secondIntermediateCity;

        static OrderedBag<Town> distancesSorted;

        static void Main(string[] args)
        {
            CreateMap();
            ConnectCities();

            int shortestOne = 0;
            int shortestTwo = 0;

            shortestOne += DijkstraShortestPath(startCity, firstIntermediateCity);
            shortestTwo += DijkstraShortestPath(startCity, secondIntermediateCity);

            int intermediateCitiesDistance = DijkstraShortestPath(firstIntermediateCity, secondIntermediateCity);
            shortestOne += intermediateCitiesDistance;
            shortestTwo += intermediateCitiesDistance;

            shortestOne += DijkstraShortestPath(secondIntermediateCity, finalCity);
            shortestTwo += DijkstraShortestPath(firstIntermediateCity, finalCity);

            Console.WriteLine(Math.Min(shortestOne, shortestTwo));
        }

        private static int DijkstraShortestPath(Town start, Town end)
        {
            RefreshMap();
            start.DistanceFromStart = 0;
            end.BeenHere = false;

            while (true)
            {
                foreach (Road road in start.Roads)
                {
                    if (!road.ToTown.BeenHere && road.ToTown.DistanceFromStart > start.DistanceFromStart + road.Distance)
                    {
                        road.ToTown.DistanceFromStart = start.DistanceFromStart + road.Distance;

                        if (road.ToTown.DistanceFromStart < end.DistanceFromStart)
                        {
                            distancesSorted.Add(road.ToTown);
                        }
                    }
                }

                if (distancesSorted.Count == 0)
                {
                    break;
                }

                start = distancesSorted.RemoveFirst();
            }

            end.BeenHere = true;
            return end.DistanceFromStart;
        }

        private static void RefreshMap()
        {
            for (int i = 1; i <= numberOfCities; i++)
            {
                map[i].DistanceFromStart = int.MaxValue;
            }
        }

        private static void CreateMap()
        {
            int[] citiesAndConnections = Console.ReadLine().Split().Select(int.Parse).ToArray();
            numberOfCities = citiesAndConnections[0];
            numberOfConnections = citiesAndConnections[1];
            //Read N and M (N - numberOfCities, M - numberOfConnections)

            map = new Dictionary<int, Town>();

            for (int i = 1; i <= numberOfCities; i++)
            {
                map.Add(i, new Town(i));
            }
            //Create a Map with the Cities


            int[] startAndEndCityNames = Console.ReadLine().Split().Select(int.Parse).ToArray();

            startCity = map[startAndEndCityNames[0]];
            startCity.BeenHere = true;

            finalCity = map[startAndEndCityNames[1]];
            finalCity.BeenHere = true;
            //Read Start and Final Cities


            int[] intermediateCitiesNames = Console.ReadLine().Split().Select(int.Parse).ToArray();

            firstIntermediateCity = map[intermediateCitiesNames[0]];
            firstIntermediateCity.BeenHere = true;

            secondIntermediateCity = map[intermediateCitiesNames[1]];
            secondIntermediateCity.BeenHere = true;
            //Read Two Intermediate Cities between Start and Final Cities

            distancesSorted = new OrderedBag<Town>();
        }

        private static void ConnectCities()
        {
            int[] connection;
            Town firstCity;
            Town secondsCity;
            int distance = new int();

            for (int i = 0; i < numberOfConnections; i++)
            {
                connection = Console.ReadLine().Split().Select(int.Parse).ToArray();

                firstCity = map[connection[0]];
                secondsCity = map[connection[1]];
                distance = connection[2];

                firstCity.Connect(secondsCity, distance);
            }
            //Connect Cities with Roads and Set Distances
        }
    }
    class Town : IComparable<Town>
    {
        public Town(int name)
        {
            this.Name = name;
            this.Roads = new List<Road>();
            this.DistanceFromStart = int.MaxValue;
        }

        public int Name { get; }
        public List<Road> Roads { get; }
        public bool BeenHere { get; set; }
        public int DistanceFromStart { get; set; }

        public int CompareTo(Town other)
        {
            return this.DistanceFromStart.CompareTo(other.DistanceFromStart);
        }

        public void Connect(Town town, int distance)
        {
            this.Roads.Add(new Road(town, distance));
            town.Roads.Add(new Road(this, distance));
        }
    }

    class Road : IComparable<Road>
    {
        public Road(Town roadTo, int distance)
        {
            this.ToTown = roadTo;
            this.Distance = distance;
        }

        public Town ToTown { get; }
        public int Distance { get; }

        public int CompareTo(Road other)
        {
            return this.Distance.CompareTo(other.Distance);
        }
    }
}
