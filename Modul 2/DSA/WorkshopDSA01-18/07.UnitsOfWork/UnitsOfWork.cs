﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Wintellect.PowerCollections;

//namespace _07.UnitsOfWork
//{
//    struct GameUnit : IComparable<GameUnit>
//    {
//        public GameUnit(string name, string type, int attackPower)
//        {
//            this.Name = name;
//            this.Type = type;
//            this.AttackPower = attackPower;
//        }

//        public string Name { get; set; }
//        public string Type { get; set; }
//        public int AttackPower { get; set; }

//        public int CompareTo(GameUnit other)
//        {
//            int result = this.AttackPower.CompareTo(other.AttackPower) * -1;

//            if (result == 0)
//            {
//                result = this.Name.CompareTo(other.Name);
//            }

//            return result;
//        }

//        public override string ToString()
//        {
//            return string.Format("{0}[{1}]({2})", this.Name, this.Type, this.AttackPower);
//        }
//    }

//    class UnitsOfWork
//    {
//        static void Main(string[] args)
//        {
//            var unitsByName = new Dictionary<string, GameUnit>();
//            var sortedByAttackPower = new SortedSet<GameUnit>();
//            var unitsByType = new Dictionary<string, OrderedSet<GameUnit>>();

//            var result = new StringBuilder();


//            string[] input = Console.ReadLine().Split();

//            string command = input[0];
//            string name = string.Empty;
//            string type = string.Empty;
//            int attack = new int();
//            int numberOfUnits = new int();


//            while (command != "end")
//            {
//                if (command == "add")
//                {
//                    name = input[1];
//                    type = input[2];
//                    attack = int.Parse(input[3]);

//                    var newGameUnit = new GameUnit(name, type, attack);
//                    //Unit to Add

//                    Add(newGameUnit, unitsByName, sortedByAttackPower, unitsByType, result);
//                }
//                else if (command == "remove")
//                {
//                    name = input[1];
//                    Remove(unitsByName, sortedByAttackPower, unitsByType, result, name);
//                }
//                else if (command == "find")
//                {
//                    type = input[1];
//                    Find(unitsByType, result, type);
//                }
//                else if (command == "power")
//                {
//                    numberOfUnits = int.Parse(input[1]);
//                    result = Power(sortedByAttackPower, result, numberOfUnits);
//                }

//                input = Console.ReadLine().Split();
//                command = input[0];
//            }

//            Console.Write(result);
//        }

//        private static void Add(GameUnit unitToAdd, Dictionary<string, GameUnit> unitsByName, SortedSet<GameUnit> sortedByAttackPower, Dictionary<string, OrderedSet<GameUnit>> unitsByType, StringBuilder result)
//        {
//            if (!unitsByName.ContainsKey(unitToAdd.Name))
//            {
//                unitsByName.Add(unitToAdd.Name, unitToAdd);
//                //Add to Dictionary that can access Units by name

//                sortedByAttackPower.Add(unitToAdd);
//                //Add to SortedSet needed for the Power command

//                if (!unitsByType.ContainsKey(unitToAdd.Type))
//                {
//                    unitsByType.Add(unitToAdd.Type, new OrderedSet<GameUnit>());
//                }

//                unitsByType[unitToAdd.Type].Add(unitToAdd);
//                //Add to Dictionary that can access several Units with same type by type

//                result.AppendLine(string.Format("SUCCESS: {0} added!", unitToAdd.Name));
//            }
//            else
//            {
//                result.AppendLine(string.Format("FAIL: {0} already exists!", unitToAdd.Name));
//            }
//        }

//        private static void Remove(Dictionary<string, GameUnit> unitsByName, SortedSet<GameUnit> sortedByAttackPower, Dictionary<string, OrderedSet<GameUnit>> unitsByType, StringBuilder result, string name)
//        {
//            if (unitsByName.ContainsKey(name))
//            {
//                GameUnit unitToRemove = unitsByName[name];

//                unitsByName.Remove(name);
//                //Remove Unit from Dictionary by Name

//                sortedByAttackPower.Remove(unitToRemove);
//                //Remove Unit from SortedSet needed for the Power Command

//                unitsByType[unitToRemove.Type].Remove(unitToRemove);
//                //Remove Unit from OrderedSet in Dictionary by Type

//                result.AppendLine(string.Format("SUCCESS: {0} removed!", name));
//            }
//            else
//            {
//                result.AppendLine(string.Format("FAIL: {0} could not be found!", name));
//            }
//        }

//        private static void Find(Dictionary<string, OrderedSet<GameUnit>> unitsByType, StringBuilder result, string type)
//        {
//            result.Append("RESULT: ");

//            if (unitsByType.ContainsKey(type) && unitsByType[type].Any())
//            {
//                result.Append(string.Join(", ", unitsByType[type].Take(10)));
//            }

//            result.AppendLine();
//        }

//        private static StringBuilder Power(SortedSet<GameUnit> sortedByAttackPower, StringBuilder result, int numberOfUnits)
//        {
//            result.Append("RESULT: ");
//            result.Append(string.Join(", ", sortedByAttackPower.Count > numberOfUnits ? sortedByAttackPower.Take(numberOfUnits) : sortedByAttackPower));
//            result.AppendLine();

//            return result;
//        }
//    }
//}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wintellect.PowerCollections;

namespace _06.PlayerRanking
{
    //public class Player : IComparable<Player>
    //{
    //    public Player(string name, string type, int age)
    //    {
    //        this.Name = name;
    //        this.Type = type;
    //        this.Age = age;
    //    }

    //    public string Name { get; set; }
    //    public string Type { get; set; }
    //    public int Age { get; set; }

    //    public int CompareTo(Player second)
    //    {
    //        if (this.Name.CompareTo(second.Name) > 0)
    //        {
    //            return 1;
    //        }
    //        else if (this.Name.CompareTo(second.Name) < 0)
    //        {
    //            return -1;
    //        }
    //        else if (this.Name.CompareTo(second.Name) == 0)
    //        {
    //            if (this.Age > second.Age)
    //            {
    //                return -1;
    //            }
    //            else if (this.Age < second.Age)
    //            {
    //                return 1;
    //            }
    //            else if (this.Age == second.Age)
    //            {
    //                return 0;
    //            }
    //        }
    //        return 0;
    //    }
    //}
    public class Unit : IComparable<Unit>
    {
        public string Name { get; set; }
        public int Power { get; set; }
        public string Type { get; set; }


        public Unit(string name, int power, string type)
        {
            this.Name = name;
            this.Power = power;
            this.Type = type;

        }
        public int CompareTo(Unit other)
        {
            if (this.Power.CompareTo(other.Power) > 0)
            {
                return -1;
            }
            else if (this.Power.CompareTo(other.Power) < 0)
            {
                return 1;
            }
            else if (this.Power.CompareTo(other.Power) == 0)
            {
                if (this.Name.CompareTo(other.Name) > 0)
                {
                    return 1;
                }
                else if (this.Name.CompareTo(other.Name) < 0)
                {
                    return -1;
                }
                else if (this.Name.CompareTo(other.Name) == 0)
                {
                    return 0;
                }
            }
            return 0;
        }
    }

    class PlayerRanking
    {
        static void Main(string[] args)
        {
            string[] command = Console.ReadLine().Split();


            Dictionary<string, OrderedBag<Unit>> unitsByType = new Dictionary<string, OrderedBag<Unit>>();
            SortedSet<Unit> unitsByPower = new SortedSet<Unit>();
            StringBuilder result = new StringBuilder();

            string name;
            string type;
            int power;

            while (command[0] != "end")
            {
                switch (command[0])
                {

                    case "add":
                        name = command[1];
                        type = command[2];
                        power = int.Parse(command[3]);
                        Unit unitToAdd = new Unit(name, power, type);

                        if (unitsByType.ContainsKey(command[2]))
                        {
                            int checker = unitsByPower.Count();

                            unitsByPower.Add(unitToAdd);

                            if (unitsByPower.Count() == checker)
                            {
                                result.AppendLine(string.Format("FAIL: {0} already exists!", unitToAdd.Name));
                                //Console.WriteLine("FAIL: {0} already exists!", unitToAdd.Name);
                                break;
                            }
                            unitsByType[type].Add(unitToAdd);

                        }
                        else
                        {
                            unitsByType.Add(type, new OrderedBag<Unit>());
                            unitsByType[type].Add(unitToAdd);
                            unitsByPower.Add(unitToAdd);
                        }
                        result.AppendLine(string.Format("SUCCESS: {0} added!", unitToAdd.Name));
                        //Console.WriteLine("SUCCESS: {0} added!", unitToAdd.Name);

                        break;
                    case "remove":
                        name = command[1];
                        Unit itemToRemove = new Unit("", 0, "");
                        foreach (var item in unitsByPower)
                        {
                            if (item.Name == name)
                            {
                                itemToRemove = item;
                            }

                        }
                        if (itemToRemove.Name != "")
                        {
                            unitsByPower.Remove(itemToRemove);
                            unitsByType[itemToRemove.Type].Remove(itemToRemove);
                            result.AppendLine(string.Format("SUCCESS: {0} removed!", itemToRemove.Name));
                        }
                        else
                        {
                            result.AppendLine(string.Format("FAIL: {0} could not be found!", name));
                        }
                        //FAIL: Kerrigan could not be found!
                        //SUCCESS: XelNaga removed!
                        break;
                    case "find":
                        type = command[1];
                        result.Append(string.Format("RESULT: "));
                        //Console.Write("RESULT ");
                        if (unitsByType.ContainsKey(type))
                        {
                            foreach (var item in unitsByType[type])
                            {
                                //XelNaga[God](500),
                                result.Append(string.Format($"{item.Name}[{type}]({item.Power}), "));
                                //Console.Write($"{item.Name}[{type}]({item.Power}), ");

                            }
                            result.Length -= 2;
                            result.AppendLine();
                        }
                        else
                        {
                            result.AppendLine();
                        }

                        break;
                    case "power":
                        int numToShow = int.Parse(command[1]);
                        int counted = 0;
                        result.Append(string.Format("RESULT: "));
                        foreach (var item in unitsByPower)
                        {
                            result.Append(string.Format($"{item.Name}[{item.Type}]({item.Power}), "));
                            //Console.Write($"{item.Name}[{item.Type}]({item.Power}), ");
                            counted++;
                            if (counted == numToShow)
                            {
                                break;
                            }

                        }
                        result.Length -= 2;
                        result.AppendLine();
                        //Console.WriteLine();

                        break;


                }
                command = Console.ReadLine().Split();
            }
            Console.WriteLine(result);

        }
    }
}