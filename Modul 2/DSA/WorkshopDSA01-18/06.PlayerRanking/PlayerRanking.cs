﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wintellect.PowerCollections;

namespace _06.PlayerRanking
{
    public class Player : IComparable<Player>
    {
        public Player(string name, string type, int age)
        {
            this.Name = name;
            this.Type = type;
            this.Age = age;
        }

        public string Name { get; set; }
        public string Type { get; set; }
        public int Age { get; set; }

        public int CompareTo(Player second)
        {
            int result = this.Name.CompareTo(second.Name);

            if (result == 0)
            {
                result = this.Age.CompareTo(second.Age) * -1;
            }

            return result;
        }

        public override string ToString()
        {
            return $"{this.Name}({this.Age})";
        }
    }

    class PlayerRanking
    {
        static BigList<Player> ranklist;
        static Dictionary<string, OrderedBag<Player>> playersByType;
        static StringBuilder result;

        static void Main(string[] args)
        {
            ranklist = new BigList<Player>();
            playersByType = new Dictionary<string, OrderedBag<Player>>();
            result = new StringBuilder();

            string[] input = Console.ReadLine().Split();

            string command = input[0];
            string name = string.Empty;
            string type = string.Empty;
            int age = new int();
            int position = new int();

            while (command != "end")
            {
                if (command == "add")
                {
                    name = input[1];
                    type = input[2];
                    age = int.Parse(input[3]);
                    position = int.Parse(input[4]);

                    Player playerToAdd = new Player(name, type, age);

                    ranklist.Insert(position - 1, playerToAdd);
                    //Add player to ranklist

                    if (!playersByType.ContainsKey(type))
                    {
                        playersByType.Add(type, new OrderedBag<Player>());
                    }

                    if (playersByType[type].Count >= 5)
                    {
                        Player lastPlayer = playersByType[type][4];

                        if (playerToAdd.CompareTo(lastPlayer) < 0)
                        {
                            playersByType[type].RemoveLast();
                            playersByType[type].Add(playerToAdd);
                        }
                    }
                    else
                    {
                        playersByType[type].Add(playerToAdd);
                    }
                    //Add player to Dictionary by Type with OrderedSet

                    result.AppendLine($"Added player {name} to position {position}");
                }
                else if (command == "find")
                {
                    type = input[1];

                    result.Append($"Type {type}: ");

                    if (playersByType.ContainsKey(type))
                    {
                        result.Append(string.Join("; ", playersByType[type]));
                    }

                    result.AppendLine();
                }
                else if (command == "ranklist")
                {
                    int start = int.Parse(input[1]);
                    int end = int.Parse(input[2]);

                    var rankListPart = ranklist.Range(start - 1, end - start + 1);

                    foreach (Player player in rankListPart)
                    {
                        result.Append($"{start++}. {player}; ");

                    }

                    result.Remove(result.Length - 2, 2);
                    result.AppendLine();
                }

                input = Console.ReadLine().Split();
                command = input[0];
            }

            Console.Write(result);
        }
    }
}