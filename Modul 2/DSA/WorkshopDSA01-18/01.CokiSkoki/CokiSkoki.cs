﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01.CokiSkoki
{
    class CokiSkoki
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            int[] buildings = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
            int[] jumps = new int[n];

            Stack<int> indexes = new Stack<int>();

            int i = buildings.Length - 1;
            indexes.Push(i);
            i--;

            while (true)
            {
                if (buildings[i] < buildings[indexes.Peek()])
                {
                    indexes.Push(i);
                    i--;
                }
                else
                {
                    jumps[indexes.Pop()] = indexes.Count;

                    if (indexes.Count == 0)
                    {
                        indexes.Push(i);
                        i--;
                    }
                }

                if (i < 0)
                {
                    while (indexes.Count != 0)
                    {
                        jumps[indexes.Pop()] = indexes.Count;
                    }

                    break;
                }
            }

            Console.WriteLine(jumps.Max());
            Console.WriteLine(string.Join(" ", jumps));
        }
    }
}
