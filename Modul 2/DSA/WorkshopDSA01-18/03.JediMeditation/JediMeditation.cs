﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.JediMeditation
{
    class JediMeditation
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            Queue<string> jedi = new Queue<string>(Console.ReadLine().Split(' ').ToArray());

            Queue<string> masters = new Queue<string>();
            Queue<string> knights = new Queue<string>();
            Queue<string> padawans = new Queue<string>();


            while (jedi.Count != 0)
            {
                string aJedi = jedi.Dequeue();
                char jediRank = aJedi[0];

                switch (jediRank)
                {
                    case 'M':
                        masters.Enqueue(aJedi);
                        break;
                    case 'K':
                        knights.Enqueue(aJedi);
                        break;
                    case 'P':
                        padawans.Enqueue(aJedi);
                        break;
                }
            }

            Console.Write(string.Join(" ", masters));
            Console.Write(" ");
            Console.Write(string.Join(" ", knights));
            Console.Write(" ");
            Console.Write(string.Join(" ", padawans));
        }
    }
}
