﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05.Actions
{
    class Actions
    {
        static void Main(string[] args)
        {
            int[] input = Console.ReadLine().Split().Select(int.Parse).ToArray();
            int actionsCount = input[0];
            int requirements = input[1];

            int[] actions = Enumerable.Range(0, actionsCount).ToArray();
            List<int>[] children = new List<int>[actionsCount];
            for (int i = 0; i < actionsCount; i++)
            {
                children[i] = new List<int>();
            }
            int[] parentsCount = new int[actionsCount];
            bool[] visited = new bool[actionsCount];

            for (int i = 0; i < requirements; i++)
            {
                input = Console.ReadLine().Split().Select(int.Parse).ToArray();
                int parent = input[0];
                int child = input[1];

                parentsCount[child]++;
                children[parent].Add(child);
            }


            for (int i = 0; i < actionsCount; i++)
            {
                if (!visited[i] && parentsCount[i] == 0)
                {
                    visited[i] = true;

                    Console.WriteLine(i);

                    for (int child = children[i].Count - 1; child >= 0; child--)
                    {
                        parentsCount[children[i][child]]--;
                    }

                    i = -1;
                }
            }


        }
    }
}
