﻿using System;
using System.Collections.Generic;
using System.Text;

class ExspressionInBrackets
{
    static void Main(string[] args)
    {
        string exprs = Console.ReadLine();//"(1+(2-(2+3)*4/(3+1))*5)";
        StringBuilder result = new StringBuilder();

        Stack<int> brackets = new Stack<int>();

        for (int ch = 0; ch < exprs.Length; ch++)
        {
            if (exprs[ch] == '(')
            {
                brackets.Push(ch);
            }
            else if (exprs[ch] == ')')
            {
                int top = brackets.Pop();

                for (int i = top; i <= ch; i++)
                {
                    result.Append(exprs[i]);
                }

                result.Append("\n");

            }
        }

        System.Console.WriteLine(result.ToString().Trim(' ', '|'));
    }
}