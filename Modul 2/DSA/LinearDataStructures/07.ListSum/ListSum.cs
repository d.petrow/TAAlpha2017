﻿using System;
using System.Collections.Generic;

namespace _07.ListSum
{
    class ListSum
    {
        static void Main(string[] args)
        {
            List<int> sequence = new List<int>();
            string input = Console.ReadLine();

            while (input != "")
            {
                int number = int.Parse(input);

                sequence.Add(number);

                input = Console.ReadLine();
            }

            Console.WriteLine(CalculateSumInCollection(sequence));
            Console.WriteLine(CalculateAverageInCollection(sequence));
        }

        private static double CalculateAverageInCollection(List<int> sequence)
        {
            return CalculateSumInCollection(sequence) / sequence.Count;
        }

        private static double CalculateSumInCollection(List<int> sequence)
        {
            var sum = 0;
            foreach (var item in sequence)
            {
                sum += item;
            }
            return sum;
        }
    }
}
