﻿using System;
using System.Collections.Generic;

class FindPrimes
{
    static void Main(string[] args)
    {
        List<int> list = new List<int>();

        bool isPrime;
        for (int i = 200; i <= 300; i++)
        {
            isPrime = true;

            for (int div = 2; div < Math.Sqrt(i); div++)
            {
                if (i % div == 0)
                {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime)
            {
                list.Add(i);
            }
        }
    }
}
