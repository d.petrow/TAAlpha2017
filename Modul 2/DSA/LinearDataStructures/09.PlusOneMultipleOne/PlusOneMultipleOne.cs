﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09.PlusOneMultipleOne
{
    class PlusOneMultipleOne
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();
            int n = int.Parse(input.Split(' ')[0].ToString());
            int m = int.Parse(input.Split(' ')[1].ToString());

            Queue<int> queue = new Queue<int>();
            queue.Enqueue(n);

            int inTurn = new int();

            for (int iter = 0; iter < m; iter++)
            {
                inTurn = queue.Dequeue();

                queue.Enqueue(inTurn + 1);
                queue.Enqueue(2 * inTurn + 1);
                queue.Enqueue(inTurn + 2);
            }

            Console.WriteLine(inTurn);
        }
    }
}
