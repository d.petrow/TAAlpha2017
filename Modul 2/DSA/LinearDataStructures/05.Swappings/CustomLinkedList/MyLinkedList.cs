﻿using System;
using System.Collections.Generic;
using System.Text;

public class MyLinkedList
{
    public MyLinkedList()
    {
        this.Nodes = new List<Node>();
    }

    public List<Node> Nodes { get; }
    public Node First { get; private set; }
    public Node Last { get; private set; }

    public void AddFirst(Node node)
    {
        if (this.Nodes.Count == 0)
        {
            this.Last = node;
        }
        else
        {
            this.First.AttachLeft(node);
        }

        this.Nodes.Add(node);
        this.First = node;
    }

    public void AddLast(Node node)
    {
        if (this.Nodes.Count == 0)
        {
            this.First = node;
        }
        else
        {
            this.Last.AttachRight(node);
        }

        this.Nodes.Add(node);
        this.Last = node;
    }

    public void Swap(int value)
    {
        Node swapNode = this.Nodes[value - 1];

        if (swapNode != null)
        {
            if (swapNode == this.First)
            {
                Node right = swapNode.Right;

                swapNode.DetachRight();
                swapNode.AttachLeft(this.Last);

                this.First = right;
                this.Last = swapNode;
            }
            else if (swapNode == this.Last)
            {
                Node left = swapNode.Left;

                swapNode.DetachLeft();
                swapNode.AttachRight(this.First);

                this.First = swapNode;
                this.Last = left;
            }
            else
            {
                Node left = swapNode.Left;
                Node right = swapNode.Right;

                swapNode.DetachLeft();
                swapNode.DetachRight();

                swapNode.AttachLeft(this.Last);
                swapNode.AttachRight(this.First);

                this.First = right;
                this.Last = left;
            }
        }
    }

    public void Reverse()
    {
        Node prev = this.First.Left;
        Node curr = this.First;
        Node next = this.First.Right;

        Reverse(prev, curr, next);
    }

    private void Reverse(Node prev, Node curr, Node next)
    {
        if (next != null)
        {
            curr.DetachRight();
            curr.DetachLeft();

            curr.AttachRight(prev);
            curr.AttachLeft(next);

            Reverse(curr, curr.Right, next.Right);
        }

        return;
    }

    public override string ToString()
    {
        Node start = this.First;
        StringBuilder sb = new StringBuilder();
        sb.Append(start.Value + " ");

        while (start.Right != null)
        {
            sb.Append(start.Right.Value + " ");
            start = start.Right;
        }

        return sb.ToString().Trim();
    }
}
