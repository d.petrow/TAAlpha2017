﻿public class Node
{
    public Node(int val)
    {
        this.Value = val;
        this.Left = null;
        this.Right = null;
    }

    public int Value { get; }
    public Node Right { get; private set; }
    public Node Left { get; private set; }


    public void AttachLeft(Node leftNode)
    {
        if (leftNode != null)
        {
            this.Left = leftNode;
            leftNode.Right = this;
        }
    }

    public void AttachRight(Node rightNode)
    {
        if (rightNode != null)
        {
            this.Right = rightNode;
            rightNode.Left = this;
        }
    }

    public bool DetachLeft()
    {
        if (this.Left != null)
        {
            this.Left.Right = null;
            this.Left = null;

            return true;
        }
        return false;
    }

    public bool DetachRight()
    {
        if (this.Right != null)
        {
            this.Right.Left = null;
            this.Right = null;

            return true;
        }

        return false;
    }
}
