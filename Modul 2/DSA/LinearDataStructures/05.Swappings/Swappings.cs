﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        int n = int.Parse(Console.ReadLine());
        MyLinkedList numbers = new MyLinkedList();
        for (int i = 0; i < n; i++)
        {
            numbers.AddLast(new Node(i + 1));
        }

        Queue<int> swapNumbers = new Queue<int>(Console.ReadLine().Split(' ').Select(int.Parse).ToArray());
        while (swapNumbers.Count != 0)
        {
            numbers.Swap(swapNumbers.Dequeue());
        }

        Console.WriteLine(numbers.ToString());
    }
}
