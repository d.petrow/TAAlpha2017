﻿using System;

class PathToOne
{
    static void Main(string[] args)
    {
        uint n = uint.Parse(Console.ReadLine());

        Console.WriteLine(OpsToOne(n));
    }

    private static int OpsToOne(uint n)
    {
        int operations = 0;

        if (n > 1u)
        {
            if (n % 2 == 0)
            {
                operations++;

                operations += OpsToOne(n / 2);
            }
            else
            {
                int ops1 = OpsToOne(n + 1);
                int ops2 = OpsToOne(n - 1);

                if (ops1 > ops2)
                {
                    operations++;

                    operations += ops2;
                }
                else
                {
                    operations++;

                    operations += ops1;
                }
            }
        }

        return operations;
    }
}

