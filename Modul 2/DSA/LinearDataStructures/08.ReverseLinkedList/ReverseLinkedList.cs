﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08.ReverseLinkedList
{
    class SinglyLinkedList<T>
    {
        private readonly List<SLLNode<T>> nodes;

        public SinglyLinkedList()
        {
            this.nodes = new List<SLLNode<T>>();
        }
        public SinglyLinkedList(IList<T> collection) : this()
        {
            foreach (var item in collection)
            {
                this.AddLast(item);
            }
        }


        public List<SLLNode<T>> Nodes { get { return this.nodes; } }
        public SLLNode<T> Head { get; private set; }
        public SLLNode<T> Tail { get; private set; }

        public void AddFirst(T value)
        {
            if (this.nodes.Count == 0)
            {
                this.AddFirstNode(value);
                return;
            }

            SLLNode<T> toBeAdded = new SLLNode<T>(value);

            toBeAdded.Next = this.Head;
            this.Head = toBeAdded;

            this.nodes.Add(toBeAdded);
        }

        public void AddLast(T value)
        {
            if (this.nodes.Count == 0)
            {
                this.AddFirstNode(value);
                return;
            }

            SLLNode<T> toBeAdded = new SLLNode<T>(value);

            this.Tail.Next = toBeAdded;
            this.Tail = toBeAdded;

            this.nodes.Add(toBeAdded);
        }

        private void AddFirstNode(T value)
        {
            SLLNode<T> firstNode = new SLLNode<T>(value);

            this.Head = firstNode;
            this.Tail = firstNode;

            this.nodes.Add(firstNode);
        }

        public void Reverse()
        {
            SLLNode<T> prev = null;
            SLLNode<T> curr = this.Head;
            SLLNode<T> next;

            this.Tail = curr;

            while (curr != null)
            {
                next = curr.Next;
                curr.Next = prev;
                prev = curr;
                curr = next;
            }

            this.Head = prev;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            SLLNode<T> node = this.Head;
            sb.Append(node.Value + " ");

            while (node.Next != null)
            {
                node = node.Next;
                sb.Append(node.Value + " ");
            }

            return sb.ToString().Trim();
        }
    }

    class SLLNode<T>
    {
        private T value;
        private SLLNode<T> next;

        public SLLNode(T value)
        {
            this.value = value;
        }

        public T Value { get { return this.value; } }
        public SLLNode<T> Next { get { return this.next; } set { this.next = value; } }
    }

    class ReverseLinkedList
    {
        static void Main(string[] args)
        {
            int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 2, 45, 23, 4, 5, 3, 534, 5 };
            SinglyLinkedList<int> toBeReversed = new SinglyLinkedList<int>(arr);

            Console.WriteLine(toBeReversed.ToString());

            toBeReversed.Reverse();

            Console.WriteLine(toBeReversed.ToString());
        }
    }
}
