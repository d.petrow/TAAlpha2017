namespace CodeFirstDB.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRequiredValidations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Addresses", "Text", c => c.String(nullable: false));
            AlterColumn("dbo.Towns", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Departments", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Employees", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.Employees", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.Employees", "JobTitle", c => c.String(nullable: false));
            AlterColumn("dbo.Projects", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Projects", "Description", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Projects", "Description", c => c.String());
            AlterColumn("dbo.Projects", "Name", c => c.String());
            AlterColumn("dbo.Employees", "JobTitle", c => c.String());
            AlterColumn("dbo.Employees", "LastName", c => c.String());
            AlterColumn("dbo.Employees", "FirstName", c => c.String());
            AlterColumn("dbo.Departments", "Name", c => c.String());
            AlterColumn("dbo.Towns", "Name", c => c.String());
            AlterColumn("dbo.Addresses", "Text", c => c.String());
        }
    }
}
