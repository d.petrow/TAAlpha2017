namespace CodeFirstDB.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectEndDateAllowNulls : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Projects", "EndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Projects", "EndDate", c => c.DateTime(nullable: false));
        }
    }
}
