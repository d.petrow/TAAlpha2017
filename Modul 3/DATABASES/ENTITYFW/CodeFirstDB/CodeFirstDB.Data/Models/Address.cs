﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirstDB.Data.Models
{
    public class Address
    {
        public int Id { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public int TownId { get; set; }

        public Town Town { get; set; }
    }
}
