﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeFirstDB.Data.Models
{
    public class Project
    {
        public Project()
        {
            this.Employees = new HashSet<Employee>();
        }

        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        public Nullable<DateTime> EndDate { get; set; }

        public ICollection<Employee> Employees { get; }
    }
}
