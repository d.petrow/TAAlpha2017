﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirstDB.Data.Models
{
    public class Town
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
