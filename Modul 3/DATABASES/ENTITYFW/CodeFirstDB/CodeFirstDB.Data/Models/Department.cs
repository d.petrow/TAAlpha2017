﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirstDB.Data.Models
{
    public class Department
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int ManagerId { get; set; }

        public Employee Manager { get; set; }
    }
}
