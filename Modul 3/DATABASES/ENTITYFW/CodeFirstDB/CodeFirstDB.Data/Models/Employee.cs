﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeFirstDB.Data.Models
{
    public class Employee
    {
        public Employee()
        {
            this.Projects = new HashSet<Project>();
        }

        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string MiddleName { get; set; }

        [Required]
        public string JobTitle { get; set; }

        [Required]
        public DateTime HireDate { get; set; }

        [Required]
        public decimal Salary { get; set; }

        [Required]
        public int DepartmentId { get; set; }
        public Department Department { get; set; }

        public int ManagerId { get; set; }
        public Employee Manager { get; set; }

        [Required]
        public int AddressId { get; set; }

        public Address Address { get; set; }

        public ICollection<Project> Projects { get; }
    }
}
