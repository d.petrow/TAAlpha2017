﻿using CodeFirstDB.Data.Migrations;
using CodeFirstDB.Data.Models;
using System.Data.Entity;

namespace CodeFirstDB.Data
{
    public class TAContext : DbContext
    {
        public TAContext()
            : base("name=TelerikAcademyDB")
        {
            var strategy = new MigrateDatabaseToLatestVersion<TAContext, Configuration>();
            Database.SetInitializer(strategy);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Employee>()
                .HasOptional(x => x.Department)
                .WithRequired(x => x.Manager);
            
            base.OnModelCreating(modelBuilder);
        }

        public IDbSet<Employee> Employees { get; set; }
        public IDbSet<Department> Departments { get; set; }
        public IDbSet<Project> Projects { get; set; }
        public IDbSet<Address> Addresses { get; set; }
        public IDbSet<Town> Towns { get; set; }
    }
}
