﻿using CodeFirstDB.Data;
using CodeFirstDB.Data.Models;
using System.Data.Entity.Validation;
using System.Linq;

namespace CodeFirstDB.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new TAContext();

            var towns = context.Towns.ToList();
            towns.Clear();

            context.SaveChanges();
        }
    }
}
