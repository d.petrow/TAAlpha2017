namespace Telebook.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateDbTelebook : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Friendships",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SenderId = c.Int(nullable: false),
                        ReceiverId = c.Int(nullable: false),
                        ApprovedStatus = c.String(),
                        FriendsSince = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.ReceiverId)
                .ForeignKey("dbo.UserProfiles", t => t.SenderId)
                .Index(t => t.SenderId)
                .Index(t => t.ReceiverId);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AuthorId = c.Int(nullable: false),
                        FriendshipId = c.Int(nullable: false),
                        Content = c.String(nullable: false),
                        SentOn = c.DateTime(nullable: false),
                        SeenOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.AuthorId, cascadeDelete: true)
                .ForeignKey("dbo.Friendships", t => t.FriendshipId, cascadeDelete: true)
                .Index(t => t.AuthorId)
                .Index(t => t.FriendshipId);
            
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false, maxLength: 20),
                        Firstname = c.String(maxLength: 50),
                        Lastname = c.String(maxLength: 50),
                        RegisteredOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Username, unique: true);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                        PostedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImageUrl = c.String(nullable: false),
                        FileExtension = c.String(nullable: false, maxLength: 4),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.PostUserProfiles",
                c => new
                    {
                        Post_Id = c.Int(nullable: false),
                        UserProfile_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Post_Id, t.UserProfile_Id })
                .ForeignKey("dbo.Posts", t => t.Post_Id, cascadeDelete: true)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfile_Id, cascadeDelete: true)
                .Index(t => t.Post_Id)
                .Index(t => t.UserProfile_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Images", "UserId", "dbo.UserProfiles");
            DropForeignKey("dbo.Friendships", "SenderId", "dbo.UserProfiles");
            DropForeignKey("dbo.Friendships", "ReceiverId", "dbo.UserProfiles");
            DropForeignKey("dbo.Messages", "FriendshipId", "dbo.Friendships");
            DropForeignKey("dbo.Messages", "AuthorId", "dbo.UserProfiles");
            DropForeignKey("dbo.PostUserProfiles", "UserProfile_Id", "dbo.UserProfiles");
            DropForeignKey("dbo.PostUserProfiles", "Post_Id", "dbo.Posts");
            DropIndex("dbo.PostUserProfiles", new[] { "UserProfile_Id" });
            DropIndex("dbo.PostUserProfiles", new[] { "Post_Id" });
            DropIndex("dbo.Images", new[] { "UserId" });
            DropIndex("dbo.UserProfiles", new[] { "Username" });
            DropIndex("dbo.Messages", new[] { "FriendshipId" });
            DropIndex("dbo.Messages", new[] { "AuthorId" });
            DropIndex("dbo.Friendships", new[] { "ReceiverId" });
            DropIndex("dbo.Friendships", new[] { "SenderId" });
            DropTable("dbo.PostUserProfiles");
            DropTable("dbo.Images");
            DropTable("dbo.Posts");
            DropTable("dbo.UserProfiles");
            DropTable("dbo.Messages");
            DropTable("dbo.Friendships");
        }
    }
}
