﻿using System.Data.Entity;
using Telebook.Data.Migrations;
using Telebook.Data.Models;

namespace Telebook.Data
{
    public class TelebookContext : DbContext
    {
        public TelebookContext()
            : base("name=TelebookDB")
        {
            var strategy = new MigrateDatabaseToLatestVersion<TelebookContext, Configuration>();
            Database.SetInitializer(strategy);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Friendship> Friendships { get; set; }
    }
}
