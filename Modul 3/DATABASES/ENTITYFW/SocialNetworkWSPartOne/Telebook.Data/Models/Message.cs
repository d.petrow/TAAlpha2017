﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Telebook.Data.Models
{
    public class Message
    {
        public int Id { get; set; }

        [Required]
        public int AuthorId { get; set; }

        public UserProfile Author { get; set; }

        [Required]
        public int FriendshipId { get; set; }

        public Friendship Friendship { get; set; }

        [Required]
        [MinLength(1)]
        public string Conent { get; set; }

        public DateTime SentOn { get; set; }

        public Nullable<DateTime> SeenOn { get; set; }
    }
}
