﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Telebook.Data.Models
{
    public class Friendship
    {
        public Friendship()
        {
            this.Messages = new HashSet<Message>();
        }

        public int Id { get; set; }

        [Required]
        public int SenderId { get; set; }

        public UserProfile Sender { get; set; }

        [Required]
        public int ReceiverId { get; set; }

        public UserProfile Receiver { get; set; }

        public string ApprovedStatus { get; set; }

        public Nullable<DateTime> FriendsSince { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
    }
}
