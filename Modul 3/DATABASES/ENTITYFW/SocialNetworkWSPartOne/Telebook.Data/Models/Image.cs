﻿using System.ComponentModel.DataAnnotations;

namespace Telebook.Data.Models
{
    public class Image
    {
        public int Id { get; set; }

        [Required]
        public string ImageUrl { get; set; }

        [Required]
        [MaxLength(4)]
        [RegularExpression(@"(?<=)[.](\w{1,3})")]
        public string FileExtension { get; set; }

        public int UserId { get; set; }

        public UserProfile User { get; set; }
    }
}
