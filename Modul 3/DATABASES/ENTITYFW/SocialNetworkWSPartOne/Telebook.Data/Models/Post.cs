﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Telebook.Data.Models
{
    public class Post
    {
        public Post()
        {
            this.TaggedUsers = new HashSet<UserProfile>();
        }

        public int Id { get; set; }

        [Required]
        [MinLength(10)]
        public string Content { get; set; }

        public DateTime PostedOn { get; set; }

        public virtual ICollection<UserProfile> TaggedUsers { get; set; }
    }
}
