SELECT m.FirstName, m.LastName
FROM Employees e
	JOIN Employees m
		ON e.ReportsTo = m.EmployeeID
GROUP BY m.FirstName, m.LastName 
HAVING COUNT(e.EmployeeID) > 2
ORDER BY m.FirstName