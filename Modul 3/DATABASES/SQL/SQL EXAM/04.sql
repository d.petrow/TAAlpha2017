SELECT p.ProductName, p.UnitPrice, c.CategoryName 
FROM Products p
	JOIN Categories c
		ON p.CategoryID = c.CategoryID
WHERE p.UnitPrice = 
				(SELECT MAX(UnitPrice)
				FROM Products
				WHERE CategoryID = p.CategoryID)
ORDER BY p.UnitPrice