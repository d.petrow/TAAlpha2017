SELECT TOP (1) sh.CompanyName
FROM Orders o
	JOIN Shippers sh
		ON o.ShipVia = sh.ShipperID
GROUP BY sh.CompanyName
ORDER BY COUNT(o.OrderID)