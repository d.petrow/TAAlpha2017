SELECT *

FROM Employees employees
LEFT OUTER JOIN Employees managers
ON employees.ManagerID = managers.EmployeeID

--FROM Employees managers
--RIGHT JOIN Employees employees
--ON employees.ManagerID = managers.EmployeeID