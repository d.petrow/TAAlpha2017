SELECT employees.FirstName, 
		employees.LastName, 
		employeesAddresses.AddressText, 
		managers.FirstName, 
		managers.LastName, 
		managersAddresses.AddressText
FROM Employees employees

INNER JOIN Addresses employeesAddresses
ON employees.AddressID = employeesAddresses.AddressID

INNER JOIN Employees managers
ON employees.ManagerID = managers.EmployeeID

INNER JOIN Addresses managersAddresses
ON managers.AddressID = managersAddresses.AddressID