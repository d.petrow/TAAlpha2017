SELECT *
FROM Employees employees
INNER JOIN Employees managers
ON employees.ManagerID = managers.EmployeeID
ORDER BY employees.EmployeeID