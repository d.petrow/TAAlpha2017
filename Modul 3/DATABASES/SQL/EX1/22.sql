SELECT *
FROM Employees employees
INNER JOIN Departments departments
ON employees.DepartmentID = departments.DepartmentID
WHERE employees.DepartmentID = 10 OR employees.DepartmentID = 3
		AND 
		HireDate BETWEEN '1995' AND '2005'
ORDER BY employees.EmployeeID