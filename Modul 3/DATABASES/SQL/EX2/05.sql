SELECT AVG(Salary)
FROM Employees
INNER JOIN Departments
ON Employees.DepartmentID = Departments.DepartmentID
WHERE Departments.Name = 'SALES'