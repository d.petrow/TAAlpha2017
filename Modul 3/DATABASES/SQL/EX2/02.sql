SELECT e.FirstName, e.LastName, e.Salary
FROM Employees e
WHERE e.Salary < 1.1 * (SELECT MIN(Salary)
							FROM Employees)