SELECT e.FirstName + ' ' + e.LastName AS FullName, e.Salary, e.JobTitle, d.Name
FROM Employees e
	JOIN Departments d
		ON d.DepartmentID = e.DepartmentID
ORDER BY d.DepartmentID, e.JobTitle

SELECT AVG(e.Salary), e.JobTitle, d.Name
FROM Employees e
	JOIN Departments d
		ON d.DepartmentID = e.DepartmentID
GROUP BY d.DepartmentID, d.Name, e.JobTitle
ORDER BY d.DepartmentID, e.JobTitle