INSERT INTO Users (UserName, PassWord, FirstName, LastName)
			SELECT LOWER(LEFT(e.FirstName, 1) + e.LastName) + CONVERT(VARCHAR(30), e.EmployeeID), LOWER(e.FirstName + e.LastName),
					e.FirstName, e.LastName
			FROM TelerikAcademy.dbo.Employees e