SELECT e.FirstName, e.LastName
FROM Employees e
GROUP BY e.FirstName, e.LastName
HAVING LEN(LastName) = 5