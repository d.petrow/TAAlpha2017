CREATE TABLE Users (
	UserID uniqueidentifier NOT NULL DEFAULT NEWID(),
	UserName varchar(20) NOT NULL UNIQUE,
	PassWord varchar(60) NOT NULL,
	FirstName nvarchar(20) NOT NULL,
	MiddleName nvarchar(20),
	LastName nvarchar(20) NOT NULL,
	LastLoginTime smalldatetime NOT NULL,

	PRIMARY KEY (UserID),
	CHECK (LEN(PassWord) >= 5)
)