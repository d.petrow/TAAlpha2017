ALTER TABLE Users
ADD GroupID uniqueidentifier

ALTER TABLE Users
ADD FOREIGN KEY (GroupID) REFERENCES Groups(GroupID)