SELECT COUNT(employees.EmployeeID)
FROM Employees employees
LEFT JOIN Employees managers
ON employees.ManagerID = managers.EmployeeID
WHERE employees.ManagerID IS NULL