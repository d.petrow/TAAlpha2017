SELECT d.Name, AVG(e.Salary) AS AvgSalaries
FROM Departments d
	JOIN Employees e
		ON d.DepartmentID = e.DepartmentID
GROUP BY d.Name, d.DepartmentID
ORDER BY d.DepartmentID