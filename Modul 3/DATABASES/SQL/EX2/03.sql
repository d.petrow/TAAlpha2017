SELECT e.FirstName, e.LastName, d.Name, e.Salary
FROM Employees e
INNER JOIN Departments d
ON e.DepartmentID = d.DepartmentID
WHERE e.Salary =
	(SELECT MIN(Salary)
	FROM Employees
	WHERE e.DepartmentID = DepartmentID)
ORDER BY e.DepartmentID
