SELECT d.Name, t.Name, COUNT(e.EmployeeID) AS EmpCountByDep
FROM Employees e
	JOIN Departments d
		ON e.DepartmentID = d.DepartmentID
	JOIN Addresses a
		ON a.AddressID = e.AddressID
	JOIN Towns t
		ON t.TownID = a.TownID
GROUP BY d.Name, t.Name, d.DepartmentID
ORDER BY d.DepartmentID