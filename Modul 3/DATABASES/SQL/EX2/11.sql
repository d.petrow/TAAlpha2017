SELECT e.FirstName + ' ' + e.LastName AS EmployeeName, m.FirstName + ' ' + m.LastName AS ManagerName
FROM Employees e JOIN Employees m
ON e.ManagerID = m.EmployeeID
ORDER BY m.EmployeeID

SELECT m.FirstName, m.LastName
FROM Employees e
	JOIN Employees m
		ON e.ManagerID = m.EmployeeID
GROUP BY m.EmployeeID, m.FirstName, m.LastName
HAVING COUNT(e.EmployeeID) = 5
ORDER BY m.EmployeeID