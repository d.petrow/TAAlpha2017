DECLARE @i int
SET @i = 1
WHILE @i <= 9
BEGIN
	UPDATE Users
	SET GroupID = (SELECT g.GroupID
					FROM Groups g
					WHERE g.Name = 'Group' + CONVERT(VARCHAR(3), @i))
	WHERE RIGHT(LastName, 1) = CONVERT(VARCHAR(3), @i)

	SET @i += 1
END