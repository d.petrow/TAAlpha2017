SELECT m.EmployeeID, t.Name
FROM Employees e
	JOIN Employees m
		ON e.ManagerID = m.EmployeeID
	JOIN Addresses a
		ON m.AddressID = a.AddressID
	JOIN Towns t
		ON t.TownID = a.TownID
GROUP BY t.Name, m.EmployeeID
ORDER BY t.Name