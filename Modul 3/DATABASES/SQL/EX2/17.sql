CREATE TABLE Groups (
	GroupID uniqueidentifier NOT NULL DEFAULT NEWID(),
	Name nvarchar(40) NOT NULL UNIQUE,

	PRIMARY KEY (GroupID)
)