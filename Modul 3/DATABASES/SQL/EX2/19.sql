INSERT INTO Users (UserName, PassWord, FirstName, MiddleName, LastName, LastLoginTime) 
VALUES ('d.petrow','12345678', 'Dimitar', 'Nikolaev', 'Petrov', GETDATE())


DECLARE @FromDate date = '2018-03-01'
DECLARE @ToDate date = '2018-03-08'
DECLARE @i int
SET @i = 1
WHILE @i <= 1000
BEGIN
	INSERT INTO Users (UserName, PassWord, FirstName, LastName, LastLoginTime) 
	VALUES ('user' + CONVERT(VARCHAR(3), @i),'12345678', 'Name' + CONVERT(VARCHAR(3), @i), 'LastName' + CONVERT(VARCHAR(3), @i),
			DATEADD(day, 
               RAND(CHECKSUM(NEWID()))*(1 + DATEDIFF(day, @FromDate, @ToDate)), 
               @FromDate))

	SET @i += 1
END
--Insert Users

DECLARE @i int
SET @i = 1
WHILE @i <= 50
BEGIN
	INSERT INTO Groups (Name) 
	VALUES ('Group' + CONVERT(VARCHAR(3), @i))

	SET @i += 1
END
--Insert Groups