CREATE VIEW [Users LoggedIn Today] AS
SELECT *
FROM Users
WHERE LastLoginTime >= dateadd(dd, datediff(dd, 0, getdate()), 0)
and LastLoginTime < dateadd(dd, datediff(dd, 0, getdate()), +1)



SELECT *
FROM [Users LoggedIn Today]