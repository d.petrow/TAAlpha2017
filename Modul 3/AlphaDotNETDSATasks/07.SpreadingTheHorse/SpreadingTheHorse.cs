﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07.SpreadingTheHorse
{
    class SpreadingTheHorse
    {
        static int rowsCount;
        static int colsCount;

        static int rowStart;
        static int colStart;

        static int[,] matrix;
        static bool[,] hasJumpedHere;

        static int possibleMovesCount;
        static int[] xMoves;
        static int[] yMoves;

        static void Main(string[] args)
        {
            ReadMatrix();

            var startCell = new Cell() { X = rowStart, Y = colStart };
            Jump(startCell);

            PrintResult(colsCount / 2);
        }

        private static void PrintResult(int col)
        {
            for (int row = 0; row < rowsCount; row++)
            {
                Console.WriteLine(matrix[row, col]);
            }
        }

        private static void ReadMatrix()
        {
            rowsCount = int.Parse(Console.ReadLine());
            colsCount = int.Parse(Console.ReadLine());

            rowStart = int.Parse(Console.ReadLine());
            colStart = int.Parse(Console.ReadLine());

            matrix = new int[rowsCount, colsCount];
            for (int r = 0; r < rowsCount; r++)
            {
                for (int c = 0; c < colsCount; c++)
                {
                    matrix[r, c] = 1;
                }
            }
            hasJumpedHere = new bool[rowsCount, colsCount];

            possibleMovesCount = 8;
            xMoves = new int[] { -2, -1, 1, 2, 2, 1, -1, -2 };
            yMoves = new int[] { 1, 2, 2, 1, -1, -2, -2, -1 };
        }

        private static void Jump(Cell cell)
        {
            var cells = new Queue<Cell>();
            cells.Enqueue(cell);
            hasJumpedHere[cell.X, cell.Y] = true;

            while (cells.Count != 0)
            {
                var currentCell = cells.Dequeue();
                int v = matrix[currentCell.X, currentCell.Y];

                for (int i = 0; i < possibleMovesCount; i++)
                {
                    if (!CanJumpHere(currentCell.X + xMoves[i], currentCell.Y + yMoves[i])
                        && !hasJumpedHere[currentCell.X + xMoves[i], currentCell.Y + yMoves[i]])
                    {
                        matrix[currentCell.X + xMoves[i], currentCell.Y + yMoves[i]] += v;

                        cells.Enqueue(new Cell()
                        {
                            X = currentCell.X + xMoves[i],
                            Y = currentCell.Y + yMoves[i]
                        });

                        hasJumpedHere[currentCell.X + xMoves[i], currentCell.Y + yMoves[i]] = true;
                    }
                }

                PrintMatrix();
            }
        }

        private static void PrintMatrix()
        {
            for (int r = 0; r < rowsCount; r++)
            {
                for (int c = 0; c < colsCount; c++)
                {
                    if (matrix[r, c] == 0)
                    {
                        Console.Write("- ");
                        continue;
                    }

                    Console.Write(matrix[r, c] + " ");
                }
                Console.WriteLine();
            }

            Console.WriteLine();
            Console.WriteLine();
        }

        private static bool CanJumpHere(int x, int y)
        {
            return x < 0 || y < 0 || x >= rowsCount || y >= colsCount;
        }
    }

    struct Cell
    {
        public int X;
        public int Y;
    }
}
