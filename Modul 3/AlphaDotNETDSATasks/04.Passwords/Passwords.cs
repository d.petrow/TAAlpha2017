﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04.Passwords
{
    class Passwords
    {
        static int n;
        static string relations;
        static int k;

        static char[] keys;
        static int counter;

        static void Main(string[] args)
        {
            InputData();

            if (FindPassword(9, keys[9].ToString()))
            {
                return;
            }

            for (int keyInd = 0; keyInd < keys.Length - 1; keyInd++)
            {
                if (FindPassword(keyInd, keys[keyInd].ToString()))
                {
                    return;
                }
            }
        }

        private static void InputData()
        {
            n = int.Parse(Console.ReadLine());
            relations = Console.ReadLine();
            k = int.Parse(Console.ReadLine());

            keys = "1234567890".ToCharArray();
            counter = 1;
        }

        private static bool FindPassword(int keyInd, string password, int relInd = 0)
        {
            if (relInd == n - 1)
            {
                if (counter == k)
                {
                    Console.WriteLine(password);
                    return true;
                }

                counter++;

                return false;
            }

            if (relations[relInd] == '>')
            {
                for (int i = keyInd; i < keys.Length - 1; i++)
                {
                    if (i == keyInd)
                    {
                        if (FindPassword(9, password + keys[9], relInd + 1))
                        {
                            return true;
                        }

                        continue;
                    }

                    if (FindPassword(i, password + keys[i], relInd + 1))
                    {
                        return true;
                    }
                }
            }
            else if (relations[relInd] == '<')
            {
                for (int i = 0; i < keyInd; i++)
                {
                    if (FindPassword(i, password + keys[i], relInd + 1))
                    {
                        return true;
                    }
                }
            }
            else if (relations[relInd] == '=')
            {
                if (FindPassword(keyInd, password + keys[keyInd], relInd + 1))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
