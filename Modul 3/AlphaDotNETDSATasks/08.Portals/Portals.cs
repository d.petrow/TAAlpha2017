﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08.Portals
{
    class Portals
    {
        static int[] startPosition;
        static int[] labyrinthSizes;
        static string[,] labyrinth;
        static bool[,] isUsed;

        static int maxPowerUsed;

        static void Main(string[] args)
        {
            ReadLabyrinth();

            Teleport(startPosition[0], startPosition[1]);

            Console.WriteLine(maxPowerUsed);
        }

        private static void PrintState(int currentPower)
        {
            for (int r = 0; r < labyrinthSizes[0]; r++)
            {
                for (int c = 0; c < labyrinthSizes[1]; c++)
                {
                    Console.Write(labyrinth[r, c] + " ");
                }

                Console.Write("| ");

                for (int c = 0; c < labyrinthSizes[1]; c++)
                {
                    if (isUsed[r, c])
                    {
                        Console.Write("@ ");
                    }
                    else
                    {
                        Console.Write("_ ");
                    }
                }

                Console.WriteLine();
            }

            Console.WriteLine();
            Console.WriteLine(currentPower);
            Console.WriteLine(maxPowerUsed);
            Console.WriteLine();
            Console.WriteLine();
        }

        private static void Teleport(int row, int col, int currentPower = 0)
        {
            //PrintState(currentPower);

            if (currentPower > maxPowerUsed)
            {
                maxPowerUsed = currentPower;
            }

            if (isUsed[row, col])
            {
                return;
            }

            var power = int.Parse(labyrinth[row, col]);

            isUsed[row, col] = true;

            if (IsValidPosition(row - power, col))
            {
                Teleport(row - power, col, currentPower + power);
            }
            if (IsValidPosition(row, col + power))
            {
                Teleport(row, col + power, currentPower + power);
            }
            if (IsValidPosition(row + power, col))
            {
                Teleport(row + power, col, currentPower + power);
            }
            if (IsValidPosition(row, col - power))
            {
                Teleport(row, col - power, currentPower + power);
            }

            isUsed[row, col] = false;
        }

        private static bool IsValidPosition(int row, int col)
        {
            return row >= 0 && col >= 0 && row < labyrinthSizes[0] && col < labyrinthSizes[1] && labyrinth[row, col] != "#";
        }

        private static void ReadLabyrinth()
        {
            startPosition = Console.ReadLine().Split().Select(int.Parse).ToArray();
            labyrinthSizes = Console.ReadLine().Split().Select(int.Parse).ToArray();

            labyrinth = new string[labyrinthSizes[0], labyrinthSizes[1]];
            isUsed = new bool[labyrinthSizes[0], labyrinthSizes[1]];

            for (int r = 0; r < labyrinthSizes[0]; r++)
            {
                var row = Console.ReadLine().Split();
                for (int c = 0; c < labyrinthSizes[1]; c++)
                {
                    labyrinth[r, c] = row[c];
                }
            }

            maxPowerUsed = 0;
        }
    }
}
