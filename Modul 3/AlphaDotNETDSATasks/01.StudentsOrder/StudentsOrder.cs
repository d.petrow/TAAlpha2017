﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _01.StudentsOrder
{
    struct StudentsOrder
    {
        static List<int> alphasAndOrdersCount;
        static List<Alpha> alphaInput;
        static Dictionary<string, Alpha> alphasDict;
        static string[] order;

        static void Main(string[] args)
        {
            GetAlphas();
            OrderAlphas();

            Alpha head = null;
            foreach (var alpha in alphaInput)
            {
                if (alpha.prev == null)
                {
                    head = alpha;
                    break;
                }
            }

            StringBuilder sb = new StringBuilder();
            while (head != null)
            {
                sb.Append($"{head.name} ");

                head = head.next;
            }
            
            Console.WriteLine(sb.ToString().Trim());
        }

        private static void OrderAlphas()
        {
            for (int i = 0; i < alphasAndOrdersCount[1]; i++)
            {
                order = Console.ReadLine().Split();
                var alpha = alphasDict[order[0]];
                var target = alphasDict[order[1]];

                SwapAlphas(alpha, target);
            }
        }

        private static void SwapAlphas(Alpha alpha, Alpha target)
        {
            if (alpha.next == target)
            {
                return;
            }

            alpha.Detach();

            alpha.AtachTo(target);
        }

        private static void GetAlphas()
        {
            alphasAndOrdersCount = Console.ReadLine().Split().Select(int.Parse).ToList();
            alphaInput = Console.ReadLine()
                .Split()
                .Select(x => new Alpha() { name = x, })
                .ToList();

            for (int i = 0; i < alphasAndOrdersCount[0]; i++)
            {
                if (i == 0)
                {
                    alphaInput[i].next = alphaInput[i + 1];
                    continue;
                }
                else if (i == alphasAndOrdersCount[0] - 1)
                {
                    alphaInput[i].prev = alphaInput[i - 1];
                    continue;
                }

                alphaInput[i].next = alphaInput[i + 1];
                alphaInput[i].prev = alphaInput[i - 1];
            }

            alphasDict = alphaInput.ToDictionary(x => x.name);
        }
    }

    class Alpha
    {
        public string name;
        public Alpha prev;
        public Alpha next;

        public override string ToString()
        {
            return this.name;
        }

        internal void AtachTo(Alpha target)
        {
            if (target.prev != null)
            {
                target.prev.next = this;
                this.prev = target.prev;
            }

            target.prev = this;
            this.next = target;
        }

        internal void Detach()
        {
            if (this.next != null)
            {
                this.next.prev = this.prev;
            }

            if (this.prev != null)
            {
                this.prev.next = this.next;
            }

            this.next = null;
            this.prev = null;
        }
    }
}
