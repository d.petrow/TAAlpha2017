﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wintellect.PowerCollections;

namespace _02.OnlineMarket
{
    class OnlineMarket
    {
        static Market market;
        static StringBuilder log;

        static void Main(string[] args)
        {
            IEnumerable<string> line;
            string commandName = string.Empty;
            IList<string> parameters;

            market = new Market();
            log = new StringBuilder();

            do
            {
                line = Console.ReadLine().Split();
                commandName = line.First();

                switch (commandName)
                {
                    case "add":
                        parameters = line.Skip(1).ToList();
                        AddProductCommand(parameters);
                        break;
                    case "filter":
                        parameters = line.Skip(2).ToList();
                        FilterCommand(parameters);
                        break;
                    default:
                        break;
                }

            } while (commandName != "end");

            Console.WriteLine(log.ToString());
        }

        private static void FilterCommand(IList<string> parameters)
        {
            var filterType = parameters[0];

            switch (filterType)
            {
                case "type":
                    FilterByType(parameters[1]);
                    break;
                case "price":
                    FIlterByPrice(parameters.Skip(1).ToList());
                    break;
                default:
                    break;
            }
        }

        private static void FIlterByPrice(List<string> range)
        {
            log.Append("Ok: ");
            if (range[0] == "from" && range.Count > 2)
            {
                var from = double.Parse(range[1]);
                var to = double.Parse(range[3]);
                var products = market.ProductsByPrice.Range(new Product() { Price = from }, true, new Product() { Price = to }, true).Take(10);
                log.Append(string.Join(", ", products));
            }
            else if (range[0] == "from" && range.Count == 2)
            {
                var from = double.Parse(range[1]);
                var products = market.ProductsByPrice.RangeFrom(new Product() { Price = from }, true).Take(10);
                log.Append(string.Join(", ", products));
            }
            else
            {
                var to = double.Parse(range[1]);
                var products = market.ProductsByPrice.RangeTo(new Product() { Price = to }, true).Take(10);
                log.Append(string.Join(", ", products));
            }
            log.AppendLine();
        }

        private static void FilterByType(string type)
        {
            if (!market.ProductsByType.ContainsKey(type))
            {
                log.AppendLine($"Error: Type {type} does not exists");
                return;
            }

            var products = market.ProductsByType[type].Take(10);
            log.AppendLine($"Ok: {string.Join(", ", products)}");
        }

        private static void AddProductCommand(IList<string> parameters)
        {
            string name = parameters[0];
            double price = double.Parse(parameters[1]);
            string type = parameters[2];

            if (market.ProductsByName.ContainsKey(name))
            {
                log.AppendLine($"Error: Product {name} already exists");
                return;
            }
            var productToAdd = new Product()
            {
                Name = name,
                Price = price,
                Type = type
            };
            market.ProductsByName.Add(name, productToAdd);

            if (!market.ProductsByType.ContainsKey(type))
            {
                market.ProductsByType.Add(type, new OrderedSet<Product>());
            }
            market.ProductsByType[type].Add(productToAdd);

            market.ProductsByPrice.Add(productToAdd);

            log.AppendLine($"Ok: Product {name} added successfully");
        }
    }

    class Market
    {
        public Market()
        {
            this.ProductsByName = new Dictionary<string, Product>();
            this.ProductsByType = new Dictionary<string, OrderedSet<Product>>();
            this.ProductsByPrice = new OrderedSet<Product>();
        }

        public IDictionary<string, Product> ProductsByName { get; set; }

        public IDictionary<string, OrderedSet<Product>> ProductsByType { get; set; }

        public OrderedSet<Product> ProductsByPrice { get; set; }
    }

    class Product : IComparable<Product>
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public string Type { get; set; }

        public int CompareTo(Product other)
        {
            var result = this.Price.CompareTo(other.Price);

            if (result == 0)
            {
                result = this.Name.CompareTo(other.Name);

                if (result == 0)
                {
                    result = this.Type.CompareTo(other.Type);
                }
            }

            return result;
        }

        public override string ToString()
        {
            return $"{this.Name}({this.Price})";
        }
    }
}
