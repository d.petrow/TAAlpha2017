﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05.ConnectAllCities
{
    class ConnectAllCities
    {
        static int townsCount;
        static bool[,] country;
        static bool[] visited;

        static void Main(string[] args)
        {
            var testsCount = int.Parse(Console.ReadLine());

            for (int t = 0; t < testsCount; t++)
            {
                townsCount = int.Parse(Console.ReadLine());
                country = new bool[townsCount, townsCount];
                visited = new bool[townsCount];

                for (int r = 0; r < townsCount; r++)
                {
                    var line = Console.ReadLine();

                    for (int c = 0; c < townsCount; c++)
                    {
                        if (line[c] == '1')
                        {
                            country[r, c] = true;
                        }
                        else
                        {
                            country[r, c] = false;
                        }
                    }

                }

                AreTheCitiesConnected();
            }
        }

        private static bool AreTheCitiesConnected()
        {
            for (int j = 0; j < townsCount; j++)
            {
                for (int i = j + 1; i < townsCount; i++)
                {
                    if (country[i, j])
                    {
                        visited = new bool[townsCount];

                        return DFS(i, j);
                    }
                }
            }

            return false;
        }

        private static bool DFS(int i, int j)
        {
            visited[j] = true;
            if (AllVisited())
            {
                return true;
            }

            for (int r = i + 1; r < townsCount; r++)
            {
                if (country[r, j])
                {
                    if (DFS(r, r))
                    {
                        return true;
                    }
                }
            }
            
            return false;
        }

        private static bool AllVisited()
        {
            foreach (var item in visited)
            {
                if (item == false)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
