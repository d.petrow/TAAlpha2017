﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _06.GirlsGoneWild
{
    class GirlsGoneWild
    {
        static int shirtsCount;
        static string skirts;
        static int girlsCount;

        static int comboLength;
        static int counter;
        static StringBuilder result;

        static bool[] usedShirts;
        static bool[] usedSkirts;

        static string[] comb;

        static void Main(string[] args)
        {
            shirtsCount = int.Parse(Console.ReadLine());
            skirts = String.Concat(Console.ReadLine().OrderBy(c => c));
            girlsCount = int.Parse(Console.ReadLine());

            counter = 0;
            comboLength = (girlsCount * 2) + (girlsCount - 1);
            result = new StringBuilder();

            comb = new string[girlsCount];

            usedShirts = new bool[shirtsCount];
            usedSkirts = new bool[skirts.Length];

            GoWild(0, 0, 0);

            Console.WriteLine(counter);
            //Console.WriteLine(result.ToString());
        }

        private static void GoWild(int girl, int currectShirt, int currentSkirtInd)
        {
            if (girl >= girlsCount)
            {
                counter++;
                Console.WriteLine(string.Join("-", comb));
                return;
            }

            for (int sk = currentSkirtInd; sk < skirts.Length; sk++)
            {
                comb[girl] = $"{currectShirt}{skirts[sk]}";
                GoWild(girl + 1, currectShirt + 1, sk + 1);
            }
        }
    }
}
