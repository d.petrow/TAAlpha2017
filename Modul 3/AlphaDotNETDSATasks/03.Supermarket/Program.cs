﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Wintellect.PowerCollections;

namespace _03.Supermarket
{
    class Program
    {
        static StringBuilder result;
        static BigList<string> supermarketQueue;
        static Dictionary<string, int> nameAppearances;

        static void Main(string[] args)
        {
            //ReadAndExecuteCommands();
        }
        
        private static void ReadAndExecuteCommands()
        {
            result = new StringBuilder();
            supermarketQueue = new BigList<string>();
            nameAppearances = new Dictionary<string, int>();

            string[] input;

            while (true)
            {
                input = Console.ReadLine().Split().ToArray();

                switch (input[0])
                {
                    case "Append":
                        Append(input[1]);
                        break;
                    case "Insert":
                        Insert(int.Parse(input[1]), input[2]);
                        break;
                    case "Find":
                        Find(input[1]);
                        break;
                    case "Serve":
                        Serve(int.Parse(input[1]));
                        break;
                    case "End":
                        Console.WriteLine(result.ToString().Trim());
                        return;
                }
            }
        }

        private static void Append(string name)
        {
            supermarketQueue.Add(name);

            if (!nameAppearances.ContainsKey(name))
            {
                nameAppearances.Add(name, 0);
            }
            nameAppearances[name]++;

            result.AppendLine("OK");
        }

        private static void Insert(int position, string name)
        {
            if (position < 0 || position > supermarketQueue.Count)
            {
                result.AppendLine("Error");
                return;
            }

            if (position == supermarketQueue.Count)
            {
                Append(name);
                return;
            }

            supermarketQueue.Insert(position, name);

            if (!nameAppearances.ContainsKey(name))
            {
                nameAppearances.Add(name, 0);
            }
            nameAppearances[name]++;

            result.AppendLine("OK");
        }

        private static void Find(string name)
        {
            if (!nameAppearances.ContainsKey(name))
            {
                result.AppendLine("0");
                return;
            }

            result.AppendLine(nameAppearances[name].ToString());
        }

        private static void Serve(int count)
        {
            if (count > supermarketQueue.Count)
            {
                result.AppendLine("Error");
                return;
            }

            var persons = supermarketQueue.Range(0, count);
            for (int i = 0; i < count; i++)
            {
                nameAppearances[persons[i]]--;

                result.Append(persons[i] + " ");
            }

            supermarketQueue.RemoveRange(0, count);

            result.Length -= 1;
            result.AppendLine();
        }
    }

}
