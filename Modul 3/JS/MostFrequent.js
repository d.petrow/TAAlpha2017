const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};

// this is the test
const test = [`15`];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

//For Judge
function PathToOne(num, actions = 1){
    if (num == 1){
        return actions;
    }
    
    if (num % 2 === 0) {
        return PathToOne(num / 2, actions + 1);
    }
    else {
        var plusOne = PathToOne((num + 1) / 2, actions + 1);
        var minusOne = PathToOne((num - 1) / 2, actions + 1);

        if (plusOne < minusOne) {
            return plusOne;
        }
        else {
            return minusOne;
        }
    }
}

var n = parseInt(gets());
var actionsCount = PathToOne(n);

console.log(actionsCount);
