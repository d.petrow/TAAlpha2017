﻿using BlogSystem.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BlogSystem.Data
{
    public class BlogSystemDbContext : IdentityDbContext<User>
    {
        public BlogSystemDbContext(DbContextOptions<BlogSystemDbContext> options)
            : base(options)
        {
        }

        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // User-To-Posts
            builder.Entity<Post>()
                .HasOne(p => p.PostedByUser)
                .WithMany(u => u.Posts)
                .HasForeignKey(p => p.PostedByUserId);

            base.OnModelCreating(builder);
        }
    }
}
