﻿using System;

namespace BlogSystem.DTO
{
    public class PostDto
    {
        public string Title { get; set; }
        
        public string Content { get; set; }

        public DateTime PostedOn { get; set; }

        public UserDto PostedByUser { get; set; }
    }
}
