﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace BlogSystem.Models
{
    public class User : IdentityUser
    {
        public ICollection<Post> Posts { get; set; }
    }
}
