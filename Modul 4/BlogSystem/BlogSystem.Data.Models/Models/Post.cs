﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BlogSystem.Models
{
    public class Post
    {
        public Guid Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        public DateTime PostedOn { get; set; }

        public DateTime? LastUpdatedOn { get; set; }

        [Required]
        public string PostedByUserId { get; set; }
        public User PostedByUser { get; set; }
    }
}