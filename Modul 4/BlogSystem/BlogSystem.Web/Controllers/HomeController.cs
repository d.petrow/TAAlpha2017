﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BlogSystem.Web.Models;
using BlogSystem.Services;
using AutoMapper;
using BlogSystem.DTO;
using BlogSystem.Web.Models.PostViewModels;

namespace BlogSystem.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly PostService postService;
        private readonly IMapper mapper;

        public HomeController(PostService postService, IMapper mapper)
        {
            this.postService = postService;
            this.mapper = mapper;
        }

        public IActionResult Index()
        {
            var recentPostsDtos = this.postService.GetMostRecentPosts(5);

            var recentPostsModels = new List<PostViewModel>();
            foreach (var post in recentPostsDtos)
            {
                var postModel = this.mapper.Map<PostDto, PostViewModel>(post);
                recentPostsModels.Add(postModel);
            }

            return View(recentPostsModels);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
