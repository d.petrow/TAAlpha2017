﻿using AutoMapper.QueryableExtensions;
using BlogSystem.Data;
using BlogSystem.DTO;
using BlogSystem.Web.Models.PostViewModels;
using System.Collections.Generic;
using System.Linq;

namespace BlogSystem.Services
{
    public class PostService
    {
        private readonly BlogSystemDbContext dbContext;

        public PostService(BlogSystemDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IEnumerable<PostDto> GetMostRecentPosts(int count)
        {
            if (this.dbContext.Posts.Count() > count)
            {
                return this.dbContext.Posts.Take(count).ProjectTo<PostViewModel>();
            }
            else
            {
                return this.dbContext.Posts.ProjectTo<PostViewModel>();
            }
        }
    }
}
