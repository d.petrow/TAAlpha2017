﻿using System;
using System.Linq;

class Porcupines
{
    static long baseColCount;
    static long totalRowsCount;
    static long[] pCo;
    static long[] rCo;
    static long[][] forest;
    static string command = string.Empty;

    static long rabbitScore;
    static long porkScore;

    static void Main(string[] args)
    {
        baseColCount = long.Parse(Console.ReadLine());
        totalRowsCount = long.Parse(Console.ReadLine());
        pCo = Console.ReadLine().Split(' ').Select(long.Parse).ToArray();
        rCo = Console.ReadLine().Split(' ').Select(long.Parse).ToArray();

        forest = new long[totalRowsCount][];
        for (long row = 0, multiplier = 1, fillNum = 1; row < totalRowsCount; row++, fillNum++)
        {
            forest[row] = new long[baseColCount * multiplier];

            for (long col = 0, fillCols = fillNum; col < forest[row].Length; col++, fillCols += fillNum)
            {
                forest[row][col] = fillCols;
            }

            if (row < (totalRowsCount / 2) - 1)
            {
                multiplier++;
            }
            else if (row > (totalRowsCount / 2) - 1)
            {
                multiplier--;
            }
        }
        //Represent the forest
        //PrintForest(forest);


        rabbitScore = forest[rCo[0]][rCo[1]];
        porkScore = forest[pCo[0]][pCo[1]];
        forest[rCo[0]][rCo[1]] = 0;
        forest[pCo[0]][pCo[1]] = 0;
        //Initial Collect Points

        //PrintForest(forest);

        do
        {
            command = Console.ReadLine();
            MoveAnimal();
        } while (command != "END");

        if (rabbitScore > porkScore)
        {
            Console.WriteLine("The rabbit WON with {0} points. The porcupine scored {1} points only.", rabbitScore, porkScore);
        }
        else if (rabbitScore < porkScore)
        {
            Console.WriteLine("The porcupine destroyed the rabbit with {0} points. The rabbit must work harder. He scored {1} points only.", porkScore, rabbitScore);
        }
        else
        {
            Console.WriteLine("Both units scored {0} points. Maybe we should play again?", rabbitScore);
        }
    }

    private static void MoveAnimal()
    {
        if (command == "END")
        {
            return;
        }
        string unit = command.Split(' ')[0];
        string dir = command.Split(' ')[1];
        int steps = int.Parse(command.Split(' ')[2]);

        if (unit == "R")
        {
            MoveRabbit(dir, steps);

            //PrintForest(forest);
        }
        else if (unit == "P")
        {
            MovePorcupine(dir, steps);

            //PrintForest(forest);
        }
    }

    private static void MoveRabbit(string dir, int steps)
    {
        switch (dir)
        {
            case "T":
                for (int step = 0; step < steps; step++)
                {
                    if (rCo[0] - 1 >= 0)
                    {
                        if (rCo[1] < forest[rCo[0] - 1].Length)
                        {
                            if ((step != steps - 1) || !((rCo[0] - 1 == pCo[0]) && (rCo[1] == pCo[1])))
                            {
                                rCo[0] -= 1;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            if ((step != steps - 1) || !((forest.Length - 1 - rCo[0] == pCo[0]) && (rCo[1] == pCo[1])))
                            {
                                rCo[0] = forest.Length - 1 - rCo[0];
                            }
                        }
                    }
                    else
                    {
                        if ((step != steps - 1) || !((forest.Length - 1 == pCo[0]) && (rCo[1] == pCo[1])))
                        {
                            rCo[0] = forest.Length - 1;
                        }
                    }
                }
                break;
            case "R":
                for (int step = 0; step < steps; step++)
                {
                    if (rCo[1] + 1 < forest[rCo[0]].Length)
                    {
                        if ((step != steps - 1) || !((rCo[1] + 1 == pCo[1]) && (rCo[0] == pCo[0])))
                        {
                            rCo[1] += 1;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        if ((step != steps - 1) || !((0 == pCo[1]) && (rCo[0] == pCo[0])))
                        {
                            rCo[1] = 0;
                        }
                    }
                }
                break;
            case "B":
                for (int step = 0; step < steps; step++)
                {
                    if (rCo[0] + 1 < totalRowsCount)
                    {
                        if (rCo[1] < forest[rCo[0] + 1].Length)
                        {
                            if ((step != steps - 1) || !((rCo[0] + 1 == pCo[0]) && (rCo[1] == pCo[1])))
                            {
                                rCo[0] += 1;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            if ((step != steps - 1) || !((totalRowsCount - 1 - rCo[0] == pCo[0]) && (rCo[1] == pCo[1])))
                            {
                                rCo[0] = totalRowsCount - 1 - rCo[0];
                            }
                        }
                    }
                    else
                    {
                        if ((step != steps - 1) || !((0 == pCo[0]) && (rCo[1] == pCo[1])))
                        {
                            rCo[0] = 0;
                        }
                    }
                }
                break;
            case "L":
                for (int step = 0; step < steps; step++)
                {
                    if (rCo[1] - 1 >= 0)
                    {
                        if ((step != steps - 1) || !((rCo[1] - 1 == pCo[1]) && (rCo[0] == pCo[0])))
                        {
                            rCo[1] -= 1;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        if ((step != steps - 1) || !((forest[rCo[0]].Length - 1 == pCo[1]) && (rCo[0] == pCo[0])))
                        {
                            rCo[1] = forest[rCo[0]].Length - 1;
                        }
                    }
                }
                break;

            default:
                break;
        }

        rabbitScore += forest[rCo[0]][rCo[1]];
        forest[rCo[0]][rCo[1]] = 0;
    }

    private static void MovePorcupine(string dir, int steps)
    {
        switch (dir)
        {
            case "T":
                for (int step = 0; step < steps; step++)
                {
                    if (pCo[0] - 1 >= 0)
                    {
                        if (pCo[1] < forest[pCo[0] - 1].Length)
                        {
                            if (!((pCo[0] - 1 == rCo[0]) && (pCo[1] == rCo[1])))
                            {
                                pCo[0] -= 1;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            if (!((totalRowsCount - 1 - pCo[0] == rCo[0]) && (pCo[1] == rCo[1])))
                            {
                                pCo[0] = totalRowsCount - 1 - pCo[0];
                            }
                        }
                    }
                    else
                    {
                        if (!((totalRowsCount - 1 == rCo[0]) && (pCo[1] == rCo[1])))
                        {
                            pCo[0] = totalRowsCount - 1;
                        }
                    }

                    porkScore += forest[pCo[0]][pCo[1]];
                    forest[pCo[0]][pCo[1]] = 0;
                }
                break;
            case "R":
                for (int step = 0; step < steps; step++)
                {
                    if (pCo[1] + 1 < forest[pCo[0]].Length)
                    {
                        if (!((pCo[1] + 1 == rCo[1]) && (pCo[0] == rCo[0])))
                        {
                            pCo[1] += 1;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (!((0 == rCo[1]) && (pCo[0] == rCo[0])))
                        {
                            pCo[1] = 0;
                        }
                    }

                    porkScore += forest[pCo[0]][pCo[1]];
                    forest[pCo[0]][pCo[1]] = 0;
                }
                break;
            case "B":
                for (int step = 0; step < steps; step++)
                {
                    if (pCo[0] + 1 < totalRowsCount)
                    {
                        if (pCo[1] < forest[pCo[0] + 1].Length)
                        {
                            if (!((pCo[0] + 1 == rCo[0]) && (pCo[1] == rCo[1])))
                            {
                                pCo[0] += 1;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            if (!((totalRowsCount - 1 - pCo[0] == rCo[0]) && (pCo[1] == rCo[1])))
                            {
                                pCo[0] = totalRowsCount - 1 - pCo[0];
                            }
                        }
                    }
                    else
                    {
                        if (!((0 == rCo[0]) && (pCo[1] == rCo[1])))
                        {
                            pCo[0] = 0;
                        }
                    }

                    porkScore += forest[pCo[0]][pCo[1]];
                    forest[pCo[0]][pCo[1]] = 0;
                }
                break;
            case "L":
                for (int step = 0; step < steps; step++)
                {
                    if (pCo[1] - 1 >= 0)
                    {
                        if (!((pCo[1] - 1 == rCo[1]) && (pCo[0] == rCo[0])))
                        {
                            pCo[1] -= 1;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (!((forest[pCo[0]].Length - 1 == rCo[1]) && (pCo[0] == rCo[0])))
                        {
                            pCo[1] = forest[pCo[0]].Length - 1;
                        }
                    }

                    porkScore += forest[pCo[0]][pCo[1]];
                    forest[pCo[0]][pCo[1]] = 0;
                }
                break;

            default:
                break;
        }

        porkScore += forest[pCo[0]][pCo[1]];
        forest[pCo[0]][pCo[1]] = 0;
    }

    private static void PrintForest(long[][] forest)
    {
        for (int row = 0; row < forest.Length; row++)
        {
            for (int col = 0; col < forest[row].Length; col++)
            {
                Console.Write("{0,5} ", forest[row][col]);
            }
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}

