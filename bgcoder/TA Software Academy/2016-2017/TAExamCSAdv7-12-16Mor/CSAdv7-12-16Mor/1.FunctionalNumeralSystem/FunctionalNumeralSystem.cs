﻿using System;
using System.Linq;
using System.Numerics;

class FunctionalNumeralSystem
{
    static void Main(string[] args)
    {
        string[] message = Console.ReadLine()
            .Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries)
            .ToArray();
        //string[] decMes = new string[message.Length];

        string[] numsInSys =
        {
            "F","E","D","C",
            "B","A","9","8",
            "7","5","4","3",
            "2","1","0","6"
        };
        string[] sys = 
        {
            "curry", "scheme", "commonlisp", "mercury",
            "elm", "racket", "standardml", "erlang",
            "clojure", "rust", "lisp", "f#",
            "scala", "haskell", "ocaml", "ml"
        };

        BigInteger result = 1;
        BigInteger numInDec = new BigInteger();

        for (int mes = 0; mes < message.Length; mes++)
        {
            for (int num = 0; num < 15;)
            {
                if (message[mes].Contains(sys[num]))
                {
                    message[mes] = message[mes].Replace(sys[num], numsInSys[num]);
                    num = 0;
                }
                else
                {
                    num++;
                }
            }

            numInDec = Convert.ToUInt64(message[mes], 16);
            result *= numInDec;
        }

        Console.WriteLine(result);
    }
}