﻿using System;
using System.Linq;

class SnackyTheSnake
{
    static int[] caveSize;
    static char[][] cave;
    static char[] steps;
    static int snackyL = 3;
    static int starving = 0;

    static int[] entrance;
    static int[] co;

    static bool lost = false;
    static bool hitRock = false;
    static bool snackyOut = false;
    static bool starved = false;

    static void Main(string[] args)
    {
        caveSize = Console.ReadLine().Split('x').Select(int.Parse).ToArray();
        cave = new char[caveSize[0]][];
        for (int r = 0; r < caveSize[0]; r++)
        {
            cave[r] = Console.ReadLine().ToCharArray();
        }
        steps = Console.ReadLine().Split(',').Select(char.Parse).ToArray();

        entrance = new int[2] { 0, 0 };
        co = new int[2] { 0, 0 };
        for (int ind = 0; ind < caveSize[1]; ind++)
        {
            if (cave[0][ind] == 's')
            {
                entrance[1] = ind;
                co[1] = ind;
                break;
            }
        }
        //Find Entrance


        for (int step = 0; step < steps.Length; step++)
        {
            MoveSnacky(steps[step]);
            
            if (lost)
            {
                Console.WriteLine("Snacky will be lost into the depths with length {0}", snackyL);
                return;
            }
            else if (hitRock)
            {
                Console.WriteLine("Snacky will hit a rock at [{0},{1}]", co[0], co[1]);
                return;
            }
            else if (snackyOut)
            {
                Console.WriteLine("Snacky will get out with length {0}", snackyL);
                return;
            }
            else if (starved)
            {
                Console.WriteLine("Snacky will starve at [{0},{1}]", co[0], co[1]);
                return;
            }
        }
        
        Console.WriteLine("Snacky will be stuck in the den at [{0},{1}]", co[0], co[1]);
    }

    private static void MoveSnacky(char dir)
    {
        switch (dir)
        {
            case 'u':
                starving++;
                if (starving == 5)
                {
                    snackyL--;
                    starving = 0;
                }
                if (snackyL <= 0)
                {
                    starved = true;
                    return;
                }

                if (co[0] - 1 >= 0)
                {
                    if (cave[co[0] - 1][co[1]] == 's')
                    {
                        snackyOut = true;
                        return;
                    }
                    else if (cave[co[0] - 1][co[1]] == '#')
                    {
                        hitRock = true;
                        co[0] -= 1;
                        return;
                    }
                    else if (cave[co[0] - 1][co[1]] == '*')
                    {
                        snackyL += 1;
                        cave[co[0] - 1][co[1]] = '.';
                        co[0] -= 1;
                    }
                    else
                    {
                        co[0] -= 1;
                    }
                }
                break;
            case 'r':
                starving++;
                if (starving == 5)
                {
                    snackyL--;
                    starving = 0;
                }
                if (snackyL <= 0)
                {
                    starved = true;
                    return;
                }

                if (co[1] + 1 >= caveSize[1])
                {
                    co[1] = -1;
                }

                if (cave[co[0]][co[1] + 1] == '#')
                {
                    co[1] = caveSize[1] - 1;
                    co[1] += 1;
                    hitRock = true;
                    return;
                }
                else if (cave[co[0]][co[1] + 1] == '*')
                {
                    snackyL += 1;
                    cave[co[0]][co[1] + 1] = '.';
                    co[1] += 1;
                }
                else
                {
                    co[1] += 1;
                }

                break;
            case 'd':
                starving++;
                if (starving == 5)
                {
                    snackyL--;
                    starving = 0;
                }
                if (snackyL <= 0)
                {
                    starved = true;
                    return;
                }

                if (co[0] + 1 < caveSize[0])
                {
                    if (cave[co[0] + 1][co[1]] == '#')
                    {
                        hitRock = true;
                        co[0] += 1;
                        return;
                    }
                    else if (cave[co[0] + 1][co[1]] == '*')
                    {
                        snackyL += 1;
                        cave[co[0] + 1][co[1]] = '.';
                        co[0] += 1;
                    }
                    else
                    {
                        co[0] += 1;
                    }
                }
                else
                {
                    lost = true;
                    return;
                }
                
                break;
            case 'l':
                starving++;
                if (starving == 5)
                {
                    snackyL--;
                    starving = 0;
                }
                if (snackyL <= 0)
                {
                    starved = true;
                    return;
                }

                if (co[1] - 1 < 0)
                {
                    co[1] = caveSize[1];
                }

                if (cave[co[0]][co[1] - 1] == '#')
                {
                    hitRock = true;
                    co[1] -= 1;
                    return;
                }
                else if (cave[co[0]][co[1] - 1] == '*')
                {
                    snackyL += 1;
                    cave[co[0]][co[1] - 1] = '.';
                    co[1] -= 1;
                }
                else
                {
                    co[1] -= 1;
                }
                
                break;

            default:
                break;
        }
    }
}

