﻿using System;
using System.Linq;

class DancingMoves
{
    static int[] dancing;
    static string move;
    static int times;
    static string dir;
    static int steps;

    static long score = 0;
    static int divisor = 0;
    static int index = 0;


    static void Main(string[] args)
    {
        dancing = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
        
        while (true)
        {
            move = Console.ReadLine();

            if (move == "stop")
            {
                break;
            }

            Dance(move);
        }

        Console.WriteLine(Math.Round((double)score / divisor, 1, MidpointRounding.AwayFromZero));
    }

    private static void Dance(string move)
    {
        times = int.Parse(move.Split(' ')[0]);
        dir = move.Split(' ')[1];
        steps = int.Parse(move.Split(' ')[2]);

        switch (dir)
        {
            case "right":
                for (int i = 0; i < times; i++)
                {
                    index += steps;

                    while (index >= dancing.Length)
                    {
                        index -= dancing.Length;
                    }

                    score += dancing[index];
                }
                break;
            case "left":
                for (int i = 0; i < times; i++)
                {
                    index -= steps;

                    while (index < 0)
                    {
                        index += dancing.Length;
                    }

                    score += dancing[index];
                }
                break;

            default:
                break;
        }

        divisor++;
    }
}

