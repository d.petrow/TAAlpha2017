﻿using System;
using System.Numerics;

class Messages
{
    static void Main(string[] args)
    {
        string fNum = Console.ReadLine();
        string op = Console.ReadLine();
        string sNum = Console.ReadLine();

        string result = string.Empty;

        string[] sys =
        {
            "cad",
            "xoz",
            "nop",
            "cyk",
            "min",
            "mar",
            "kon",
            "iva",
            "ogi",
            "yan"
        };


        for (int num = 0; num < 10;)
        {
            if (fNum.Contains(sys[num]))
            {
                fNum = fNum.Replace(sys[num], num.ToString());
                num = 0;
            }
            else
            {
                num++;
            }
        }

        for (int num = 0; num < 10;)
        {
            if (sNum.Contains(sys[num]))
            {
                sNum = sNum.Replace(sys[num], num.ToString());
                num = 0;
            }
            else
            {
                num++;
            }
        }

        if (op == "+")
        {
            result = (BigInteger.Parse(fNum) + BigInteger.Parse(sNum)).ToString();
        }
        else if (op == "-")
        {
            result = (BigInteger.Parse(fNum) - BigInteger.Parse(sNum)).ToString();
        }

        for (int num = 0; num < 10;)
        {
            if (result.Contains(num.ToString()))
            {
                result = result.Replace(num.ToString(), sys[num]);
                num = 0;
            }
            else
            {
                num++;
            }
        }

        Console.WriteLine(result);
    }
}

