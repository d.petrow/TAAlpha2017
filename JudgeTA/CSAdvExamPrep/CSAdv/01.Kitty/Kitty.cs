﻿using System;
using System.Linq;

class Kitty
{
    static void Main(string[] args)
    {
        char[] field = Console.ReadLine().ToCharArray();
        int[] moves = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();

        int souls = 0;
        int food = 0;
        int deadLocks = 0;
        bool deadLocked = false;

        for (int move = 0, pos = 0; move <= moves.Length; move++)
        {
            switch (field[pos])
            {
                case '@':
                    if (!(field[pos] == '-'))
                    {
                        souls++;
                        field[pos] = '-';
                    }
                    break;
                case '*':
                    if (!(field[pos] == '-'))
                    {
                        food++;
                        field[pos] = '-';
                    }
                    break;
                case 'x':
                    deadLocks++;

                    if (pos % 2 == 0)
                    {
                        field[pos] = '@';
                        souls--;
                    }
                    else
                    {
                        field[pos] = '*';
                        food--;
                    }

                    break;

                default:
                    break;
            }
            //Soul, food or DeadLock?

            if (food < 0 || souls < 0)
            {
                deadLocked = true;
                Console.WriteLine("You are deadlocked, you greedy kitty!");
                Console.WriteLine("Jumps before deadlock: {0}", move);
                break;
            }
            //DeadLocked
            
            if (!(move == moves.Length))
            {
                while (moves[move] >= field.Length)
                {
                    moves[move] -= field.Length;
                }

                while (moves[move] <= -field.Length)
                {
                    moves[move] += field.Length;
                }

                if (pos + moves[move] < 0)
                {
                    pos = pos + moves[move] + field.Length;
                }
                else if (pos + moves[move] >= field.Length)
                {
                    pos = pos + moves[move] - field.Length;
                }
                else
                {
                    pos += moves[move];
                }
            }
            //Jump
        }

        if (!deadLocked)
        {
            Console.WriteLine("Coder souls collected: {0}", souls);
            Console.WriteLine("Food collected: {0}", food);
            Console.WriteLine("Deadlocks: {0}", deadLocks);
        }
    }
}
