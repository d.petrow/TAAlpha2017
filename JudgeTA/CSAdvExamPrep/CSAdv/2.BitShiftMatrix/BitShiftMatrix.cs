﻿using System;
using System.Linq;
using System.Numerics;

class BitShiftMatrix
{
    static void Main(string[] args)
    {
        int rows = int.Parse(Console.ReadLine());
        int cols = int.Parse(Console.ReadLine());
        int n = int.Parse(Console.ReadLine());
        int[] codes = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
        int coef = Math.Max(rows, cols);
        BigInteger sum = 0;

        BigInteger[][] matrix = new BigInteger[rows][];
        for (int row = 0; row < rows; row++)
        {
            matrix[row] = new BigInteger[cols];
        }
        int r = rows - 1;
        int c = 0;

        for (int row = rows - 1, col = 0, pow = 0; col < cols; pow++)
        {
            BigInteger fillNumber = (BigInteger)Math.Pow(2, pow);
            matrix[row][col] = fillNumber;

            for (int startR = row, startC = col; ;)
            {
                if (((startR + 1) < rows) && ((startC + 1) < cols))
                {
                    startR++;
                    startC++;
                    matrix[startR][startC] = fillNumber;
                }
                else
                {
                    break;
                }
            }

            if (row != 0)
            {
                row--;
            }
            else
            {
                col++;
            }
        }

        for (int instr = 0; instr < n; instr++)
        {
            int rowEnd = codes[instr] / coef;
            int colEnd = codes[instr] % coef;

            while (true)
            {
                sum += matrix[r][c];
                matrix[r][c] = 0;

                if (c == colEnd)
                {
                    break;
                }

                if (c > colEnd)
                {
                    c--;
                }
                else if (c < colEnd)
                {
                    c++;
                }
            }

            while (true)
            {
                sum += matrix[r][c];
                matrix[r][c] = 0;

                if (r == rowEnd)
                {
                    break;
                }

                if (r > rowEnd)
                {
                    r--;
                }
                else if (r < rowEnd)
                {
                    r++;
                }
            }

            //PrintMatrix(matrix);
            //Console.WriteLine();
        }

        Console.WriteLine(sum);
    }
}

