﻿using System;
using System.Linq;

class Battleships
{
    static int[][] fBoard;
    static int[][] sBoard;
    static bool[][] beenThereF;
    static bool[][] beenThereS;
    static int fScore = 0;
    static int sScore = 0;

    static string player;
    static int row;
    static int col;


    static void Main(string[] args)
    {
        string size = Console.ReadLine();
        int r = int.Parse(size.Split(' ')[0]);
        int c = int.Parse(size.Split(' ')[1]);
        fBoard = new int[r][];
        sBoard = new int[r][];
        beenThereF = new bool[r * 2][];
        beenThereS = new bool[r * 2][];
        for (int row = 0; row < 2 * r; row++)
        {
            if (row < (2 * r) / 2)
            {
                fBoard[row] = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
                beenThereF[row] = new bool[c];
                for (int col = 0; col < fBoard[row].Length; col++)
                {
                    if (fBoard[row][col] == 1)
                    {
                        fScore++;
                    }
                }
            }
            else
            {
                sBoard[row - r] = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
                beenThereS[row - r] = new bool[c];
                for (int col = 0; col < fBoard[row - r].Length; col++)
                {
                    if (sBoard[row - r][col] == 1)
                    {
                        sScore++;
                    }
                }
            }

            beenThereF[row] = new bool[c];

        }

        //Print(fBoard);
        //Console.WriteLine("------------------------------------------------------------------");
        //Print(sBoard);

        string command = string.Empty;
        while (true)
        {
            command = Console.ReadLine();

            if (command == "END")
            {
                break;
            }

            Fire(command);

            //Print(fBoard);
            //Console.WriteLine("------------------------------------------------------------------");
            //Print(sBoard);
        }

        Console.WriteLine(fScore + ":" + sScore);
    }

    private static void Fire(string command)
    {
        player = command.Split(' ')[0];    
        row = int.Parse(command.Split(' ')[1]);    
        col = int.Parse(command.Split(' ')[2]);

        switch (player)
        {
            case "P1":
                row = (sBoard.Length - 1) - row;

                if (sBoard[row][col] == 1)
                {
                    Console.WriteLine("Booom");
                    sBoard[row][col] = 0;
                    sScore--;

                    beenThereS[row][col] = true;
                }
                else if (sBoard[row][col] == 0)
                {
                    if (beenThereS[row][col])
                    {
                        Console.WriteLine("Try again!");
                    }
                    else
                    {
                        Console.WriteLine("Missed");
                        beenThereS[row][col] = true;
                    }
                }
                break;
            case "P2":
                if (fBoard[row][col] == 1)
                {
                    Console.WriteLine("Booom");
                    fBoard[row][col] = 0;
                    fScore--;

                    beenThereF[row][col] = true;
                }
                else if (fBoard[row][col] == 0)
                {
                    if (beenThereF[row][col])
                    {
                        Console.WriteLine("Try again!");
                    }
                    else
                    {
                        Console.WriteLine("Missed");
                        beenThereF[row][col] = true;
                    }
                }
                break;

            default:
                break;
        }
    }

    private static void Print(int[][] board)
    {
        for (int r = 0; r < board.Length; r++)
        {
            for (int c = 0; c < board[r].Length; c++)
            {
                Console.Write("{0,5} ", board[r][c]);
            }

            Console.WriteLine();
            Console.WriteLine();
        }
    }
}

