﻿using System;

class SendMeTheCode
{
    static void Main(string[] args)
    {
        string message = Console.ReadLine();
        long result = 0;

        for (int i = message.Length - 1, index = 1; i >= 0; i--, index++)
        {
            if (i == 0 && message[i] == '-')
            {
                continue;
            }

            int digit = int.Parse(message[i].ToString());

            if (index % 2 == 1)
            {
                result += digit * index * index; 
            }
            else
            {
                result += digit * digit * index;
            }
        }
        
        int length = (int)(result % 10);
        if (length == 0)
        {
            Console.WriteLine(result);
            Console.WriteLine("Big Vik wins again!");
            return;
        }
        else
        {
            Console.WriteLine(result);

            int s = (int)(result % 26);
            for (int i = 0, start = s; i < length; i++)
            {
                if (start == 26)
                {
                    start = 1;
                }
                else
                {
                    start++;
                }

                Console.Write((char)(start + 64));
            }
            Console.WriteLine();
        }
    }
}
