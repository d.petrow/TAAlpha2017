﻿using System;
using System.Linq;

class Move
{
    static long index;
    static long[] array;
    static string move;
    static long steps;
    static string dir;
    static long size;

    static long forward = 0;
    static long backwards = 0;


    static void Main(string[] args)
    {
        index = int.Parse(Console.ReadLine());
        array = Console.ReadLine().Split(',').Select(long.Parse).ToArray();

        while (true)
        {
            move = Console.ReadLine();

            if (move == "exit")
            {
                break;
            }

            Moving(move);
        }

        Console.WriteLine("Forward: {0}", forward);
        Console.WriteLine("Backwards: {0}", backwards);
    }

    private static void Moving(string move)
    {
        steps = int.Parse(move.Split(' ')[0]);
        dir = move.Split(' ')[1];
        size = int.Parse(move.Split(' ')[2]);

        switch (dir)
        {
            case "forward":
                for (int i = 0; i < steps; i++)
                {
                    index += size;

                    while (index >= array.Length)
                    {
                        index -= array.Length;
                    }

                    forward += array[index];
                }
                break;
            case "backwards":
                for (int i = 0; i < steps; i++)
                {
                    index -= size;

                    while (index < 0)
                    {
                        index += array.Length;
                    }

                    backwards += array[index];
                }
                break;

            default:
                break;
        }
    }
}

