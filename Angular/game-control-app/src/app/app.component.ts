import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private oddNumbers: number[] = [];
  private evenNumbers: number[] = [];

  onNumberIncreased(number: number) {
    if (number % 2 === 0) {
      this.evenNumbers.push(number);
    }
    else {
      this.oddNumbers.push(number);
    }
  }

  onGameStarted() {
    this.evenNumbers = [];
    this.oddNumbers = [];
  }
}
