import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  @Output() private increaseNumber: EventEmitter<number> = new EventEmitter<number>();
  @Output() private startGame: EventEmitter<void> = new EventEmitter<void>();
  private isGameStarted: boolean = false;
  private number = 0;
  private interval;

  constructor() { }

  ngOnInit() {
  }

  onStartGame() {
    this.isGameStarted = true;

    this.startGame.emit();
    this.interval = setInterval(() => this.increaseNumber.emit(this.number++), 1000);
  }

  onStopGame() {
    this.isGameStarted = false;

    clearInterval(this.interval);
    this.number = 0;
  }
}
