import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: { id: number, name: string };

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    let id = this.route.snapshot.params['id'];
    let name = this.route.snapshot.params['name'];

    this.user = { id: id, name: name };

    
    this.route.params.subscribe(
      (params: Params) => {
        let id = params['id'];
        let name = params['name'];

        this.user = { id: id, name: name };
      }
    )
  }

}
