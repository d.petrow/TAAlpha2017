import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  private areDetailsDisplayed: boolean = false;
  private logDetailButtonClicks: string[] = [];

  constructor() { }

  ngOnInit() {
  }

  onDetailsButtonClick(): void {
    this.logDetailButtonClicks.push(`${new Date()}`)

    if (this.areDetailsDisplayed) {
      this.areDetailsDisplayed = false;
    }
    else {
      this.areDetailsDisplayed = true;
    }
  }
}
