import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  private title: string = 'app';
  private username: string = '';
  private isResetButtonClickable: boolean = false;


  constructor() {

  }

  onInputTyping(event : Event): any {
    var typedValue: string = (<HTMLInputElement>event.target).value;
    this.username = typedValue;

    this.checkIfUsernameIsEmpty();
  }

  onResetButtonClick(): any {
    this.username = '';
    this.isResetButtonClickable = false;
  }

  private checkIfUsernameIsEmpty(): any {
    if (this.username.length <= 0) {
      this.isResetButtonClickable = false;
    }
    else {
      this.isResetButtonClickable = true;
    }
  }
}
