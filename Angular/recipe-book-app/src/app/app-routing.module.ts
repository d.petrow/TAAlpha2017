import { NgModule } from '../../node_modules/@angular/core';
import { RouterModule, Routes } from '../../node_modules/@angular/router';

import { RecipesComponent } from './recipes/recipes.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { EditRecipeComponent } from './recipes/edit-recipe/edit-recipe.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { AddRecipeComponent } from './recipes/add-recipe/add-recipe.component';

const appRoutes: Routes = [
    {
        path: '', component: RecipesComponent, children: [
            { path: 'new', component: AddRecipeComponent },
            { path: ':id/detail', component: RecipeDetailComponent },
            { path: ':id/edit', component: EditRecipeComponent }
        ]
    },
    { path: 'shopping-list', component: ShoppingListComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
