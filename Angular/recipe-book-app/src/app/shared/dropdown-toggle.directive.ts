import { Directive, HostListener, OnInit, HostBinding } from "@angular/core";

@Directive({
  selector: "[appDropdownToggle]"
})
export class DropdownToggleDirective implements OnInit {
  @HostBinding("class.open") isOpen: boolean;

  constructor() {}

  ngOnInit() {}

  @HostListener("click")
  click() {
    this.isOpen = !this.isOpen;
  }
}
