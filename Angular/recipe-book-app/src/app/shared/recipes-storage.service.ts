import { Injectable } from "../../../node_modules/@angular/core";
import { Http, Response } from "@angular/http";
import { Subject } from "../../../node_modules/rxjs";
import { map } from "rxjs/operators";

import { RecipeDetail } from "../recipes/recipe-detail/recipe-detail.model";
import { Recipe } from "../recipes/recipe-list/recipe.model";
import { Ingredient } from "./ingredient.model";

@Injectable()
export class RecipesStorageService {
  private sampleImgPath =
    // tslint:disable-next-line:max-line-length
    "https://www.bbcgoodfood.com/sites/default/files/recipe-collections/collection-image/2013/05/frying-pan-pizza-easy-recipe-collection.jpg";
  private sampleIngredients: Ingredient[] = [
    new Ingredient("Bread", 2),
    new Ingredient("Meat", 1),
    new Ingredient("Chicken", 10),
    new Ingredient("Butter", 1)
  ];
  private recipeDataUrl = "https://recipebookdata.firebaseio.com/Recipes.json";

  private idIncremented = 0;
  private recipes: Recipe[] = [];

  recipesUpdated: Subject<Recipe[]> = new Subject();
  areRecipesUpdated = false;

  constructor(private http: Http) {
    this.recipesUpdated.subscribe(() => (this.areRecipesUpdated = true));
  }

  getRecipes() {
    this.http
      .get(this.recipeDataUrl)
      .pipe(
        map(
          (response: Response): Recipe[] => {
            let id = 0;
            const recipes: Recipe[] = response
              .json()
              .filter(x => x)
              .map((recipe: Recipe) => {
                recipe.id = id;
                id++;
                return recipe;
              });
            this.idIncremented = recipes.length;
            return recipes;
          }
        )
      )
      .subscribe((recipes: Recipe[]) => {
        this.recipes = recipes;
        this.recipesUpdated.next(recipes);
      });
  }

  saveRecipes() {
    this.http
      .put(this.recipeDataUrl, JSON.stringify(this.recipes))
      .subscribe((response: Response) => {
        this.recipesUpdated.next(this.recipes);
      });
  }

  getRecipeById(id: number): RecipeDetail {
    if (id < 0 || (this.recipes && id >= this.recipes.length)) {
      throw new Error('Index out of boundaries error!');
    }

    const recipe: Recipe = this.recipes[id];
    return new RecipeDetail(
      recipe.name,
      recipe.description,
      recipe.imagePath,
      this.sampleIngredients.slice()
    );
  }

  addRecipe(recipe: Recipe): number {
    if (!recipe.imagePath) {
      recipe.imagePath = this.sampleImgPath;
    }
    recipe.id = this.idIncremented;
    this.idIncremented += 1;

    this.recipes.push(recipe);
    return recipe.id;
  }

  updateRecipe(
    id: number,
    name: string,
    description: string,
    imagePath: string
  ) {
    const recipe = this.recipes[id];
    recipe.name = name;
    recipe.description = description;
    recipe.imagePath = imagePath;
  }
}
