import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Ingredient } from '../../shared/ingredient.model';

@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.css']
})
export class ShoppingListEditComponent implements OnInit {
  @Output() private addIngredient: EventEmitter<Ingredient> = new EventEmitter<Ingredient>();

  constructor() { }

  ngOnInit() {
  }

  onAddIngredient(ingredientNameInput: HTMLInputElement, ingredientAmountInput: HTMLInputElement) {
    const newIngredient = new Ingredient(ingredientNameInput.value, +ingredientAmountInput.value);
    this.addIngredient.emit(newIngredient);
  }
}
