import { EventEmitter } from "@angular/core";
import { Ingredient } from "../../shared/ingredient.model";

export class ShoppingListService {
  private ingredients: Ingredient[] = [
    new Ingredient("sample Ingredient", 100),
    new Ingredient("sample Ingredient2", 100),
    new Ingredient("sample Ingredient3", 100),
    new Ingredient("sample Ingredient4", 100)
  ];
  ingredientAdded: EventEmitter<void> = new EventEmitter();

  getIngredients() {
    return this.ingredients.slice();
  }

  addIngredient(ingredient: Ingredient) {
    this.ingredients.push(ingredient);
    this.ingredientAdded.emit();
  }

  addIngredients(ingredients: Ingredient[]) {
    this.ingredients.push(...ingredients);
    this.ingredientAdded.emit();
  }
}
