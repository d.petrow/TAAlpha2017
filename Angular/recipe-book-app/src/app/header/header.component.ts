import { Component } from "@angular/core";
import { RecipesStorageService } from "../shared/recipes-storage.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent {
  constructor(private recipesStorageService: RecipesStorageService) {}
  onGetRecipes() {
    this.recipesStorageService.getRecipes();
  }

  onSaveRecipes() {
    this.recipesStorageService.saveRecipes();
  }
}
