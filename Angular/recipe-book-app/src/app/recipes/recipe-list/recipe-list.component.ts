import { Component, OnInit } from "@angular/core";

import { Recipe } from "./recipe.model";
import { RecipesStorageService } from "../../shared/recipes-storage.service";

@Component({
  selector: "app-recipe-list",
  templateUrl: "./recipe-list.component.html",
  styleUrls: ["./recipe-list.component.css"],
  providers: []
})
export class RecipeListComponent implements OnInit {
  private recipes: Recipe[] = [];

  constructor(private recipesStorageService: RecipesStorageService) {}

  ngOnInit() {
    this.recipesStorageService.recipesUpdated.subscribe((recipes: Recipe[]) => {
      this.recipes = recipes;
    });

    this.recipesStorageService.getRecipes();
  }
}
