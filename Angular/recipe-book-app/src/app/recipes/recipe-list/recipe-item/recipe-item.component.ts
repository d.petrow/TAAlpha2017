import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Recipe } from '../recipe.model';
import { RecipesService } from '../../services/recipes.service';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {
  @Input() private recipeId: number;
  @Input() private recipeItem: Recipe;

  constructor(private recipeListService: RecipesService, private router: Router) {

  }

  ngOnInit() {

  }

  onRecipeClicked() {
    this.router.navigate(['/', this.recipeId, 'detail']);
  }
}
