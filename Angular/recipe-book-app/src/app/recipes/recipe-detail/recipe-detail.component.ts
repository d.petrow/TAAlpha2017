import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Subscription } from "rxjs";

import { RecipeDetail } from "./recipe-detail.model";
import { RecipesService } from "../services/recipes.service";
import { RecipesStorageService } from "../../shared/recipes-storage.service";

@Component({
  selector: "app-recipe-detail",
  templateUrl: "./recipe-detail.component.html",
  styleUrls: ["./recipe-detail.component.css"]
})
export class RecipeDetailComponent implements OnInit, OnDestroy {
  private recipeDetail: RecipeDetail;
  private recipesUpdateSubscription: Subscription;

  constructor(
    private recipeStorageService: RecipesStorageService,
    private recipesService: RecipesService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      const recipeId = +params["id"];
      if (!this.recipeStorageService.areRecipesUpdated) {
        this.recipesUpdateSubscription = this.recipeStorageService.recipesUpdated.subscribe(
          () => {
            try {
              this.recipeDetail = this.recipeStorageService.getRecipeById(
                recipeId
              );
            } catch (e) {
              if (e instanceof Error) {
                console.log(e.message);
                this.router.navigate(["/"]);
              }
            }
          }
        );
      } else {
        try {
          this.recipeDetail = this.recipeStorageService.getRecipeById(recipeId);
        } catch (e) {
          if (e instanceof Error) {
            console.log(e.message);
            this.router.navigate(["/"]);
          }
        }
      }
    });
  }

  onAddIngredients() {
    this.recipesService.addIngredientsToShoppingList(
      this.recipeDetail.ingredients
    );
  }

  onEditRecipe() {
    this.router.navigate(["../", "edit"], { relativeTo: this.route });
  }

  ngOnDestroy() {
    console.log("destroyed Detail!");
    // this.recipesUpdateSubscription.unsubscribe();
  }
}
