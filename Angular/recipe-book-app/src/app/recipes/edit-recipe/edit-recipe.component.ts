import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";

import { RecipeDetail } from "../recipe-detail/recipe-detail.model";
import { RecipesStorageService } from "../../shared/recipes-storage.service";

@Component({
  selector: "app-edit-recipe",
  templateUrl: "./edit-recipe.component.html",
  styleUrls: ["./edit-recipe.component.css"]
})
export class EditRecipeComponent implements OnInit, OnDestroy {
  private recipeId: number;
  private recipeToEdit: RecipeDetail;
  private recipesUpdateSubscription: Subscription;

  constructor(
    private recipesStorageService: RecipesStorageService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.recipeId = +this.route.snapshot.params["id"];

    if (!this.recipesStorageService.areRecipesUpdated) {
      this.recipesUpdateSubscription = this.recipesStorageService.recipesUpdated.subscribe(
        () => {
          try {
            this.recipeToEdit = this.recipesStorageService.getRecipeById(
              this.recipeId
            );
          } catch (e) {
            if (e instanceof Error) {
              console.log(e.message);
              this.router.navigate(["/"]);
            }
          }
        }
      );
    } else {
      try {
        this.recipeToEdit = this.recipesStorageService.getRecipeById(
          this.recipeId
        );
      } catch (e) {
        if (e instanceof Error) {
          console.log(e.message);
          this.router.navigate(["/"]);
        }
      }
    }
  }

  onSave(
    name: HTMLInputElement,
    description: HTMLInputElement,
    imagePath: HTMLInputElement
  ) {
    this.recipesStorageService.updateRecipe(
      this.recipeId,
      name.value,
      description.value,
      imagePath.value
    );

    this.router.navigate(["../", "detail"], { relativeTo: this.route });
  }

  ngOnDestroy() {
    console.log("edit Destroyed");
    // this.recipesUpdateSubscription.unsubscribe();
  }
}
