import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css'],
  providers: []
})
export class RecipesComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {
  }

  onCreateRecipe() {
    this.router.navigate(['/', 'new']);
  }
}
