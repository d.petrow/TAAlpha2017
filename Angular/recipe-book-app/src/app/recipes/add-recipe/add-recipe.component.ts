import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { Recipe } from "../recipe-list/recipe.model";
import { RecipesStorageService } from "../../shared/recipes-storage.service";

@Component({
  selector: "app-add-recipe",
  templateUrl: "./add-recipe.component.html",
  styleUrls: ["./add-recipe.component.css"]
})
export class AddRecipeComponent implements OnInit {
  constructor(
    private recipesStorageService: RecipesStorageService,
    private router: Router
  ) {}

  ngOnInit() {}

  onCreate(
    recipeNameInput: HTMLInputElement,
    recipeDescriptionInput: HTMLInputElement,
    recipeimagePathInput?: HTMLInputElement
  ) {
    const recipeName: string = recipeNameInput.value;
    const recipeDescription: string = recipeDescriptionInput.value;
    const recipeImagePath: string = recipeimagePathInput.value;

    const recipe = new Recipe(recipeName, recipeDescription, recipeImagePath);

    const recipeId = this.recipesStorageService.addRecipe(recipe);

    this.router.navigate(["../", recipeId, "detail"]);
  }
}
