import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpModule } from "@angular/http";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { ShoppingListComponent } from "./shopping-list/shopping-list.component";
import { ShoppingListEditComponent } from "./shopping-list/shopping-list-edit/shopping-list-edit.component";

import { RecipesComponent } from "./recipes/recipes.component";
import { RecipeListComponent } from "./recipes/recipe-list/recipe-list.component";
import { RecipeItemComponent } from "./recipes/recipe-list/recipe-item/recipe-item.component";
import { RecipeDetailComponent } from "./recipes/recipe-detail/recipe-detail.component";
import { AddRecipeComponent } from "./recipes/add-recipe/add-recipe.component";
import { EditRecipeComponent } from "./recipes/edit-recipe/edit-recipe.component";

import { AppRoutingModule } from "./app-routing.module";
import { DropdownToggleDirective } from "./shared/dropdown-toggle.directive";
import { ShoppingListService } from "./shopping-list/services/shopping-list.service";
import { RecipesStorageService } from "./shared/recipes-storage.service";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ShoppingListComponent,
    ShoppingListEditComponent,
    RecipeItemComponent,
    RecipeDetailComponent,
    RecipeListComponent,
    AddRecipeComponent,
    DropdownToggleDirective,
    RecipesComponent,
    EditRecipeComponent
  ],
  imports: [BrowserModule, HttpModule, AppRoutingModule],
  providers: [ShoppingListService, RecipesStorageService],
  bootstrap: [AppComponent]
})
export class AppModule {}
