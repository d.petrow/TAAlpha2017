﻿using System;
using System.Collections.Generic;

class LargestAreaInMatrix
{
    static int n;
    static int m;
    static int[,] matrix;
    static bool[,] visited;
    static int maxAreaEquals;

    static void Main(string[] args)
    {
        string input = Console.ReadLine();
        n = int.Parse(input.Split(' ')[0]);
        m = int.Parse(input.Split(' ')[1]);
        matrix = new int[n, m];
        visited = new bool[n, m]; // Visited Elements

        for (int row = 0; row < n; row++)
        {
            string[] inputRow = (Console.ReadLine()).Split(' ');
            for (int col = 0; col < m; col++)
            {
                matrix[row, col] = int.Parse(inputRow[col]);
            }
        }
        //Read Matrix

        maxAreaEquals = 1;

        for (int r = 0; r < n; r++)
        {
            for (int c = 0; c < m; c++)
            {
                if (visited[r, c] == false)
                {
                    DepthFirstSearch(matrix, r, c);
                }
            }
        }




    }

    private static void DepthFirstSearch(int[,] matrix, int row, int col)
    {
        visited[row, col] = true;

        if (row + 1 < n && matrix[row, col] == matrix[row + 1, col] && visited[row + 1, col])
        {
            DepthFirstSearch(matrix, row + 1, col);
        }
        else if (col + 1 < m && matrix[row, col] == matrix[row, col + 1] && visited[row, col + 1])
        {
            DepthFirstSearch(matrix, row, col + 1);
        }
        else if (row - 1 >= 0 && matrix[row, col] == matrix[row - 1, col] && visited[row - 1, col])
        {
            DepthFirstSearch(matrix, row - 1, col);
        }
        else if (col - 1 >= 0 && matrix[row, col] == matrix[row, col - 1] && visited[row, col - 1])
        {
            DepthFirstSearch(matrix, row, col - 1);
        }


    }
}

