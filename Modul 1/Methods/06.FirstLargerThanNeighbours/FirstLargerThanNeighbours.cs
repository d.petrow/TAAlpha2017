﻿using System;
using System.Linq;

class FirstLargerThanNeighbours
{
    static void Main(string[] args)
    {
        int n = int.Parse(Console.ReadLine());
        int[] nums = Console.ReadLine()
            .Split(' ')
            .Select(int.Parse)
            .ToArray();

        int indOfLarg = IndexOxLargest(nums);

        Console.WriteLine(indOfLarg);
    }

    private static int IndexOxLargest(int[] nums)
    {
        for (int pos = 1; pos < nums.Length - 1; pos++)
        {
            if (LargThanNeigh(nums, pos))
            {
                return pos;
            }
        }

        return -1;
    }

    private static bool LargThanNeigh(int[] nums, int pos)
    {
        if (((pos > 0) && (pos < nums.Length - 1))
            && ((nums[pos] > nums[pos - 1]) && (nums[pos] > nums[pos + 1])))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

