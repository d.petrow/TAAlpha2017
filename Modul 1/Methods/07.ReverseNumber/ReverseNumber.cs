﻿using System;
using System.Text;

class ReverseNumber
{
    static void Main(string[] args)
    {
        decimal number = decimal.Parse(Console.ReadLine());

        decimal reversed = ReverseNum(number);

        Console.WriteLine("{0}", reversed);
    }

    private static decimal ReverseNum(decimal number)
    {
        string num = number.ToString();
        StringBuilder sb = new StringBuilder();
        bool isFraction = false;
        bool hasZeros = true;

        for (int i = num.Length - 1; i >= 0; i--)
        {
            if (isFraction && num[i] != '0')
            {
                isFraction = false;
                hasZeros = false;
            }

            if (num[i] == '.')
            {
                isFraction = true;
            }   

            sb.Append(num[i]);
        }

        decimal reversed = decimal.Parse(sb.ToString());
        
        if (hasZeros)
        {
            long wOZeros = long.Parse(((long)reversed).ToString());
            reversed = decimal.Parse(wOZeros.ToString());
        }

        return reversed;
    }
}

