﻿using System;
using System.Linq;

class AddingPolynomials
{
    static void Main(string[] args)
    {
        int n = int.Parse(Console.ReadLine());
        string fPoly = Console.ReadLine();
        string sPoly = Console.ReadLine();

        SumPolynoms(fPoly, sPoly);
    }

    private static void SumPolynoms(string fInput, string sInput)
    {
        int[] fPolynom = fInput.Split(' ').Select(int.Parse).ToArray();
        int[] sPolynom = sInput.Split(' ').Select(int.Parse).ToArray();

        Console.Write(fPolynom[0] + sPolynom[0]);
        for (int i = 1; i < fPolynom.Length; i++)
        {
            Console.Write(" " + (fPolynom[i] + sPolynom[i]));
        }
    }
}
