﻿using System;
using System.Linq;

class AppearanceCount
{
    static void Main(string[] args)
    {
        int n = int.Parse(Console.ReadLine());
        int[] nums = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
        int x = int.Parse(Console.ReadLine());

        CountAppearancesOfNum(nums, x);
    }

    private static void CountAppearancesOfNum(int[] arr, int num)
    {
        int count = 0;
        for (int index = 0; index < arr.Length; index++)
        {
            if (arr[index] == num)
            {
                count++;
            }
        }
        Console.WriteLine(count);
    }
}

