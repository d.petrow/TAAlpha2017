﻿using System;

class GetLargestNumber
{
    static void Main(string[] args)
    {
        string input = Console.ReadLine();
        int a = int.Parse(input.Split(' ')[0]);
        int b = int.Parse(input.Split(' ')[1]);
        int c = int.Parse(input.Split(' ')[2]);

        int max = GetMax(a, b);
        max = GetMax(max, c);

        Console.WriteLine(max);
    }

    private static int GetMax(int x, int y)
    {
        if (x > y)
        {
            return x;
        }
        else
        {
            return y;
        }
    }
}