﻿using System;
using System.Collections.Generic;
using System.Linq;

class NumberAsArray
{
    static void Main(string[] args)
    {
        string sizes = Console.ReadLine();
        int[] fArr = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
        int[] sArr = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();

        int[] sumOfArrays = SumArrays(fArr, sArr);

        Console.WriteLine(string.Join(" ", sumOfArrays));
    }

    private static int[] SumArrays(int[] fArr, int[] sArr)
    {
        int bigLength = (fArr.Length > sArr.Length) ? fArr.Length : sArr.Length;
        int smallLength = (fArr.Length < sArr.Length) ? fArr.Length : sArr.Length;
        List<int> helpArr = new List<int>();

        int curSum = new int();
        for (int index = 0, remainder = 0; index < bigLength; index++)
        {
            if (index < smallLength)
            {
                if (remainder != 0)
                {
                    curSum = fArr[index] + sArr[index] + remainder;
                    remainder = 0;
                }
                else
                {
                    curSum = fArr[index] + sArr[index];
                }

                if (curSum > 9)
                {
                    helpArr.Add(curSum % 10);
                    curSum /= 10;
                    remainder = curSum;
                }
                else
                {
                    helpArr.Add(curSum);
                }
            }
            else
            {
                if (remainder != 0)
                {

                    curSum = (fArr.Length > sArr.Length ? fArr[index] : sArr[index]) + remainder;
                    remainder = 0;
                }
                else
                {
                    curSum = (fArr.Length > sArr.Length ? fArr[index] : sArr[index]);
                }

                if (curSum > 9)
                {
                    helpArr.Add(curSum % 10);
                    curSum /= 10;
                    remainder = curSum;
                }
                else
                {
                    helpArr.Add(curSum);
                }
            }

        }

        int[] sumArr = new int[helpArr.Count];
        for (int i = 0; i < helpArr.Count; i++)
        {
            sumArr[i] = helpArr[i];
        }

        return sumArr;
    }
}
