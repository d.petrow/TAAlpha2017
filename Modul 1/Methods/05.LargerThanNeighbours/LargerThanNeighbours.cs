﻿using System;
using System.Linq;

class LargerThanNeighbours
{
    static void Main(string[] args)
    {
        int n = int.Parse(Console.ReadLine());
        int[] nums = Console.ReadLine()
            .Split(' ')
            .Select(int.Parse)
            .ToArray();
        int largerElems = 0;


        for (int pos = 0; pos < n; pos++)
        {
            if (LargThanNeigh(nums, pos))
            {
                largerElems++;
            }

        }

        Console.WriteLine(largerElems);
    }

    private static bool LargThanNeigh(int[] nums, int pos)
    {
        if ( ((pos > 0) && (pos < nums.Length - 1)) 
            && ((nums[pos] > nums[pos - 1]) && (nums[pos] > nums[pos + 1])) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

