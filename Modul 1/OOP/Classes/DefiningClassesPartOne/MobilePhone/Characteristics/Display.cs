﻿namespace MobilePhone.Characteristics
{
    class Display
    {
        public Display(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

        public Display(int width, int height, int colorsNumber)
            :this(width, height)
        {
            this.ColorsNumber = colorsNumber;
        }

        public int Width { get; }
        public int Height { get; }
        public int ColorsNumber { get; }
    }
}
