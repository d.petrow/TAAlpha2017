﻿using MobilePhone.Characteristics.Batteries;

namespace MobilePhone.Characteristics
{
    class Battery
    {
        public Battery(string model, BatteryType type)
        {
            this.Model = model;
            this.Type = type;
        }

        public Battery(string model, BatteryType type, int hoursIdle)
            : this(model, type)
        {
            this.HoursIdle = hoursIdle;
        }

        public Battery(string model, BatteryType type, int hoursIdle, int hoursTalk)
            : this(model, type, hoursIdle)
        {
            this.HoursTalk = hoursTalk;
        }


        public string Model { get; }
        public BatteryType Type { get; }
        public int HoursIdle { get; }
        public int HoursTalk { get; }
    }
}
