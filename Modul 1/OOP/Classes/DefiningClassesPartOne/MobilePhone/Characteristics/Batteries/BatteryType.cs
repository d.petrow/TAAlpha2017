﻿namespace MobilePhone.Characteristics.Batteries
{
     public enum BatteryType
    {
        Li_Ion,
        NiMH,
        NiCd,
        Al_Ion,
        Flow,
        Lead_Acid,
        Glass,
        Lithium_Air,
        Magnesium_Ion,
        Molten_Salt
    }
}
