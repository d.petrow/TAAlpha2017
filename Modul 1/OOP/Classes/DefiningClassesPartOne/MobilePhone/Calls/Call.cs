﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilePhone.Calls
{
    public class Call
    {
        private string date;
        private string time;
        private string duration;
        private double price = 0d;


        public Call(string date, string time, string dialedNumber, int seconds)
        {
            this.date = date;
            this.time = time;
            this.DialedNumber = dialedNumber;
            this.Seconds = seconds;

            StringBuilder strB = new StringBuilder();
            int hours = 0;
            int mins = 0;
            int secs = seconds;

            while (secs >= 60)
            {
                secs -= 60;
                mins++;

                while (mins >= 60)
                {
                    mins -= 60;
                    hours++;
                }
            }

            if (hours != 0)
            {
                strB.Append(string.Format("{0:00}", hours)).Append(":").Append(string.Format("{0:00}", mins)).Append(":").Append(string.Format("{0:00}", secs));
            }
            else if (mins != 0)
            {
                strB.Append(string.Format("{0:00}",mins)).Append(":").Append(string.Format("{0:00}", secs));

            }
            else
            {
                strB.Append(string.Format("{0:00}", secs));

            }


            this.duration = strB.ToString();


            if (seconds % 60 == 0)
            {
                this.price += (seconds % 60) * GSM.PricePerMinute;
            }
            else
            {
                this.price += ((seconds % 60) + 1) * GSM.PricePerMinute;
            }
        }


        public string Date { get { return date; } }

        public string Time { get { return time; } }

        public string DialedNumber { get; }

        public int Seconds { get; }

        public string Duration { get { return this.duration; } }

        public double Price { get => price; }
    }
}
