﻿using System;
using MobilePhone.Characteristics;
using MobilePhone.Characteristics.Batteries;
using MobilePhone.Calls;
using System.Collections.Generic;

namespace MobilePhone
{
    public class GSM
    {
        //Fields
        private Battery battery;
        private Display display;
        private List<Call> callHistory;
        private static double pricePerMinute = 0.37d;
        private double callPrice = 0d;

        private static GSM iPhone4S = new GSM("4S", "Apple", 599.99d);



        //Ctors
        public GSM(string model, string manufacturer)
        {
            this.Model = model;
            this.Manufacturer = manufacturer;
            this.callHistory = new List<Call>();
        }

        public GSM(string model, string manufacturer, double price)
            : this(model, manufacturer)
        {
            this.Price = price;
        }

        public GSM(string model, string manufacturer, string owner)
            : this(model, manufacturer)
        {
            this.Owner = owner;
        }

        public GSM(string model, string manufacturer, string owner, double price)
            : this(model, manufacturer, price)
        {
            this.Owner = owner;
        }


        //Props
        public string Model { get; }
        public string Manufacturer { get; }
        public double Price { get; }
        public string Owner { get; }
        public List<Call> CallHistory { get { return callHistory; } }
        public static double PricePerMinute { get { return pricePerMinute; } set { pricePerMinute = PricePerMinute; } }

        public static GSM IPhone4S { get { return iPhone4S; } }

        public double CallPrice { get { return callPrice; } }



        //Methods
            //Phone Characteristics Info
        public void SetBattery(string batteryModel, BatteryType type, int hoursIdle = 0, int hoursTalk = 0)
        {
            battery = new Battery(batteryModel, type, hoursIdle, hoursTalk);
        }
        public void BatteryInfo()
        {
            if (battery != null)
            {
                Console.WriteLine(" -\tBattery Model: {0}", battery.Model);
                Console.WriteLine(" -\tBattery Model: {0}", battery.Type);
                if (battery.HoursIdle != 0)
                {
                    Console.WriteLine(" -\tBattery Idle Hours: {0}", battery.HoursIdle);
                }
                if (battery.HoursTalk != 0)
                {
                    Console.WriteLine(" -\tBattery Talk Hours: {0}", battery.HoursTalk);
                }
            }
            else
            {
                Console.WriteLine("No Battery Set");
            }
        }

        public void SetDisplay(int width, int height, int colorsNumber = 0)
        {
            display = new Display(width, height, colorsNumber);

        }
        public void DisplayInfo()
        {
            if (display != null)
            {
                Console.WriteLine(" -\tDisplay Width: {0}", display.Width);
                Console.WriteLine(" -\tDisplay Height: {0}", display.Height);
                if (display.ColorsNumber != 0)
                {
                    Console.WriteLine(" -\tNumber Of Colors: {0}M", display.ColorsNumber);
                }
            }
            else
            {
                Console.WriteLine("Display Not Set");
            }
        }

        public void AboutInfo()
        {
            if (this.Owner != null)
            {
                Console.WriteLine("Phone belongs to: {0}", this.Owner);
                Console.WriteLine();
            }
            Console.WriteLine("Phone Info:");
            Console.WriteLine(" -\tManufacturer: {0}", this.Manufacturer);
            Console.WriteLine(" -\tModel: {0}", this.Model);
            if (this.Price != 0)
            {
                Console.WriteLine(" -\tPhone costs: {0:c2}", this.Price);
            }

            if (battery != null)
            {
                Console.WriteLine();
                Console.WriteLine("Phone's Battery Info:");
                BatteryInfo();
            }
            if (display != null)
            {
                Console.WriteLine();
                Console.WriteLine("Phone's Display Info:");
                DisplayInfo();
            }
        }

            //Phone Calls Info
        public void MakeACall(string date, string time, string dialedNumber, int seconds)
        {
            callHistory.Add(new Call(date, time, dialedNumber, seconds));

            callPrice += CallHistory[CallHistory.Count - 1].Price;
        }

        public void DeleteCall(string dialedNumber)
        {
            foreach (var call in callHistory)
            {
                if (call.DialedNumber == dialedNumber)
                {
                    callHistory.Remove(call);
                    callPrice -= call.Price;
                    break;
                }
            }
        }
        public void DeleteCall(string date, string time)
        {
            foreach (var call in callHistory)
            {
                if (call.Date == date && call.Time == time)
                {
                    CallHistory.Remove(call);
                    callPrice -= call.Price;
                    break;
                }
            }
        }
        public void DeleteCall(string date, string time, string dialedNumber)
        {
            foreach (var call in callHistory)
            {
                if (call.DialedNumber == dialedNumber && call.Date == date && call.Time == time)
                {
                    CallHistory.Remove(call);
                    callPrice -= call.Price;
                    break;
                }
            }
        }

        public void DeleteCallsAt(string date)
        {
            foreach (var call in callHistory)
            {
                if (call.Date == date)
                {
                    callHistory.Remove(call);
                    callPrice -= call.Price;
                }
            }
        }

        public void DeleteCallsFrom(string dialedNumber)
        {
            foreach (var call in callHistory)
            {
                if (call.DialedNumber == dialedNumber)
                {
                    callHistory.Remove(call);
                    callPrice -= call.Price;
                }
            }
        }

        public void DeleteCallHistory()
        {
            callHistory.Clear();
            callPrice = 0d;
        }

        public void DeleteLongestCall()
        {
            string longestCallDate = string.Empty;
            string longestCallTime = string.Empty;
            string longestCallNumber = string.Empty;

            foreach (var call in this.callHistory)
            {
                int longestCall = 0;

                if (call.Seconds > longestCall)
                {
                    longestCall = call.Seconds;
                    longestCallDate = call.Date;
                    longestCallTime = call.Time;
                    longestCallNumber = call.DialedNumber;
                }
            }

            DeleteCall(longestCallDate, longestCallTime, longestCallNumber);
        }

        public void ShowCallHistory()
        {
            if (callHistory.Count != 0)
            {
                Console.WriteLine($"{Manufacturer} {Model}'s history:");

                foreach (var call in this.callHistory)
                {
                    Console.WriteLine($"   Number: {call.DialedNumber}  Date: {call.Date}   Time: {call.Time}   Duration: {call.Duration}");
                }
            }
            else
            {
                Console.WriteLine($"{Manufacturer} {Model}'s history is empty");
            }
        }
    }
}
