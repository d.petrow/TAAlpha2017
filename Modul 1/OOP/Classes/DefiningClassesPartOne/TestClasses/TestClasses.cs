﻿using System;
using MobilePhone;
using MobilePhone.Calls;
using System.Collections.Generic;
using System.Text;

namespace TestClasses
{
    class TestClasses
    {
        static void Main(string[] args)
        {
            GSM[] phones =
            {
                new GSM("Galaxy S8", "Samsung", "Dimitar Petrov", 1999.99d),
                new GSM("Optimus", "LG", "Dimitar Petrov", 599.99d),
                new GSM("P9", "Huawei", "Dimitar Petrov", 799.99d),
                new GSM("IPhone 6S", "Apple", "Dimitar Petrov", 1299.99d),
                new GSM("One X", "HTC", "Dimitar Petrov", 899.99d)
            };

            foreach (var item in phones)
            {
                item.AboutInfo();
                Console.WriteLine();
                Console.WriteLine();
            }

            Console.WriteLine(GSM.IPhone4S.Price);


            phones[0].MakeACall("8.12.2017", "18:45", "0897608777", 600);

            Console.WriteLine("------------------");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("------------------");


            Random rand = new Random();


            GSM nokia = new GSM("c300", "NOKIA", "Dimitar Petrov", 5.99d);
            for (int i = 0; i < 30; i++)
            {
                StringBuilder randDate = new StringBuilder();
                randDate.Append(rand.Next(7, 10).ToString()).Append(".12.2017");

                StringBuilder randTime = new StringBuilder();
                randTime.Append(rand.Next(8, 12).ToString()).Append(":00");

                StringBuilder randTNumber = new StringBuilder();
                randTNumber.Append("08").Append(rand.Next(7, 9).ToString()).Append(rand.Next(1000000, 9999999).ToString());

                int seconds = rand.Next(10, 1000);

                nokia.MakeACall(randDate.ToString(), randTime.ToString(), randTNumber.ToString(), seconds);
            }
            nokia.ShowCallHistory();
            Console.WriteLine("Total call price: {0:c2}", nokia.CallPrice);

            nokia.DeleteLongestCall();

            Console.WriteLine();

            Console.WriteLine("Total call price: {0:c2}", nokia.CallPrice);

            nokia.DeleteCallHistory();

            Console.WriteLine();

            nokia.ShowCallHistory();
        }
    }
}
