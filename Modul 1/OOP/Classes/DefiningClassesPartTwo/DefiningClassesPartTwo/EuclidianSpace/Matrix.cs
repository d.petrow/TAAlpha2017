﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bytes2you.Validation;

namespace EuclidianSpace
{
    class Matrix<T> where T : struct
    { 
        //Indexer
        public T this[int row, int col]
        {
            get
            {
                Guard.WhenArgument(row, "r").IsLessThan(0).IsGreaterThan(this.Rows).Throw();
                Guard.WhenArgument(col, "c").IsLessThan(0).IsGreaterThan(this.Cols).Throw();

                return this.InnerMatrix[row, col];
            }
            set
            {
                Guard.WhenArgument(row, "r").IsLessThan(0).IsGreaterThan(this.Rows).Throw();
                Guard.WhenArgument(col, "c").IsLessThan(0).IsGreaterThan(this.Cols).Throw();

                this.InnerMatrix[row, col] = value;
            }
        }

        //Ctors
        public Matrix(int rows, int cols)
        {
            this.Rows = rows;
            this.Cols = cols;
            this.InnerMatrix = new T[rows, cols];
        }

        //Props
        public int Rows { get; }
        public int Cols { get; }
        public T[,] InnerMatrix { get; }

        ////Overloading Operators
        //public static Matrix<T> operator +(Matrix<T> m1, Matrix<T> m2)
        //{
        //    Matrix<T>[,] matrix = new Matrix<T>[m1.Rows, m1.Cols];

        //    for (int r = 0; r < m1.Rows; r++)
        //    {
        //        for (int c = 0; c < m1.Cols; c++)
        //        {
        //            matrix[r, c] = m1[r, c] + m2[r, c];
        //        }
        //    }

        //    return matrix;
        //}
    }
}
