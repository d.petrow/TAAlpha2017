﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EuclidianSpace
{
    class GenericList<T> where T : IComparable<T>, new()
    {
        //Indexer
        public T this [int index]
        {
            get
            {
                if (index < 0 || index >= Count)
                {
                    throw new IndexOutOfRangeException($"Invalid index {index}! Index must be between list's Length. ( Right now: [0; {this.Count - 1}] )");
                }
                return this.List[index];
            }
            set
            {
                if (index < 0 || index >= Count)
                {
                    throw new IndexOutOfRangeException($"Invalid index {index}! Index must be between list's Length. ( Right now: [0; {this.Count - 1}] )");
                }
                this.List[index] = value;
            }
        }

        //Ctors
        public GenericList(int capacity)
        {
            this.Capacity = capacity;
            this.List = new T[capacity];
        }

        //Props
        public int Capacity { get; private set; }
        public T[] List { get; private set; }
        public int Count { get; private set; }

        //Methods
        public void Add(T element)
        {
            this.List[this.Count] = element;

            this.Count++;
            if (this.Count == this.Capacity)
            {
                ExtendList();
            }
        }

        public void Remove(int index)
        {
            if (index < 0 || index >= Count)
            {
                throw new IndexOutOfRangeException($"Invalid index {index}! Index must be between list's Length. ( Right now: [0; {this.Count - 1}] )");
            }

            for (int ind = index + 1; ind < this.Count; ind++)
            {
                this.List[ind - 1] = this.List[ind];
            }

            this.List[this.Count - 1] = default(T);
            this.Count--;
        }

        public void AddAt(T element, int index)
        {
            if (index < 0 || index >= Count)
            {
                throw new IndexOutOfRangeException($"Invalid index {index}! Index must be between list's Length. ( Right now: [0; {this.Count - 1}] )");
            }

            this.Count++;
            if (this.Count == this.Capacity)
            {
                ExtendList();
            }

            for (int ind = this.Count - 1; ind >= index; ind--)
            {
                this.List[ind] = this.List[ind - 1];
            }

            this.List[index] = element;
        }

        public void Clear()
        {
            for (int ind = 0; ind < this.Count; ind++)
            {
                List[ind] = default(T);
            }
        }

        public int IndexOf(T element)
        {
            for (int ind = 0; ind < this.Count; ind++)
            {
                if (element.CompareTo(List[ind]) == 0)
                {
                    return ind;
                }
            }

            return -1;
        }

        public int LastIndexOf(T element)
        {
            for (int ind = this.Count - 1; ind >= 0; ind--)
            {
                if (element.CompareTo(List[ind]) == 0)
                {
                    return ind;
                }
            }

            return -1;
        }

        public T Max()
        {
            if (this.Count == 0)
            {
                Console.WriteLine("No elements in List!");
                return default(T);
            }

            T max = List[0];

            for (int ind = 0; ind < this.Count; ind++)
            {
                if (max.CompareTo(List[ind]) == -1)
                {
                    max = List[ind];
                }
            }

            return max;
        }

        public T Min()
        {
            if (this.Count == 0)
            {
                Console.WriteLine("No elements in List!");
                return default(T);
            }

            T min = List[0];

            for (int ind = 0; ind < this.Count; ind++)
            {
                if (min.CompareTo(List[ind]) == 1)
                {
                    min = List[ind];
                }
            }

            return min;
        }

        private void ExtendList()
        {
            T[] temp = new T[this.Capacity * 2];
            for (int ind = 0; ind < this.Capacity; ind++)
            {
                temp[ind] = this.List[ind];
            }
            Capacity *= 2;
            this.List = temp;
        }
    }
}
