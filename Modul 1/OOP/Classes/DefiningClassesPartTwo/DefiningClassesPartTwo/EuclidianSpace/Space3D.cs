﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EuclidianSpace
{
    static class Space3D
    {
        static public double CalculateDistance(Point3D a, Point3D b)
        {
            double distance = 0d;

            double x = a.X - b.X;
            double y = a.Y - b.Y;
            double z = a.Z - b.Z;

            distance = x * x + y * y + z * z;

            return Math.Sqrt(distance);
        }
    }
}
