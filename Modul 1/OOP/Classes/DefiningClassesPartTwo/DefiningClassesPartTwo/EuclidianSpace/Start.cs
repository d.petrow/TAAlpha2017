﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EuclidianSpace
{
    class Start
    {
        static void Main(string[] args)
        {
            Path path = new Path();

            path.AddPoint(new Point3D(1, 2, 3));
            path.AddPoint(new Point3D(3, 2, 1));
            path.AddPoint(new Point3D(5, 5, 5));

            PathStorage.AddPath("nqkavPath", path);
            var p = PathStorage.GetPath("nqkavPath");
        }
    }
}
