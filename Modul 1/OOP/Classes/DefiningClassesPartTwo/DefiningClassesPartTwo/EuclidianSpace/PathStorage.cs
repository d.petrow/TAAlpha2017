﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EuclidianSpace
{
    static class PathStorage
    {
        private static StreamWriter writer;
        private static StreamReader reader;
        private static List<string> files = new List<string>();

        public static void AddPath(string name, Path path)
        {
            files.Add(name);

            writer = new StreamWriter(@"C:\Users\Dimitar Petrow\source\repos\TAAlpha Repo\OOP\Classes\DefiningClassesPartTwo\DefiningClassesPartTwo\EuclidianSpace\Paths\" + name + ".txt");
            writer.WriteLine(name);
            writer.WriteLine("{");
            for (int ind = 0; ind < path.Points.Count; ind++)
            {
                writer.WriteLine("{0} {1} {2}", path.Points[ind].X, path.Points[ind].Y, path.Points[ind].Z);
            }
            writer.WriteLine("}");

            writer.Close();
        }

        public static Path GetPath(string name)
        {
            Path path = new Path();
            if (files.Contains(name))
            {

                reader = new StreamReader(@"C:\Users\Dimitar Petrow\source\repos\TAAlpha Repo\OOP\Classes\DefiningClassesPartTwo\DefiningClassesPartTwo\EuclidianSpace\Paths\" + name + ".txt");
                string line;
                bool pathFound = false;
                while (!reader.EndOfStream)
                {
                    line = reader.ReadLine();

                    if (pathFound && !line.Contains('}'))
                    {
                        double[] coords = line.Split(' ').Select(double.Parse).ToArray();
                        Point3D point = new Point3D(coords[0], coords[1], coords[2]);
                        path.AddPoint(point);
                    }
                    else
                    {
                        pathFound = false;
                    }

                    if (line.Contains('{'))
                    {
                        pathFound = true;
                    }
                }

                reader.Close();

                return path;
            }
            else
            {
                Console.WriteLine("No such path!");
                return null;
            }
        }
    }
}
