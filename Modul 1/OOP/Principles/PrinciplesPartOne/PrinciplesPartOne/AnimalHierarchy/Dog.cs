﻿using AnimalHierarchy.Common;

namespace AnimalHierarchy
{
    public class Dog : Animal
    {
        public Dog(string name, int age, Gender sex) : base(name, age, sex)
        {
        }

        public override void ProduceSound()
        {
            throw new System.NotImplementedException();
        }
    }
}
