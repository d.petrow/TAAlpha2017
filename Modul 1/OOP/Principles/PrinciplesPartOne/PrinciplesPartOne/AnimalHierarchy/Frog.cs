﻿using AnimalHierarchy.Common;

namespace AnimalHierarchy
{
    public class Frog : Animal
    {
        public Frog(string name, int age, Gender sex) : base(name, age, sex)
        {
        }

        public override void ProduceSound()
        {
            throw new System.NotImplementedException();
        }
    }
}
