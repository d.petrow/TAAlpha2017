﻿using AnimalHierarchy.Common;

namespace AnimalHierarchy
{
    public class Tomcat : Cat
    {
        public Tomcat(string name, int age, Gender sex) : base(name, age, sex)
        {
        }
    }
}
