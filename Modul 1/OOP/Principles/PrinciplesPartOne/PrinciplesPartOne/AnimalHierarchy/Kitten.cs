﻿using AnimalHierarchy.Common;

namespace AnimalHierarchy
{
    public class Kitten : Cat
    {
        public Kitten(string name, int age, Gender sex) : base(name, age, sex)
        {
            //
        }
    }
}
