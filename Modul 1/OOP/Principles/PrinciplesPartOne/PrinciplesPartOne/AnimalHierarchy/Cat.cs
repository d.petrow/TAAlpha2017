﻿using AnimalHierarchy.Common;

namespace AnimalHierarchy
{
    public class Cat : Animal
    {
        public Cat(string name, int age, Gender sex) : base(name, age, sex)
        {
        }

        public override void ProduceSound()
        {
            throw new System.NotImplementedException();
        }
    }
}
