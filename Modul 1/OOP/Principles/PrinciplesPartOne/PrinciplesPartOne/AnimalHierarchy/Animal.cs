﻿using AnimalHierarchy.Common;
using AnimalHierarchy.Contracts;

namespace AnimalHierarchy
{
    public abstract class Animal : ISound
    {
        public Animal(string name, int age, Gender sex)
        {
            this.Name = name;
            this.Age = age;
            this.Sex = sex;
        }

        //Props
        public int Age { get; }
        public string Name { get; }
        public Gender Sex { get; }

        //Methods
        public abstract void ProduceSound();
    }
}
