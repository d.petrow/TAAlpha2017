﻿namespace HumanHierarhy
{
    public abstract class Human
    {
        //Ctors
        public Human(string fName, string lName)
        {
            this.FirstName = fName;
            this.LastName = lName;
        }

        //Props
        public string FirstName { get; }
        public string LastName { get; }

    }
}
