﻿namespace HumanHierarhy
{
    public class Worker : Human
    {
        //Ctors
        public Worker(string fName, string lName, int weekSalary, int workHPD) : base(fName, lName)
        {
            this.WeekSalary = weekSalary;
            this.WorkHoursPerDay = workHPD;
        }

        //Props
        public int WeekSalary { get; }
        public int WorkHoursPerDay { get; }

        //Methods
        public int MoneyPerHour()
        {
            return (this.WeekSalary / 7) / this.WorkHoursPerDay;
        }
    }
}
