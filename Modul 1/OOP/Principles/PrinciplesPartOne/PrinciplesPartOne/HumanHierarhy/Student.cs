﻿namespace HumanHierarhy
{
    public class Student : Human
    {
        //Ctors
        public Student(string fName, string lName, int grade) : base(fName, lName)
        {
            this.Grade = grade;
        }

        //Props
        public int Grade { get; }
    }
}
