﻿using HumanHierarhy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] firstNames = { "Petar", "Ivan", "Dimitar" };
            string[] lastNames = { "Petrov", "Ivanov", "Dimitrov" };


            List<Student> students = new List<Student>();
            List<Worker> workers = new List<Worker>();
            Random rand = new Random();
            for (int i = 1; i <= 10; i++)
            {
                students.Add(new Student(firstNames[rand.Next(3)], lastNames[rand.Next(3)], rand.Next(1, 12)));
                workers.Add(new Worker(firstNames[rand.Next(3)], lastNames[rand.Next(3)], rand.Next(400, 900), rand.Next(6, 8)));
            }

            students = students.OrderBy(x => x.Grade).ToList();


            foreach (var worker in workers)
            {
                Console.WriteLine($"{worker.FirstName} {worker.LastName} MPH: {worker.MoneyPerHour()}$");
            }
            workers = workers.OrderByDescending(x => x.MoneyPerHour()).ToList();
            foreach (var worker in workers)
            {
                Console.WriteLine($"{worker.FirstName} {worker.LastName} MPH: {worker.MoneyPerHour()}$");
            }

            List<Human> humen = new List<Human>();
            for (int i = 0; i < 10; i++)
            {
                humen.Add(students[i]);
                humen.Add(workers[i]);
            }

            humen = humen.OrderBy(x => x.FirstName + x.LastName).ToList();
            foreach (var human in humen)
            {
                Console.WriteLine($"{human.FirstName} {human.LastName}");
            }
        }
    }
}
