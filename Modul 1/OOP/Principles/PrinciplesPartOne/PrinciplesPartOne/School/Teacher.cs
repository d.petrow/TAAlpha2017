﻿using SchoolHierarhy.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolHierarhy
{
    public class Teacher : Person, ITeacher
    {
        //Ctors
        public Teacher(string fName, string lName, IList<Subject> subjects)
        {
            this.FirstName = fName;
            this.LastName = lName;
            this.Subjects = subjects;
        }
        public Teacher(string fName, string lName, IList<Subject> subjects, string comments) : this(fName, lName, subjects)
        {
            this.OptionalComments = comments.Split(new string[] { "; ", ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        //Props
        public override string FirstName { get; }
        public override string LastName { get; }
        public IList<Subject> Subjects { get; }
        public override IList<string> OptionalComments { get; }

        //Methods
        public void AddSubject(Subject subject)
        {
            Subjects.Add(subject);
        }
        public void AddSubject(string name, int lectures, int exercises)
        {
            var sub = new Subject(name, lectures, exercises);
            Subjects.Add(sub);
        }
        public void AddSubject(string name, int lectures, int exercises, string comments)
        {
            var sub = new Subject(name, lectures, exercises, comments);

        }

        public void RemoveSubject(string name)
        {
            if (Subjects.Count == 0)
            {
                throw new ArgumentException("No Subjects to Remove");
            }

            for (int subject = 0; subject < Subjects.Count; subject++)
            {
                if (Subjects[subject].Name == name)
                {
                    Subjects.RemoveAt(subject);
                    return;
                }
            }

            throw new ArgumentException($"No Subject with Name: {name}");
        }

        public void RemoveAllSubjects()
        {
            if (Subjects.Count == 0)
            {
                throw new ArgumentException("No Subjects to Remove");
            }

            Subjects.Clear();
        }
        
        public override void Info()
        {
            throw new NotImplementedException();
        }
    }
}
