﻿using System.Collections.Generic;

namespace SchoolHierarhy.Contracts
{
    interface IOptionalSchoolStuff
    {
        IList<string> OptionalComments { get; }

        void Info();
    }
}
