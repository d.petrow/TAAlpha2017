﻿using System.Collections.Generic;

namespace SchoolHierarhy.Contracts
{
    interface ISchool : IOptionalSchoolStuff
    {
        //Props
        string Name { get; }
        IList<ClassOfStudents> Classes { get; }

        //Methods
        void AddClass(string textIdentifier);
        void AddClass(ClassOfStudents classOfStudents);

        void RemoveClass(string textIdentifier);

        void RemoveAllClasses();
        
    }
}
