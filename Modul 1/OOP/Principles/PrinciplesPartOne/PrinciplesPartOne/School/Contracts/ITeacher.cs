﻿using System.Collections.Generic;

namespace SchoolHierarhy.Contracts
{
    interface ITeacher : IPerson
    {
        //Props
        IList<Subject> Subjects { get; }

        //Methods
        void AddSubject(Subject subject);
        void AddSubject(string name, int lectures, int exercises);
        void AddSubject(string name, int lectures, int exercises, string comments);

        void RemoveSubject(string name);

        void RemoveAllSubjects();
    }
}
