﻿namespace SchoolHierarhy.Contracts
{
    interface ISubject : IOptionalSchoolStuff
    {
        //Props
        string Name { get; }
        int Lectures { get; }
        int Exercises { get; }
    }
}
