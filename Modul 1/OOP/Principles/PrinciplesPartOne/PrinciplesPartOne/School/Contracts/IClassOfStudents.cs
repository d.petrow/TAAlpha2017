﻿using System.Collections.Generic;

namespace SchoolHierarhy.Contracts
{
    interface IClassOfStudents : IOptionalSchoolStuff
    {
        //Props
        string TextIdentifier { get; }
        IList<Teacher> Teachers { get; }
        IList<Student> Students { get; }

        //Methods
        void AddTeacher(Teacher teacher);
        void AddTeacher(string fName, string lName, IList<Subject> subjects);
        void AddTeacher(string fName, string lName, IList<Subject> subjects, string comments);

        void AddStudent(Student student);

        void RemoveStudent(int classNumber);
        void RemoveAllStudents();

        void RemoveTeacher(string firstName, string lastName);
        void RemoveAllTeachers();
    }
}
