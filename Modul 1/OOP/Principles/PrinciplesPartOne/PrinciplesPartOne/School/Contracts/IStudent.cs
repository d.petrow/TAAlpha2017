﻿namespace SchoolHierarhy.Contracts
{
    interface IStudent : IPerson
    {
        int ClassNumber { get; }
    }
}
