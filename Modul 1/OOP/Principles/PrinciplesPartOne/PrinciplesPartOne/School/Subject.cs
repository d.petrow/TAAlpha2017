﻿using System.Collections.Generic;
using SchoolHierarhy.Contracts;
using System;
using System.Linq;

namespace SchoolHierarhy
{
    public class Subject : ISubject
    {
        //Ctors
        public Subject(string name, int lectures, int exercises)
        {
            this.Name = name;
            this.Lectures = lectures;
            this.Exercises = exercises;
        }
        public Subject(string name, int lectures, int exercises, string comments) : this(name, lectures, exercises)
        {
            this.OptionalComments = comments.Split(new string[] { "; ", ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        //Props
        public string Name { get; }

        public int Lectures { get; }

        public int Exercises { get; }

        public IList<string> OptionalComments { get; }

        //Methods
        public void Info()
        {
            throw new System.NotImplementedException();
        }
    }
}
