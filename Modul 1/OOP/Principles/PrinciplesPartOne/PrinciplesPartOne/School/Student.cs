﻿using SchoolHierarhy.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolHierarhy
{
    public class Student : Person, IStudent
    {
        //Ctors
        public Student(string fName, string lName, int classNumber)
        {
            if (classNumber <= 0 || classNumber > 30)
            {
                throw new ArgumentException("Student's class number must be between (0; 30]");
            }

            this.FirstName = fName;
            this.LastName = lName;
            this.ClassNumber = classNumber;
        }
        public Student(string fName, string lName, int classNumber, string comments) : this(fName, lName, classNumber)
        {
            this.OptionalComments = comments.Split(new string[] { "; ", ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        //Props
        public override string FirstName { get; }
        public override string LastName { get; }
        public override IList<string> OptionalComments { get; }
        public int ClassNumber { get; }

        //Methods
        public override void Info()
        {
            throw new NotImplementedException();
        }
    }
}
