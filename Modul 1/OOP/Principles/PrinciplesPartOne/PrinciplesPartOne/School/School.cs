﻿using System.Collections.Generic;
using SchoolHierarhy.Contracts;
using System;
using System.Linq;

namespace SchoolHierarhy
{
    public class School : ISchool
    {
        //Ctors
        public School(string name)
        {
            this.Name = name;
            this.Classes = new List<ClassOfStudents>();
            this.OptionalComments = new List<string>();
        }
        public School(string name, string comments) : this(name)
        {
            this.OptionalComments = comments.Split(new string[] { "; ", ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        //Props
        public string Name { get; }
        public IList<ClassOfStudents> Classes { get; }
        public IList<string> OptionalComments { get; }

        //Methods
        public void AddClass(string textIdentifier)
        {
            var classOfStudents = new ClassOfStudents(textIdentifier);
            Classes.Add(classOfStudents);
        }
        public void AddClass(string textIdentifier, string comments)
        {
            var classOfStudents = new ClassOfStudents(textIdentifier, comments);
            Classes.Add(classOfStudents);
        }
        public void AddClass(ClassOfStudents classOfStudents)
        {
            Classes.Add(classOfStudents);
        }

        public void RemoveClass(string textIdentifier)
        {
            if (Classes.Count == 0)
            {
                throw new ArgumentException("No Classes to Remove");
            }

            for (int cl = 0; cl < Classes.Count; cl++)
            {
                if (Classes[cl].TextIdentifier == textIdentifier)
                {
                    Classes.RemoveAt(cl);
                    return;
                }
            }

            throw new ArgumentException($"No Class with Text Identifier: {textIdentifier}");
        }

        public void RemoveAllClasses()
        {
            if (Classes.Count == 0)
            {
                throw new ArgumentException("No Classes to Remove");
            }

            Classes.Clear();
        }

        public void AddComment(string comment)
        {
            this.OptionalComments.Add(comment);
        }

        public void Info()
        {
            throw new System.NotImplementedException();
        }
    }
}
