﻿using SchoolHierarhy.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolHierarhy
{
    public abstract class Person : IPerson
    {
        //Props
        public abstract string FirstName { get; }
        public abstract string LastName { get; }
        public abstract IList<string> OptionalComments { get; }

        //Methods
        public abstract void Info();
    }
}
