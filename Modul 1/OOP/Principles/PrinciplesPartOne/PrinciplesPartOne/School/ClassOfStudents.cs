﻿using SchoolHierarhy.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolHierarhy
{
    public class ClassOfStudents : IClassOfStudents
    {
        //Ctors
        public ClassOfStudents(string textIdentifier)
        {
            this.TextIdentifier = textIdentifier;
            this.Teachers = new List<Teacher>();
            this.Students = new List<Student>();
        }
        public ClassOfStudents(string textIdentifier, string comments) : this(textIdentifier)
        {
            this.OptionalComments = comments.Split(new string[] { "; ", ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        //Props
        public string TextIdentifier { get; }

        public IList<Teacher> Teachers { get; }

        public IList<Student> Students { get; }

        public IList<string> OptionalComments { get; }

        //Methods
        public void AddStudent(Student student)
        {
            Students.Add(student);
        }

        public void AddTeacher(Teacher teacher)
        {
            Teachers.Add(teacher);
        }
        public void AddTeacher(string fName, string lName, IList<Subject> subjects)
        {
            Teachers.Add(new Teacher(fName, lName, subjects));
        }
        public void AddTeacher(string fName, string lName, IList<Subject> subjects, string comments)
        {
            Teachers.Add(new Teacher(fName, lName, subjects, comments));
        }

        public void RemoveStudent(int classNumber)
        {
            if (Students.Count == 0)
            {
                throw new ArgumentException("No Students to Remove");
            }

            for (int student = 0; student < Students.Count; student++)
            {
                if (Students[student].ClassNumber == classNumber)
                {
                    Students.RemoveAt(student);
                    return;
                }
            }

            throw new ArgumentException($"No Student with Class Number: {classNumber}");
        }
        public void RemoveAllStudents()
        {
            if (Students.Count == 0)
            {
                throw new ArgumentException("No Students to Remove");
            }
            Students.Clear();
        }

        public void RemoveTeacher(string firstName, string lastName)
        {
            if (Teachers.Count == 0)
            {
                throw new ArgumentException("No Teachers to Remove");
            }

            for (int teacher = 0; teacher < Teachers.Count; teacher++)
            {
                if (Teachers[teacher].FirstName == firstName && Teachers[teacher].LastName == lastName)
                {
                    Teachers.RemoveAt(teacher);
                    return;
                }
            }

            throw new ArgumentException($"No Teacher with Name: {firstName} {lastName}");

        }
        public void RemoveAllTeachers()
        {
            if (Teachers.Count == 0)
            {
                throw new ArgumentException("No Teachers to Remove");
            }

            Teachers.Clear();
        }

        public void Info()
        {
            throw new NotImplementedException();
        }

    }
}
