﻿namespace Geometry
{
    public abstract class Shape
    {
        //Ctors
        public Shape(double width, double height)
        {
            this.Width = width;
            this.Height = height;
        }

        //Props
        public double Width { get; }
        public double Height { get; }

        //Methods
        public abstract double CalculateSurface();
    }
}
