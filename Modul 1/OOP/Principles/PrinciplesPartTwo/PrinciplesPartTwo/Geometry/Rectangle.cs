﻿namespace Geometry
{
    public class Rectangle : Shape
    {
        //Ctors
        public Rectangle(double width, double height) : base(width, height)
        {
        }

        //Methods
        public override double CalculateSurface()
        {
            return this.Width * this.Height;
        }
    }
}
