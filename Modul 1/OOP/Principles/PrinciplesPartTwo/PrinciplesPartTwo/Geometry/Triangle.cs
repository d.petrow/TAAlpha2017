﻿namespace Geometry
{
    public class Triangle : Shape
    {
        //Ctors
        public Triangle(double width, double height) : base(width, height)
        {
            
        }

        //Methods
        public override double CalculateSurface()
        {
            return (this.Width * this.Height) / 2;
        }
    }
}
