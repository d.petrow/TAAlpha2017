﻿namespace Geometry
{
    public class Square : Shape
    {
        public Square(double width, double height) : base(width, height)
        {
            if (width != height)
            {
                throw new System.Exception("Square has equal width and height");
            }
        }

        public override double CalculateSurface()
        {
            return this.Width * this.Height;
        }
    }
}
