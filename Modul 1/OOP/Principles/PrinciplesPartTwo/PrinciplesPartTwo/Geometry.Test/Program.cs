﻿namespace Geometry.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape[] shapes =
            {
                new Triangle(12.5d,21.2d),
                new Rectangle(20d,30d),
                new Square(20d,20d)
            };

            foreach (var shape in shapes)
            {
                System.Console.WriteLine("{0:f2}",shape.CalculateSurface());
            }
        }
    }
}
