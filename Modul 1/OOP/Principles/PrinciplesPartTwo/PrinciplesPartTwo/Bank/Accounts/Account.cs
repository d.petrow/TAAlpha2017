﻿using Bank.Customers;
using System;

namespace Bank.Accounts
{
    public class Account
    {
        public Account(Customer customer, decimal balance, decimal interestRate)
        {
            if (balance < 0m)
            {
                throw new ArgumentException("Balance cannot be Negative");
            }

            this.Customer = customer ?? throw new ArgumentNullException("No customer");
            this.Balance = balance;
            this.InterestRate = interestRate;
        }


        public Customer Customer { get; }

        public decimal Balance { get; protected set; }

        public decimal InterestRate { get; }


        public void Deposit(decimal amount)
        {
            if (amount < 0m)
            {
                throw new ArgumentException("Cannot deposit Negative Amount");
            }

            this.Balance += amount;
        }

        public virtual void Withdraw(decimal amount)
        {
            throw new ArgumentException("This Account Cannot With Draw Money");
        }

        public decimal CalculateInterestAmount(int months)
        {
            return this.InterestRate * months;
        }
    }
}
