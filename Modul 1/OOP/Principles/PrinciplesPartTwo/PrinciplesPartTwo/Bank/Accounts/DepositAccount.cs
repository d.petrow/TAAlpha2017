﻿using Bank.Customers;
using System;

namespace Bank.Accounts
{
    public class DepositAccount : Account
    {
        public DepositAccount(Customer customer, decimal balance, decimal interestRate) : base(customer, balance, interestRate)
        {
        }

        public override void Withdraw(decimal amount)
        {
            if (amount < 0m)
            {
                throw new ArgumentException("Cannot withdraw Negative Amount");
            }
            if (this.Balance - amount < 0m)
            {
                throw new ArgumentException("Cannot withdraw Amount bigger than the Account's Balance");
            }

            this.Balance -= amount;
        }
    }
}
