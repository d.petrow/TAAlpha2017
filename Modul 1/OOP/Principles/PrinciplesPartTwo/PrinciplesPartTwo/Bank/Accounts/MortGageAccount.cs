﻿using Bank.Customers;

namespace Bank.Accounts
{
    public class MortGageAccount : Account
    {
        public MortGageAccount(Customer customer, decimal balance, decimal interestRate) : base(customer, balance, interestRate)
        {
        }
    }
}
