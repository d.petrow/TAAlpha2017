﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleCity.Core
{
    public class BattleCityEngine
    {
        //Fields
        private static readonly BattleCityEngine SingleInstance = new BattleCityEngine();

        //Ctors...

        //Props
        public static BattleCityEngine Instance
        {
            get
            {
                return SingleInstance;
            }
        }
        
        //Methods
        public void Start()
        {
            
        }
    }
}
