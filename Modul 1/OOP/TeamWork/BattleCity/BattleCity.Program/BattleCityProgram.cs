﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleCity.Core;

namespace BattleCity
{
    class BattleCityProgram
    {
        static void Main(string[] args)
        {
            BattleCityEngine.Instance.Start();
        }
    }
}
