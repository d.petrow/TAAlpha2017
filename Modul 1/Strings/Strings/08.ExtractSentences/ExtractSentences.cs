﻿using System;
using System.Collections.Generic;
using System.Text;

class ExtractSentences
{
    static void Main(string[] args)
    {
        string word = Console.ReadLine();
        string text = Console.ReadLine();
        //StringBuilder wordInSent = new StringBuilder();
        StringBuilder result = new StringBuilder();

        string[] sentences = text.Split('.');
        List<char> separatorsList = new List<char>();


        for (int ch = 0; ch < text.Length; ch++)
        {
            if (!char.IsLetter(text[ch]) && !separatorsList.Contains(text[ch]))
            {
                separatorsList.Add(text[ch]);
            }
        }

        char[] seps = separatorsList.ToArray();

        //bool isFirstSent = true;
        for (int sent = 0; sent < sentences.Length; sent++)
        {
            bool wordFound = false;

            string[] words = sentences[sent].Split(seps, StringSplitOptions.RemoveEmptyEntries);

            for (int w = 0; w < words.Length; w++)
            {
                if (words[w] == word)
                {
                    wordFound = true;
                    break;
                }
            }

            if (wordFound)
            {
                result.Append(sentences[sent].Trim() + ". ");
                continue;
            }

            //    bool wordFound = false;
            //    for (int ch = 0; ch <= sentences[sent].Length; ch++)
            //    {
            //        if ((ch != sentences[sent].Length) && !wordFound && (char.IsLetter(sentences[sent][ch]) || sentences[sent][ch] == '\''))
            //        {
            //            wordInSent.Append(sentences[sent][ch]);
            //        }
            //        else
            //        {
            //            if (word.Equals(wordInSent.ToString()))
            //            {
            //                wordFound = true;
            //                if (isFirstSent)
            //                {
            //                    Console.Write(sentences[sent] + ".");
            //                    isFirstSent = false;
            //                }
            //                else
            //                {
            //                    Console.Write(" " + sentences[sent] + ".");
            //                }

            //                wordInSent.Remove(0, wordInSent.Length);

            //                break;
            //            }

            //            wordInSent.Remove(0, wordInSent.Length);
            //        }

            //    }
        }

        Console.WriteLine(result.ToString().Trim());
    }
}

