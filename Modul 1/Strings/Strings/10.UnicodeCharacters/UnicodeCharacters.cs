﻿using System;
using System.Text;

class UnicodeCharacters
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        StringBuilder seqofUniChars = new StringBuilder();

        string uniChar = string.Empty;
        for (int index = 0; index < str.Length; index++)
        {
            uniChar = string.Format("{0:X}", (int)str[index]);

            seqofUniChars.Append("\\u");
            seqofUniChars.Append(uniChar.PadLeft(4, '0'));
        }

        Console.WriteLine(seqofUniChars.ToString());
    }
}

