﻿using System;
using System.Text;

class ParseURL
{
    static void Main(string[] args)
    {
        string urlAdress = Console.ReadLine();
        StringBuilder protocol = new StringBuilder();
        StringBuilder server = new StringBuilder();
        StringBuilder resource = new StringBuilder();

        bool protocolFound = false;
        bool serverFound = false;
        for (int index = 0; index < urlAdress.Length; index++)
        {
            if (!protocolFound && urlAdress[index] == ':')
            {
                if (index + 3 < urlAdress.Length && urlAdress.Substring(index, 3) == "://")
                {
                    protocolFound = true;
                    index += 3;
                }
            }
            else if (!serverFound && urlAdress[index] == '/')
            {
                serverFound = true;
            }


            if (!protocolFound)
            {
                protocol.Append(urlAdress[index]);
            }
            else if (!serverFound)
            {
                server.Append(urlAdress[index]);
            }
            else
            {
                resource.Append(urlAdress[index]);
            }
        }

        Console.WriteLine("[protocol] = {0}", protocol.ToString());
        Console.WriteLine("[server] = {0}", server.ToString());
        Console.WriteLine("[resource] = {0}", resource.ToString());
    }
}

