﻿using System;
using System.Text;

class Seriesfetters
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        StringBuilder removedRepeats = new StringBuilder(str[0].ToString());


        for (int index = 1; index < str.Length; index++)
        {
            if (str[index - 1] != str[index])
            {
                removedRepeats.Append(str[index]);
            }
        }

        Console.WriteLine(removedRepeats.ToString());
    }
}
